<?php 
include_once('dashboard1.php');
//session_start();
if(!isset($_SESSION['login']) || $_SESSION['login'] !== true){
$_SESSION['sign_error'] = "You must log in to access your dashboard";    
header("Location: login.php");    
}
$test = new Dashboard1();
//    $test->set_session_value('login_details',$res);    
//} 


?>

<!DOCTYPE html>
<html lang="en">    
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">   
<link rel="shortcut icon" href="images/icon2.fw.png">
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/mdb.css">
<link rel="stylesheet" href="stylo.css">
<style>

.sc_style{
        border-radius:20px;
        padding:8px; 
        text-transform:capitalize; 
        font-size:14px; 
        background-color: black; 
        border: 1px solid black;
    } 
    .sc_mouse{
        border-radius:20px;
        padding:8px; 
        text-transform:capitalize; 
        font-size:14px; 
        background-color: white; 
        color:black;
    }

#div_span{
    border: 1px solid lightgrey; 
    background-color: lightgray;
    border-radius: 50px;
    width: auto;
}

.succ_div{
                    width:auto;
                    float:left;                    
                }
                
                .succ_details{
                    width:auto;
                    background:#5aeeb0;
                    color:#048d54;
                    padding:10px 40px;
                    margin:30px;
                    border-radius:5px;                  
                }
.vid_div{
        border: 1px solid lightgrey; 
        border-radius: 10px; 
        margin: 20px 10px 20px 10px;
        text-align: center;
}        
.square-btnn{
    display: block;    
}
.f_right{
        float: right;
}        
.h2_text{
        color:white;
}        
.p_text{
        color: white; 
        font-size: 18px;
}        
.back_div{
        background-color:black; 
        opacity:0.9; 
        width: inherit; 
        height: 50%; 
        padding: 8%;        
}        
.d_bg1{
        background-image: url("images/thor_bus1.png");
        background-repeat: no-repeat;
        width: 100%;
        height: 750px;
        border:1px solid white;
        border-radius: 5px;

    }
    .d_bg2{
        background-image: url("images/thor_bus2.png");
        background-repeat: no-repeat;
        height: 750px;
        width: 100%;
        border:1px solid white;
        border-radius: 5px;
    }
    .d_bg3{
        background-image: url("images/thor_bus3.png");
        background-repeat: no-repeat;
        height: 750px;
        width: 100%;
        border:1px solid white;
        border-radius: 5px;
    }

 .foot_img{
        /*width: 100%;*/
        margin-top: 20px;
        border-top: 1px solid lightgrey;
        height: 600px;
        background-image: url("images/busss.png");
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
        
    }   
    

    @media screen and (max-width: 900px) {
  .c1{
    margin-left:30px;
  }
  .back_div{
      padding: 2%;    
      height: 60%;
   
  }
  .h2_text{
        font-size: 18px;
  }
  .vid_div{
        border: 1px solid white;           
  }
  .p_text{
        font-size: 15px;
  }
  video{
      margin-left: 50px;
  }
  .foot_img{
        
          background-image: url("images/bus11.png");
          height: 500px;
  }
  .d_bg1{
        
        height: 390px;
        width:100%;
  }
  .d_bg2{
        
        width:100%;
        height: 390px;

  }
  .d_bg3{
        
        height: 390px;
        width:100%;

  }
  .square-btnn{
    /*display: none;    */
}

}


@media screen and (max-width: 500px) {
  .c1{
    margin-left:30px;
  }
  .vid_div{
        border: 1px solid white;           
  }
  #div_span{
      border-radius: 80px;
  }
  .back_div{
      padding: 2%;    
      height: 100%;
  }
  .h2_text{
        font-size: 15px;
  }
  .p_text{
        font-size: 12px;
  }
  video{
      margin-left: 80px;
  }
  .foot_img{
        
          background-image: url("images/bus1.png");
          height: 500px;
  }
  .d_bg1{
        background-image: url("images/thor11.png");
        height: 200px;

  }
  .d_bg2{
        background-image: url("images/thor22.png");
        height: 200px;

  }
  .d_bg3{
        background-image: url("images/thor4444.png");
        height: 200px;

  }
  .square-btnn{
    /*display: none;    */
}

}
li:hover{
        text-decoration: underline;
}


</style>
<title>247Rentals</title>   
</head>
<body>
                <header>
                                <nav class="navbar navbar-expand-lg navbar-dark indigo">
                                    
                                      
                                      <a class="navbar-brand" href="index.php"><img src="images/icon.fw.png" style="margin-right:3px"><span style="font-size:18px;" class="badge badge-pill badge-light">247Rentals</span></a>
                                    
                                      
                                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
                                          aria-expanded="false" aria-label="Toggle navigation">
                                          <span class="navbar-toggler-icon"></span>
                                      </button>
                                    
                                      <!-- Collapsible content -->
                                      <div class="collapse navbar-collapse" id="basicExampleNav">
                                    
                                          <!-- Links -->
                                          <ul class="navbar-nav mr-auto">
                                              <li class="nav-item">
                                                  <a class="nav-link" href="loginsuccess.php">Dashboard
                                                      <span class="sr-only">(current)</span>
                                                  </a>
                                              </li>
                                              <li class="nav-item">
                                                  <a class="nav-link" href="profile.php">Profile</a>
                                              </li>
                                              
                        
                                              <li class="nav-item active">
                                                <a class="nav-link" href="videos.php">Upload Videos</a>
                                            </li>

                                            <li class="nav-item">
                                                <a class="nav-link" href="movie_rate.php">Rate Movies</a>
                                            </li>

                                            <li class="nav-item">
                                                  <a class="nav-link" href="logout.php">Log out</a>
                                              </li>
                                              <!-- Dropdown -->
                                                                                        </ul>
                                          <!-- Links -->
                                    
                                                                                    
                                          
                                      </div>
                                      <!-- Collapsible content -->
                                    
                                    </nav>

                                    

                                </header>
<main class="mt-40">
 <div class="jumbotron joumbotron-fluid indigo">
        <div class="row justify-content-center"><div class="p-2 mb-4 text-center text-white" id="div_span"><h4 class="display-4 font-weight-bold black-text">Upload Your Videos</h4></div></div>
        
        

</div>   
<div class="container">


        <section id="contact">
                                                    <hr>
                                                        <!-- Heading -->
                                                        <h2 class="mb-3 font-weight-bold text-center"> Upload Your Movie</h3>
                                                        <!--Grid row-->
                                                        <div class="row">
                                                    
                                                        
                                                                <div class="col-lg-2">
                                                                        <!-- Form contact -->
                                                                        <!-- Form contact -->
                                                                      </div>

                                                        <!--Grid column-->
                                                        <div class="col-lg-8">
                                                        <h5 style="color:red;">
                                                        <?php 
                                                        if(isset($_SESSION['update_reply'])){
                                                            echo $_SESSION['update_reply'];
                                                            unset($_SESSION['update_reply']);
                                                        }else{
                                                            //echo "No session data";
                                                            
                                                        }
                                                        ?>
                                                        </h5>
                                                                <!-- Form contact -->
                                            <form class="p-5" id="upload_form" enctype="multipart/form-data">
                                                            
                                                                    <div class="md-form form-sm">
                                                                        <input type="text" name="movie_title" id="movie_title" value="" class="form-control form-control-sm inp" placeholder="Movie Title" >
                                                                        <span id="movie_title_error" style="color:red"></span>
                                                                    </div>
                                                                        
                                                                        <div class="md-form form-sm">
                                                                        <input type="text" value="" name="movie_genre" id="movie_genre" class="form-control form-control-sm inp" placeholder="Movie Genre">
                                                                        <span id="movie_genre_error" style="color:red"></span>
                                                                    </div>

                                                                    <div class="md-form form-sm">
                                                                        <input type="text" value="" name="movie_description" id="movie_description" class="form-control form-control-sm inp" placeholder="Movie Description" value="">
                                                                    <span id="movie_description_error" style="color:red"></span>
                                                                    </div>

                                                                    
                                                                        
                                                                    <div class="md-form form-sm">
                                                                        <input type="file" name="video" id="video" class="form-control form-control-sm inp" placeholder="Upload Movie">
                                                                        <span id="video_error" style="color:red;"></span>
                                                                    </div>

                                                                    <div class="mt-4 text-center">
                                                                    <button type="submit" class="btn btn-sm btn-success BTN" id="submit_button"><span data-feather="send"></span> Submit</button>
                                                                         
                                                                    </div>
                                                                    
                                                                    <input type="hidden" name="log" value="update">
                                                                    <div class="progress" id="prog_container20">
                                                                    <div class="progress-bar" id="progress_bar" role="progressbar" aria-valuenow="0"
  aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                                                        
  </div>
                                                                    </form>
                                                                    
                                                                    
                      </div>        

                                                                    
                                                              </div>


                                                              <div class="col-lg-2">
                                                                    <!-- Form contact -->
                                                                    
                                                                    <!-- Form contact -->
                                                                  </div>
                                                        <!--Grid column-->
                                                    
                                                        <!--Grid column-->
                                                        
                                                        <!--Grid column-->
                                                    
                                                        </div>
                                                        <!--Grid row-->
                                                    
                                                    </section>

        <div class="row">
                
        <?php 
        $check = $test->check_video_database($_SESSION['login_user'],"*",'username');
        $login=array();
          $count=0;
         if($check !== false){ 
        foreach($check as $res){
            $count+=1;
        ?>
                  <!--Grid column-->
                  <div class="col-lg-4 col-md-12">
                
                    <!--Card-->
                    <div class="card">
                <form id="delete" action="dashboard.php" method="post"></form>
                <input type = "hidden" name="d_id[]" form="delete" value="<?php echo $res['id']; ?>">
                <input type = "hidden" name="link_id[]" form="delete" value="<?php echo $res['download_link']; ?>">
                <input type = "hidden" name="log_delete" form="delete" value="delete">
                      <!--Card image-->
                      <div class="view overlay">
                        <div style='background-color:black; width:350px; height:150px; color: white' id="img_opq19" class="card-img-top"><div class="succ_div ">
                            <div class="succ_details text-center featurette-heading font-weight-bold"> <?php echo $res['title']; ?></div>
                        </div></div>
                      </div>
                
                      <!--Card content-->
                      <div class="card-body">
                        <!--Title-->
                        <h4 class="card-title text-center">Movie Attributes</h4>
                        <hr>
                        <!--Text-->
                        <p class="card-text"><b>Genre:</b> <?php echo $res['genre']; ?></p>
                        <hr>
                        <p class="card-text"><b>Description:</b> <?php echo $res['description']; ?></p>
                        <hr>
                        
                        <a class="btn btn-primary" href="<?php echo "{$res['download_link']}" ?>" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;" download>Download Video</a>
                      </div>
                      
                      <form class="p-2" action="dashboard.php" method="POST">
                                                            
                                                            <div class="md-form form-sm">
                                                                <input type="text" name="title[]" value="" class="form-control form-control-sm inp" placeholder="Movie Title" >
                                                                
                                                            </div>
                                                                
                                                                <div class="md-form form-sm">
                                                                <input type="text" value="" name="genre[]" class="form-control form-control-sm inp" placeholder="Movie Genre">
                                                                
                                                            </div>

                                                            <div class="md-form form-sm">
                                                                <input type="text" value="" name="description[]" id="description" class="form-control form-control-sm inp" placeholder="Movie Description">
                                                                
                                                            </div>

                                                            
                                                                

                                                            <div class="mt-4 text-center">
                                                            <button type="submit" class="btn btn-sm btn-success BTN" id="submit_button"><span data-feather="send"></span> Update Movie</button>
                                                            <a href="dashboard.php?del=<?php echo $res['download_link']; ?>"><button type="submit" class="btn btn-sm btn-success BTN" id="delete_button" form="delete"><span data-feather="send"></span> Delete Movie</button></a>
                                                                
                                                            </div>
                                                            
                                                            <input type="hidden" name="log" value="update_video">
                                                            <input type="hidden" name="id_value[]" value="<?php echo $res['id']; ?>">
                                                                <div class="mt-4 text-center" id="log_messages">
                                                                
                                                                </div>
                                                            </form>

                       

                    </div>
                    <!--/.Card-->
                
                  </div>
                  <?php
}}
                  ?>
                  <!--Grid column-->
                
                  <!--Grid column-->
                  
                <hr>

                
</div>

</main>                                
<footer class="page-footer text-center text-md-left font-medium indigo pt-4 mt-4">
  
      <!--Footer Links-->
      <div class="container text-center text-md-left">
          <div class="row">
  
              <!--First column-->
              <div class="col-md-6 pb-3">
                  <h5 class="text-uppercase">Quick Links</h5>
                  
                  <b style="font-size:18px;"><a href="loginsuccess.php">Dashboard | 
                                
                              </a></b>
                              <b style="font-size:18px;"><a  href="profile.php">Profile | 
                                
                                  </a></b>
                                  <b style="font-size:18px;"><a href="videos.php">Upload Videos 
                                
                                      </a></b>

                                                                            
                                      

<!--<button class="btn sc_style sc" id="scroll_up"><span data-feather="arrow-up"></span> Automatic ScrollUp</button>-->
                  
              </div>
              <!--/.First column-->
  
              <!--Second column-->
              <div class="col-md-6 pb-3">
                      <!--
                      <a href="#!"><img src="images/teleg1.png"></a>
                      <a href="#!"><img src="images/teleg.png"></a>
                      <a href="#!"><img src="images/teleg.png"></a>
                      <a href="#!"><img src="images/teleg1.png"></a>
                      -->
              </div>
      
          </div>
      </div>
      
      <div class="footer-copyright py-3 text-center indigo">
          © 2018 Copyright:
          <a href="#"> 247Rentals.com </a>
          <?php  ?>
      </div>
      
  
  </footer>
  <script src="bootstrap/js/jquery.js"></script>
  <script src="js/popper.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="bootstrap/js/mdb.min.js"></script>
  <script src="includes/js/feather.min.js"></script>    
  <script src="feath.js"></script>
  <script src="js/gallery.js"></script>

<script>
    
    function check_empty_file(video){
        if(video.files.length===0){
            return false;
        }
        return true;
    }
    function check_file_support(filee){
        var arr = ['mp4','avi','3gp'];
        for(var i =0; i< arr.length;i++){
            if(filee == arr[i]){
                return true;
            }
        }
        return false;
    }
        var video = document.getElementById('video');
        video.addEventListener('change',check_file,false);
        function check_file(){
            var file_select = this.files;
            var s = file_select[0].name.split('.');
            var ext = s[s.length-1];
            var file_size = Math.floor(file_select[0].size/1000000);
            if(!check_file_support(ext)){
                $('#video_error').html('File format not supported. Only mp4 and avi files are allowed. '+ext+" files are not supported");
                return;
            }else{
                $('#video_error').html('');
            }
            if(file_select.length>0){
                if(file_size>10){
                    $('#video_error').html('File too large. Please ensure file is 10mb or less in size');    
                }else{
                    $('#video_error').html('');
                }
            }



        //    console.log("Extension is "+ext);
          //  console.log('File name '+file_select[0].name);
            //console.log('File size '+Math.floor(file_select[0].size/1000000));
            
        }
        var form = document.getElementById('upload_form');
        var but = document.getElementById('submit_button');
        form.addEventListener('submit',function(e){
            e.preventDefault();

            if($('#movie_title').val().trim() === ''){
                $('#movie_title_error').html('Please fill the movie title field');
                return false;
            }else{
                $('#movie_title_error').html('');
            }

            if($('#movie_genre').val().trim() === ''){
                $('#movie_genre_error').html('Please fill the movie genre field');
                return false;
            }else{
                $('#movie_genre_error').html('');
            }

            if($('#movie_description').val().trim() === ''){
                $('#movie_description_error').html('Please fill in a brief description of the movie');
                return false;
            }else{
                $('#movie_description_error').html('');
            }
            var vid = document.getElementById('video');
            var file_select = vid.files;
            if(!check_empty_file(vid)){
                $('#video_error').html('Please select a video file of 10mb or less');
                return false;
            }else{
                $('#video_error').html('');
            }
            //var file_select = this.files;
            var s = file_select[0].name.split('.');
            var ext = s[s.length-1];
            var file_size = Math.floor(file_select[0].size/1000000);
            if(!check_file_support(ext)){
                $('#video_error').html('File format not supported. Only mp4 and avi files are allowed. '+ext+" files are not supported");
                return false;
            }else{
                $('#video_error').html('');
            }
            if(file_select.length>0){
                if(file_size>10){
                    $('#video_error').html('File too large. Please ensure file is 10mb or less in size');    
                }else{
                    $('#video_error').html('');
                }
            }
            
    //return false;
    
    var formdata = new FormData(form);
    var request = new XMLHttpRequest();
    //https://week-6.herokuapp.com/t.php
        request.open('POST','t.php');
        $('#submit_button').attr('readonly','readonly');
        request.onreadystatechange = function() { // A simple event handler.
    if (request.readyState === 4 ){
        switch(request.responseText){
            case 'large file':
            $('#video_error').html('File size too large for server. Please make sure file size is 10mb at most');
            $('#submit_button').removeAttr('readonly');
            break;
            case 'ini error':
            $('#video_error').html('File size too large for the server to handle');
            $('#submit_button').removeAttr('readonly');
            break;
            case 'network error':
            $('#video_error').html('Connection to server terminated by poor network');
            $('#submit_button').removeAttr('readonly');
            break;
            case 'no file error':
            $('#video_error').html('Please specify a video file to upload');
            $('#submit_button').removeAttr('readonly');
            break;
            case 'php error':
            $('#video_error').html('Error from server');
            $('#submit_button').removeAttr('readonly');
            break;
            case 'error':
            $('#video_error').html('Unfortunately heroku does not support writing to folders');
            $('#submit_button').removeAttr('readonly');
            //console.log(request.responseText);
            break;
            case 'failed insert':
            $('#video_error').html('Error saving data to database');
            $('#submit_button').removeAttr('readonly');
            //console.log(request.responseText);
            break;
            case 'invalid extension':
            $('#video_error').html('Only mp4, 3gp and avi files are permitted');
            $('#submit_button').removeAttr('readonly');
            break;
            case 'successful':
            //$('#video_error').html('Video information uploaded and saved successfully');
            //$('#progress_bar').html("");
           // $('#progress_bar').attr('aria-valuenow',0);
           // $('#progress_bar').css('width',0+"%");
            location.reload();
            break;
            default:
            $('#video_error').html('Unfortunately heroku does not support writing to folders');
            $('#submit_button').removeAttr('readonly');
            //console.log(request.responseText);
        }
    }
     // When response is complete
   // console.log(request.responseText); // ...call the callback.
    //window.location.href = "videos.html";
    };
    
    request.upload.onprogress = function(e) {
    if (e.lengthComputable) {
        //console.log("Total length"+e.total/1000);
        //console.log("Loaded length"+e.loaded/1000);
      
    var uploaded =Math.round(e.loaded/e.total*100);
    $('#progress_bar').html(uploaded+"% Uploaded");
    $('#progress_bar').attr('aria-valuenow',uploaded);
    $('#progress_bar').css('width',uploaded+"%");
    //console.log(Math.round(e.loaded/e.total*100) +"% Uploaded");
    }
    };
    
    
    
    request.send(formdata);
    request.onprogress = function(e) {
    if (e.lengthComputable){
     //   console.log(e.total);
       // console.log(e.loaded);
        //console.log(Math.round(100*e.loaded/e.total) + "% Complete");
    }
    
    }
        });
        $('#submit').click(function(){
           
        /*var request = new XMLHttpRequest();
        request.open('POST','http://247rentals/t.php');
        request.onreadystatechange = function() { // A simple event handler.
    if (request.readyState === 4 ) // When response is complete
    console.log(req.responseText); // ...call the callback.
    };
    
    for(var name in formdata) {
    if (!formdata.hasOwnProperty(name)) 
    continue; // Skip inherited properties
    var value = formdata[name];
    if (typeof value === "function") continue; // Skip methods
    // Each property becomes one "part" of the request.
    // File objects are allowed here
    
    formdata.append(name, value); // Add name/value as one part
    }
    for(var i = 0; i< formdata.getAll('mat_no').length;i++){
        console.log(formdata.getAll('mat_no')[i]);
    }
    // Send the name/value pairs in a multipart/form-data request body. Each
    // pair is one part of the request. Note that send automatically sets
    // the Content-Type header when you pass it a FormData object
    //request.send(formdata);
    return false;*/
        })
        
    </script>
    


</body>
</html>
