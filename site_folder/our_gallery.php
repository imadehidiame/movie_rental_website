<!DOCTYPE html>
<html lang="en">    
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">   
<link rel="shortcut icon" href="images/icon2.fw.png">
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/mdb.css">
<link rel="stylesheet" href="stylo.css">
<style>

.sc_style{
        border-radius:20px;
        padding:8px; 
        text-transform:capitalize; 
        font-size:14px; 
        background-color: black; 
        border: 1px solid black;
    } 
    .sc_mouse{
        border-radius:20px;
        padding:8px; 
        text-transform:capitalize; 
        font-size:14px; 
        background-color: white; 
        color:black;
    }

#div_span{
    border: 1px solid lightgrey; 
    background-color: lightgray;
    border-radius: 50px;
    width: auto;
}
.vid_div{
        border: 1px solid lightgrey; 
        border-radius: 10px; 
        margin: 20px 10px 20px 10px;
        text-align: center;
}        
.square-btnn{
    display: block;    
}
.f_right{
        float: right;
}        
.h2_text{
        color:white;
}        
.p_text{
        color: white; 
        font-size: 18px;
}        
.back_div{
        background-color:black; 
        opacity:0.9; 
        width: inherit; 
        height: 50%; 
        padding: 8%;        
}        
.d_bg1{
        background-image: url("images/thor_bus1.png");
        background-repeat: no-repeat;
        width: 100%;
        height: 750px;
        border:1px solid white;
        border-radius: 5px;

    }
    .d_bg2{
        background-image: url("images/thor_bus2.png");
        background-repeat: no-repeat;
        height: 750px;
        width: 100%;
        border:1px solid white;
        border-radius: 5px;
    }
    .d_bg3{
        background-image: url("images/thor_bus3.png");
        background-repeat: no-repeat;
        height: 750px;
        width: 100%;
        border:1px solid white;
        border-radius: 5px;
    }

 .foot_img{
        /*width: 100%;*/
        margin-top: 20px;
        border-top: 1px solid lightgrey;
        height: 600px;
        background-image: url("images/busss.png");
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
        
    }   
    

    @media screen and (max-width: 900px) {
  .c1{
    margin-left:30px;
  }
  .back_div{
      padding: 2%;    
      height: 60%;
   
  }
  .h2_text{
        font-size: 18px;
  }
  .vid_div{
        border: 1px solid white;           
  }
  .p_text{
        font-size: 15px;
  }
  video{
      margin-left: 50px;
  }
  .foot_img{
        
          background-image: url("images/bus11.png");
          height: 500px;
  }
  .d_bg1{
        
        height: 390px;
        width:100%;
  }
  .d_bg2{
        
        width:100%;
        height: 390px;

  }
  .d_bg3{
        
        height: 390px;
        width:100%;

  }
  .square-btnn{
    /*display: none;    */
}

}


@media screen and (max-width: 500px) {
  .c1{
    margin-left:30px;
  }
  .vid_div{
        border: 1px solid white;           
  }
  #div_span{
      border-radius: 80px;
  }
  .back_div{
      padding: 2%;    
      height: 100%;
  }
  .h2_text{
        font-size: 15px;
  }
  .p_text{
        font-size: 12px;
  }
  video{
      margin-left: 80px;
  }
  .foot_img{
        
          background-image: url("images/bus1.png");
          height: 500px;
  }
  .d_bg1{
        background-image: url("images/thor11.png");
        height: 200px;

  }
  .d_bg2{
        background-image: url("images/thor22.png");
        height: 200px;

  }
  .d_bg3{
        background-image: url("images/thor4444.png");
        height: 200px;

  }
  .square-btnn{
    /*display: none;    */
}

}
li:hover{
        text-decoration: underline;
}


</style>
<title>247Rentals</title>   
</head>
<body>
                <header>
                                <nav class="navbar navbar-expand-lg navbar-dark indigo">
                                    
                                      
                                      <a class="navbar-brand" href="index.php"><img src="images/icon.fw.png" style="margin-right:3px"><span style="font-size:18px;" class="badge badge-pill badge-light">247Rentals</span></a>
                                    
                                      
                                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
                                          aria-expanded="false" aria-label="Toggle navigation">
                                          <span class="navbar-toggler-icon"></span>
                                      </button>
                                    
                                      <!-- Collapsible content -->
                                      <div class="collapse navbar-collapse" id="basicExampleNav">
                                    
                                          <!-- Links -->
                                          <ul class="navbar-nav mr-auto">
                                              <li class="nav-item">
                                                  <a class="nav-link" href="index.php">Home
                                                      <span class="sr-only">(current)</span>
                                                  </a>
                                              </li>
                                              <li class="nav-item active">
                                                  <a class="nav-link" href="gallery.php">Gallery</a>
                                              </li>
                                              
                        
                                              <li class="nav-item">
                                                <a class="nav-link" href="about_us.php">About</a>
                                            </li>
                                              <!-- Dropdown -->
                                              <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Contact <span style="font-size:13px;" class="badge badge-pill badge-light">247Rentals</span></a>
                                                    <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                                                        <a class="dropdown-item" href="send_message.php">News Letter Subscription</a>
                                                        <a class="dropdown-item" href="place_order.php">Place An Order</a>
                                                        
                                                        
                                                    </div>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="leap_year.php">Leap Year</a>
                                                </li>
                                          </ul>
                                          <!-- Links -->
                                    
                                          <form class="form-inline">
                                            <div class="md-form mt-0">
                                            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                                            </div>
                                            <div class="md-form mt-0">
                                              <button class="btn btn-sm btn-success BTN"><span data-feather="search"></span> Search</button>
                                        </div>
                                          </form>
                                          <form class="form-inline">
                                              <div class="md-form mt-0">
                                              <!--<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">-->
                                              <select class="custom-select" id="scroll_down_select" name="sellist1" style="background-color:black;color:white;">
                                                <option selected value="150">Select Scroll Speed</option>
                                                <option value="1000">Extra Slow</option>
                                                <option value="500">Slow</option>
                                                <option value="150">Normal</option>
                                                <option value="50">Fast</option>
                                                <option value="10">Very Fast</option>
                                                <option value="1">Extra Fast</option>
                                              </select>
                                              </div>
                                              <div class="md-form mt-0">
                                                  <button class="btn sc_style sc" id="scroll_down"><span data-feather="arrow-down"></span>Automatic ScrollDown</button>
                                          </div>
                                            </form>                                          
                                          
                                      </div>
                                      <!-- Collapsible content -->
                                    
                                    </nav>

                                    

                                </header>
<main class="mt-40">
 <div class="jumbotron joumbotron-fluid indigo">
        <div class="row justify-content-center"><div class="p-2 mb-4 text-center text-white" id="div_span"><h4 class="display-4 font-weight-bold black-text">247Rentals Gallery</h4></div></div>
        
        

</div>   
<div class="container">

        <div class="row">
                
                  <!--Grid column-->
                  <div class="col-lg-4 col-md-12">
                
                    <!--Card-->
                    <div class="card">
                
                      <!--Card image-->
                      <div class="view overlay">
                        <img src="css/blood_work1.jpg" id="img_opq20" class="card-img-top" alt="">
                        <a href="#">
                          <div class="mask rgba-white-slight"></div>
                        </a>
                      </div>
                
                      <!--Card content-->
                      <div class="card-body">
                        <!--Title-->
                        <h4 class="card-title text-center">Blood Work</h4>
                        <hr>
                        <!--Text-->
                        <p class="card-text"><b>Genre:</b> Action Adventure</p>
                        <hr>
                        <p class="card-text"><b>Length:</b> 1:31:45</p>
                        <hr>
                        <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                      </div>
                      <button class="btn btn-sm btn-outline-warning BTN" id="hide_img20"><span data-feather="zoom-out"></span> Hide Image</button>
                      <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img20"><span data-feather="zoom-in"></span> Show Image</button>
                      <div class="progress pro_hide" id="prog_container20">
                      <div class="progress-bar" id="prog20"></div>
                      </div> 

                    </div>
                    <!--/.Card-->
                
                  </div>
                  <!--Grid column-->
                
                  <!--Grid column-->
                  <div class="col-lg-4 col-md-12">
                        
                            <!--Card-->
                            <div class="card">
                        
                              <!--Card image-->
                              <div class="view overlay">
                                <img src="css/invisible_sisters11.jpg" id="img_opq19" class="card-img-top" alt="">
                                <a href="#">
                                  <div class="mask rgba-white-slight"></div>
                                </a>
                              </div>
                        
                              <!--Card content-->
                              <div class="card-body">
                                <!--Title-->
                                <h4 class="card-title text-center">Invisible Sister</h4>
                                <hr>
                                <!--Text-->
                                <p class="card-text"><b>Genre:</b> Comedy</p>
                                <hr>
                                <p class="card-text"><b>Length:</b> 1:31:45</p>
                                <hr>
                                <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                              </div>
                              <button class="btn btn-sm btn-outline-warning BTN" id="hide_img19"><span data-feather="zoom-out"></span> Hide Image</button>
                              <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img19"><span data-feather="zoom-in"></span> Show Image</button>
                              <div class="progress pro_hide" id="prog_container19">
                              <div class="progress-bar" id="prog19"></div>
                              </div> 
                            </div>
                            <!--/.Card-->
                        
                          </div>
                  <!--Grid column-->
                
                  <!--Grid column-->
                  <div class="col-lg-4 col-md-12">
                        
                            <!--Card-->
                            <div class="card">
                        
                              <!--Card image-->
                              <div class="view overlay">
                                <img src="img/gal2.jpg" id="img_opq18" class="card-img-top" alt="">
                                <a href="#">
                                  <div class="mask rgba-white-slight"></div>
                                </a>
                              </div>
                        
                              <!--Card content-->
                              <div class="card-body">
                                <!--Title-->
                                <h4 class="card-title text-center">Honey</h4>
                                <hr>
                                <!--Text-->
                                <p class="card-text"><b>Genre:</b> High School Romance, Dance</p>
                                <hr>
                                <p class="card-text"><b>Length:</b> 1:41:45</p>
                                <hr>
                                <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                              </div>
                              <button class="btn btn-sm btn-outline-warning BTN" id="hide_img18"><span data-feather="zoom-out"></span> Hide Image</button>
                              <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img18"><span data-feather="zoom-in"></span> Show Image</button>
                              <div class="progress pro_hide" id="prog_container18">
                              <div class="progress-bar" id="prog18"></div>
                              </div> 
                            </div>
                            <!--/.Card-->
                        
                          </div>
                          </div>
                <hr>

                <div class="row" style="margin-top:20px;">
                        
                          <!--Grid column-->
                          <div class="col-lg-4 col-md-12">
                                
                                    <!--Card-->
                                    <div class="card">
                                
                                      <!--Card image-->
                                      <div class="view overlay">
                                        <img src="css/a_quiet_place1.jpg" id="img_opq17" class="card-img-top" alt="">
                                        <a href="#">
                                          <div class="mask rgba-white-slight"></div>
                                        </a>
                                      </div>
                                
                                      <!--Card content-->
                                      <div class="card-body">
                                        <!--Title-->
                                        <h4 class="card-title text-center">A Quiet Place</h4>
                                        <hr>
                                        <!--Text-->
                                        <p class="card-text"><b>Genre:</b> Horror</p>
                                        <hr>
                                        <p class="card-text"><b>Length:</b> 1:23:45</p>
                                        <hr>
                                        <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                      </div>
                                      <button class="btn btn-sm btn-outline-warning BTN" id="hide_img17"><span data-feather="zoom-out"></span> Hide Image</button>
                                      <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img17"><span data-feather="zoom-in"></span> Show Image</button>
                                      <div class="progress pro_hide" id="prog_container17">
                                      <div class="progress-bar" id="prog17"></div>
                                      </div> 
                                    </div>
                                    <!--/.Card-->
                                
                                  </div>
                                  <!--Grid column-->
                                
                                  <!--Grid column-->
                                  <div class="col-lg-4 col-md-12">
                                        
                                            <!--Card-->
                                            <div class="card">
                                        
                                              <!--Card image-->
                                              <div class="view overlay">
                                                <img src="css/after_earth1.jpg" id="img_opq16" class="card-img-top" alt="">
                                                <a href="#">
                                                  <div class="mask rgba-white-slight"></div>
                                                </a>
                                              </div>
                                        
                                              <!--Card content-->
                                              <div class="card-body">
                                                <!--Title-->
                                                <h4 class="card-title text-center">After Earth</h4>
                                                <hr>
                                                <!--Text-->
                                                <p class="card-text"><b>Genre:</b> Action ,Sci-fi</p>
                                                <hr>
                                                <p class="card-text"><b>Length:</b> 2:41:45</p>
                                                <hr>
                                                <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                              </div>
                                              <button class="btn btn-sm btn-outline-warning BTN" id="hide_img16"><span data-feather="zoom-out"></span> Hide Image</button>
                                              <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img16"><span data-feather="zoom-in"></span> Show Image</button>
                                              <div class="progress pro_hide" id="prog_container16">
                                              <div class="progress-bar" id="prog16"></div>
                                              </div> 
                                            </div>
                                            <!--/.Card-->
                                        
                                          </div>
                                  <!--Grid column-->
                                
                                  <!--Grid column-->
                                  <div class="col-lg-4 col-md-12">
                                        
                                            <!--Card-->
                                            <div class="card">
                                        
                                              <!--Card image-->
                                              <div class="view overlay">
                                                <img src="css/baby1.jpg" id="img_opq15" class="card-img-top" alt="">
                                                <a href="#">
                                                  <div class="mask rgba-white-slight"></div>
                                                </a>
                                              </div>
                                        
                                              <!--Card content-->
                                              <div class="card-body">
                                                <!--Title-->
                                                <h4 class="card-title text-center">Baby</h4>
                                                <hr>
                                                <!--Text-->
                                                <p class="card-text"><b>Genre:</b> Love</p>
                                                <hr>
                                                <p class="card-text"><b>Length:</b> 2:58:45</p>
                                                <hr>
                                                <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                              </div>
                                              <button class="btn btn-sm btn-outline-warning BTN" id="hide_img15"><span data-feather="zoom-out"></span> Hide Image</button>
                                              <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img15"><span data-feather="zoom-in"></span> Show Image</button>
                                              <div class="progress pro_hide" id="prog_container15">
                                              <div class="progress-bar" id="prog15"></div>
                                              </div> 
                                            </div>
                          <!--Grid column-->
                        
                        </div>
                        </div>
                            <hr>
                            <div class="row" style="margin-top:20px;">
                                    
                                      <!--Grid column-->
                                      <div class="col-lg-4 col-md-12">
                                            
                                                <!--Card-->
                                                <div class="card">
                                            
                                                  <!--Card image-->
                                                  <div class="view overlay">
                                                    <img src="css/black_panther1.jpg" id="img_opq14" class="card-img-top" alt="">
                                                    <a href="#">
                                                      <div class="mask rgba-white-slight"></div>
                                                    </a>
                                                  </div>
                                            
                                                  <!--Card content-->
                                                  <div class="card-body">
                                                    <!--Title-->
                                                    <h4 class="card-title text-center">Black Panther</h4>
                                                    <hr>
                                                    <!--Text-->
                                                    <p class="card-text"><b>Genre:</b> Action,Adventure</p>
                                                    <hr>
                                                    <p class="card-text"><b>Length:</b> 1:51:45</p>
                                                    <hr>
                                                    <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                                  </div>
                                                  <button class="btn btn-sm btn-outline-warning BTN" id="hide_img14"><span data-feather="zoom-out"></span> Hide Image</button>
                                                  <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img14"><span data-feather="zoom-in"></span> Show Image</button>
                                                  <div class="progress pro_hide" id="prog_container14">
                                                  <div class="progress-bar" id="prog14"></div>
                                                  </div> 
                                                </div>
                                                <!--/.Card-->
                                            
                                              </div>
                                              <!--Grid column-->
                                            
                                              <!--Grid column-->
                                              <div class="col-lg-4 col-md-12">
                                                    
                                                        <!--Card-->
                                                        <div class="card">
                                                    
                                                          <!--Card image-->
                                                          <div class="view overlay">
                                                            <img src="css/criminal1.jpg" id="img_opq13" class="card-img-top" alt="">
                                                            <a href="#">
                                                              <div class="mask rgba-white-slight"></div>
                                                            </a>
                                                          </div>
                                                    
                                                          <!--Card content-->
                                                          <div class="card-body">
                                                            <!--Title-->
                                                            <h4 class="card-title text-center">Criminal</h4>
                                                            <hr>
                                                            <!--Text-->
                                                            <p class="card-text"><b>Genre:</b> Action, Crime</p>
                                                            <hr>
                                                            <p class="card-text"><b>Length:</b> 1:09:45</p>
                                                            <hr>
                                                            <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                                          </div>
                                                          <button class="btn btn-sm btn-outline-warning BTN" id="hide_img13"><span data-feather="zoom-out"></span> Hide Image</button>
                                                          <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img13"><span data-feather="zoom-in"></span> Show Image</button>
                                                          <div class="progress pro_hide" id="prog_container13">
                                                          <div class="progress-bar" id="prog13"></div>
                                                          </div> 
                                                        </div>
                                                        <!--/.Card-->
                                                    
                                                      </div>
                                              <!--Grid column-->
                                            
                                              <!--Grid column-->
                                              <div class="col-lg-4 col-md-12">
                                                    
                                                        <!--Card-->
                                                        <div class="card">
                                                    
                                                          <!--Card image-->
                                                          <div class="view overlay">
                                                            <img src="css/gifted1.jpg" id="img_opq12" class="card-img-top" alt="">
                                                            <a href="#">
                                                              <div class="mask rgba-white-slight"></div>
                                                            </a>
                                                          </div>
                                                    
                                                          <!--Card content-->
                                                          <div class="card-body">
                                                            <!--Title-->
                                                            <h4 class="card-title text-center">Gifted</h4>
                                                            <hr>
                                                            <!--Text-->
                                                            <p class="card-text"><b>Genre:</b> Love</p>
                                                            <hr>
                                                            <p class="card-text"><b>Length:</b> 1:35:45</p>
                                                            <hr>
                                                            <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                                          </div>
                                                          <button class="btn btn-sm btn-outline-warning BTN" id="hide_img12"><span data-feather="zoom-out"></span> Hide Image</button>
                                                          <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img12"><span data-feather="zoom-in"></span> Show Image</button>
                                                          <div class="progress pro_hide" id="prog_container12">
                                                          <div class="progress-bar" id="prog12"></div>
                                                          </div> 
                                                        </div>
                                      <!--Grid column-->
                                    
                                    </div>
                                    </div>
                                    <hr>
                                <div class="row" style="margin-top:20px;">
                                        
                                          <!--Grid column-->
                                          <div class="col-lg-4 col-md-12">
                                        
                                            <!--Card-->
                                            <div class="card">
                                        
                                              <!--Card image-->
                                              <div class="view overlay">
                                                <img src="css/home_front1.jpg" id="img_opq11" class="card-img-top" alt="">
                                                <a href="#">
                                                  <div class="mask rgba-white-slight"></div>
                                                </a>
                                              </div>
                                        
                                              <!--Card content-->
                                              <div class="card-body">
                                                    <!--Title-->
                                                    <h4 class="card-title text-center">Home Front</h4>
                                                    <hr>
                                                    <!--Text-->
                                                    <p class="card-text"><b>Genre:</b> Crime, Action Drama</p>
                                                    <hr>
                                                    <p class="card-text"><b>Length:</b> 1:39:45</p>
                                                    <hr>
                                                    <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                                  </div>
                                                  <button class="btn btn-sm btn-outline-warning BTN" id="hide_img11"><span data-feather="zoom-out"></span> Hide Image</button>
                                                  <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img11"><span data-feather="zoom-in"></span> Show Image</button>
                                                  <div class="progress pro_hide" id="prog_container11">
                                                  <div class="progress-bar" id="prog11"></div>
                                                  </div> 
                                            </div>
                                            <!--/.Card-->
                                        
                                          </div>
                                          <!--Grid column-->
                                        
                                          <!--Grid column-->
                                          <div class="col-lg-4 col-md-12">
                                        
                                            <!--Card-->
                                            <div class="card">
                                        
                                              <!--Card image-->
                                              <div class="view overlay">
                                                <img src="css/iceman1.jpg" id="img_opq10" class="card-img-top" alt="">
                                                <a href="#">
                                                  <div class="mask rgba-white-slight"></div>
                                                </a>
                                              </div>
                                        
                                              <!--Card content-->
                                              <div class="card-body">
                                                    <!--Title-->
                                                    <h4 class="card-title text-center">Iceman</h4>
                                                    <hr>
                                                    <!--Text-->
                                                    <p class="card-text"><b>Genre:</b> Action</p>
                                                    <hr>
                                                    <p class="card-text"><b>Length:</b> 2:00:45</p>
                                                    <hr>
                                                    <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                                  </div>
                                                  <button class="btn btn-sm btn-outline-warning BTN" id="hide_img10"><span data-feather="zoom-out"></span> Hide Image</button>
                                                  <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img10"><span data-feather="zoom-in"></span> Show Image</button>
                                                  <div class="progress pro_hide" id="prog_container10">
                                                  <div class="progress-bar" id="prog10"></div>
                                                  </div> 
                                            </div>
                                            <!--/.Card-->
                                        
                                          </div>
                                          <!--Grid column-->
                                        
                                          <!--Grid column-->
                                          <div class="col-lg-4 col-md-12">
                                        
                                            <!--Card-->
                                            <div class="card">
                                        
                                              <!--Card image-->
                                              <div class="view overlay">
                                                <img src="css/pan1.jpg" id="img_opq9" class="card-img-top" alt="">
                                                <a href="#">
                                                  <div class="mask rgba-white-slight"></div>
                                                </a>
                                              </div>
                                        
                                              <!--Card content-->
                                              <div class="card-body">
                                                    <!--Title-->
                                                    <h4 class="card-title text-center">Pan</h4>
                                                    <hr>
                                                    <!--Text-->
                                                    <p class="card-text"><b>Genre:</b> Action, Crime</p>
                                                    <hr>
                                                    <p class="card-text"><b>Length:</b> 1:35:45</p>
                                                    <hr>
                                                    <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                                  </div>
                                                  <button class="btn btn-sm btn-outline-warning BTN" id="hide_img9"><span data-feather="zoom-out"></span> Hide Image</button>
                                                  <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img9"><span data-feather="zoom-in"></span> Show Image</button>
                                                  <div class="progress pro_hide" id="prog_container9">
                                                  <div class="progress-bar" id="prog9"></div>
                                                  </div> 
                                            </div>
                                            <!--/.Card-->
                                        
                                          </div>
                                          <!--Grid column-->
                                        
                                        </div>

                                        <hr>
                                        <div class="row" style="margin-top:20px;">
                                                
                                                  <!--Grid column-->
                                                  <div class="col-lg-4 col-md-12">
                                                
                                                    <!--Card-->
                                                    <div class="card">
                                                
                                                      <!--Card image-->
                                                      <div class="view overlay">
                                                        <img src="css/pirates_of_the_caribbean.jpg" id="img_opq8" class="card-img-top" alt="">
                                                        <a href="#">
                                                          <div class="mask rgba-white-slight"></div>
                                                        </a>
                                                      </div>
                                                
                                                      <!--Card content-->
                                                      <div class="card-body">
                                                            <!--Title-->
                                                            <h4 class="card-title text-center">Pirates of The Caribbean</h4>
                                                            <hr>
                                                            <!--Text-->
                                                            <p class="card-text"><b>Genre:</b> Classic, Action, Drama</p>
                                                            <hr>
                                                            <p class="card-text"><b>Length:</b> 2:39:45</p>
                                                            <hr>
                                                            <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                                          </div>
                                                          <button class="btn btn-sm btn-outline-warning BTN" id="hide_img8"><span data-feather="zoom-out"></span> Hide Image</button>
                                                          <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img8"><span data-feather="zoom-in"></span> Show Image</button>
                                                          <div class="progress pro_hide" id="prog_container8">
                                                          <div class="progress-bar" id="prog8"></div>
                                                          </div> 
                                                    </div>
                                                    <!--/.Card-->
                                                
                                                  </div>
                                                  <!--Grid column-->
                                                
                                                  <!--Grid column-->
                                                  <div class="col-lg-4 col-md-12">
                                                
                                                    <!--Card-->
                                                    <div class="card">
                                                
                                                      <!--Card image-->
                                                      <div class="view overlay">
                                                        <img src="css/pompeii1.jpg" id="img_opq7" class="card-img-top" alt="">
                                                        <a href="#">
                                                          <div class="mask rgba-white-slight"></div>
                                                        </a>
                                                      </div>
                                                
                                                      <!--Card content-->
                                                      <div class="card-body">
                                                            <!--Title-->
                                                            <h4 class="card-title text-center">PompeII</h4>
                                                            <hr>
                                                            <!--Text-->
                                                            <p class="card-text"><b>Genre:</b> Action</p>
                                                            <hr>
                                                            <p class="card-text"><b>Length:</b> 2:09:45</p>
                                                            <hr>
                                                            <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                                          </div>
                                                          <button class="btn btn-sm btn-outline-warning BTN" id="hide_img7"><span data-feather="zoom-out"></span> Hide Image</button>
                                                          <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img7"><span data-feather="zoom-in"></span> Show Image</button>
                                                          <div class="progress pro_hide" id="prog_container7">
                                                          <div class="progress-bar" id="prog7"></div>
                                                          </div> 
                                                
                                                    </div>
                                                    <!--/.Card-->
                                                
                                                  </div>
                                                  <!--Grid column-->
                                                
                                                  <!--Grid column-->
                                                  <div class="col-lg-4 col-md-12">
                                                
                                                    <!--Card-->
                                                    <div class="card">
                                                
                                                      <!--Card image-->
                                                      <div class="view overlay">
                                                        <img src="css/red_sparrow1.jpg" id="img_opq6" class="card-img-top" alt="">
                                                        <a href="#">
                                                          <div class="mask rgba-white-slight"></div>
                                                        </a>
                                                      </div>
                                                
                                                      <!--Card content-->
                                                      <div class="card-body">
                                                            <!--Title-->
                                                            <h4 class="card-title text-center">Red Sparrow</h4>
                                                            <hr>
                                                            <!--Text-->
                                                            <p class="card-text"><b>Genre:</b> Action</p>
                                                            <hr>
                                                            <p class="card-text"><b>Length:</b> 1:35:45</p>
                                                            <hr>
                                                            <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                                          </div>
                                                          <button class="btn btn-sm btn-outline-warning BTN" id="hide_img6"><span data-feather="zoom-out"></span> Hide Image</button>
                                                          <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img6"><span data-feather="zoom-in"></span> Show Image</button>
                                                          <div class="progress pro_hide" id="prog_container6">
                                                          <div class="progress-bar" id="prog6"></div>
                                                          </div>

                                                    </div>
                                                    <!--/.Card-->
                                                
                                                  </div>
                                                  <!--Grid column-->
                                                
                                                </div>

                                                <hr>
                                                <div class="row" style="margin-top:20px;">
                                                        
                                                          <!--Grid column-->
                                                          <div class="col-lg-4 col-md-12">
                                                        
                                                            <!--Card-->
                                                            <div class="card">
                                                        
                                                              <!--Card image-->
                                                              <div class="view overlay">
                                                                <img src="css/spartacus1.jpg" id="img_opq5" class="card-img-top" alt="">
                                                                <a href="#">
                                                                  <div class="mask rgba-white-slight"></div>
                                                                </a>
                                                              </div>
                                                        
                                                              <!--Card content-->
                                                              <div class="card-body">
                                                                    <!--Title-->
                                                                    <h4 class="card-title text-center">Spartacus</h4>
                                                                    <hr>
                                                                    <!--Text-->
                                                                    <p class="card-text"><b>Genre:</b>  Action, Drama, Series</p>
                                                                    <hr>
                                                                    <p class="card-text"><b>Length:</b> 9:39:45</p>
                                                                    <hr>
                                                                    <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                                                  </div>
                                                                  <button class="btn btn-sm btn-outline-warning BTN" id="hide_img5"><span data-feather="zoom-out"></span> Hide Image</button>
                                                                  <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img5"><span data-feather="zoom-in"></span> Show Image</button>
                                                                  <div class="progress pro_hide" id="prog_container5">
                                                                  <div class="progress-bar" id="prog5"></div>
                                                                  </div>
                                                            </div>
                                                            <!--/.Card-->
                                                        
                                                          </div>
                                                          <!--Grid column-->
                                                        
                                                          <!--Grid column-->
                                                          <div class="col-lg-4 col-md-12">
                                                        
                                                            <!--Card-->
                                                            <div class="card">
                                                        
                                                              <!--Card image-->
                                                              <div class="view overlay">
                                                                <img src="css/sultan1.jpg" id="img_opq4" class="card-img-top" alt="">
                                                                <a href="#">
                                                                  <div class="mask rgba-white-slight"></div>
                                                                </a>
                                                              </div>
                                                        
                                                              <!--Card content-->
                                                              <div class="card-body">
                                                                    <!--Title-->
                                                                    <h4 class="card-title text-center">Sultan</h4>
                                                                    <hr>
                                                                    <!--Text-->
                                                                    <p class="card-text"><b>Genre:</b> Action</p>
                                                                    <hr>
                                                                    <p class="card-text"><b>Length:</b> 2:00:45</p>
                                                                    <hr>
                                                                    <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                                                  </div>
                                                                  <button class="btn btn-sm btn-outline-warning BTN" id="hide_img4"><span data-feather="zoom-out"></span> Hide Image</button>
                                                                  <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img4"><span data-feather="zoom-in"></span> Show Image</button>
                                                                  <div class="progress pro_hide" id="prog_container4">
                                                                  <div class="progress-bar" id="prog4"></div>
                                                                  </div>
                                                            </div>
                                                            <!--/.Card-->
                                                        
                                                          </div>
                                                          <!--Grid column-->
                                                        
                                                          <!--Grid column-->
                                                          <div class="col-lg-4 col-md-12">
                                                        
                                                            <!--Card-->
                                                            <div class="card">
                                                        
                                                              <!--Card image-->
                                                              <div class="view overlay">
                                                                <img src="css/the_bourne_identity1.jpg" id="img_opq3" class="card-img-top" alt="">
                                                                <a href="#">
                                                                  <div class="mask rgba-white-slight"></div>
                                                                </a>
                                                              </div>
                                                        
                                                              <!--Card content-->
                                                              <div class="card-body">
                                                                    <!--Title-->
                                                                    <h4 class="card-title text-center">The Bourne Identity</h4>
                                                                    <hr>
                                                                    <!--Text-->
                                                                    <p class="card-text"><b>Genre:</b> Action, Crime</p>
                                                                    <hr>
                                                                    <p class="card-text"><b>Length:</b> 1:35:45</p>
                                                                    <hr>
                                                                    <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                                                  </div>
                                                                  <button class="btn btn-sm btn-outline-warning BTN" id="hide_img3"><span data-feather="zoom-out"></span> Hide Image</button>
                                                                  <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img3"><span data-feather="zoom-in"></span> Show Image</button>
                                                                  <div class="progress pro_hide" id="prog_container3">
                                                                  <div class="progress-bar" id="prog3"></div>
                                                                  </div>
                                                            </div>
                                                            <!--/.Card-->
                                                           
                                                          </div>
                                                          <!--Grid column-->
                                                        
                                                        </div>

                                                        <hr>
                                                        <div class="row" style="margin-top:20px;">
                                                                
                                                                  <!--Grid column-->
                                                                  <div class="col-lg-4 col-md-12">
                                                                
                                                                    <!--Card-->
                                                                    <div class="card">
                                                                
                                                                      <!--Card image-->
                                                                      <div class="view overlay">
                                                                        <img src="css/the_great_wall1.jpg" id="img_opq2" class="card-img-top" alt="">
                                                                        <a href="#">
                                                                          <div class="mask rgba-white-slight"></div>
                                                                        </a>
                                                                      </div>
                                                                
                                                                      <!--Card content-->
                                                                      <div class="card-body">
                                                                            <!--Title-->
                                                                            <h4 class="card-title text-center">The Great Wall</h4>
                                                                            <hr>
                                                                            <!--Text-->
                                                                            <p class="card-text"><b>Genre:</b> Action Drama</p>
                                                                            <hr>
                                                                            <p class="card-text"><b>Length:</b> 1:59:45</p>
                                                                            <hr>
                                                                            <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                                                          </div>
                                                                          <button class="btn btn-sm btn-outline-warning BTN" id="hide_img2"><span data-feather="zoom-out"></span> Hide Image</button>
                                                                          <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img2"><span data-feather="zoom-in"></span> Show Image</button>
                                                                          <div class="progress pro_hide" id="prog_container2">
                                                                          <div class="progress-bar" id="prog2"></div>
                                                                          </div>
                                                                    </div>
                                                                    <!--/.Card-->
                                                                
                                                                  </div>
                                                                  <!--Grid column-->
                                                                
                                                                  <!--Grid column-->
                                                                  <div class="col-lg-4 col-md-12">
                                                                
                                                                    <!--Card-->
                                                                    <div class="card">
                                                                
                                                                      <!--Card image-->
                                                                      <div class="view overlay">
                                                                        <img src="css/the_twilight_saga1.jpg" id="img_opq1" class="card-img-top" alt="">
                                                                        <a href="#">
                                                                          <div class="mask rgba-white-slight"></div>
                                                                        </a>
                                                                      </div>
                                                                
                                                                      <!--Card content-->
                                                                      <div class="card-body">
                                                                            <!--Title-->
                                                                            <h4 class="card-title text-center">The Twilight Saga</h4>
                                                                            <hr>
                                                                            <!--Text-->
                                                                            <p class="card-text"><b>Genre:</b> Horror</p>
                                                                            <hr>
                                                                            <p class="card-text"><b>Length:</b> 2:00:45</p>
                                                                            <hr>
                                                                            <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a>
                                                                          </div>
                                                                          <button class="btn btn-sm btn-outline-warning BTN" id="hide_img1"><span data-feather="zoom-out"></span> Hide Image</button>
                                                                          <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img1"><span data-feather="zoom-in"></span> Show Image</button>
                                                                          <div class="progress pro_hide" id="prog_container1">
                                                                          <div class="progress-bar" id="prog1"></div>
                                                                          </div>
                                                                    </div>
                                                                    <!--/.Card-->
                                                                    
                                                                  </div>
                                                                  <!--Grid column-->
                                                                
                                                                  <!--Grid column-->
                                                                  <div class="col-lg-4 col-md-12">
                                                                
                                                                    <!--Card-->
                                                                    <div class="card">
                                                                
                                                                      <!--Card image-->
                                                                      <div class="view overlay">
                                                                        <img src="css/tomb_raider1.jpg" id="img_opq" class="card-img-top" alt="">
                                                                        <a href="#">
                                                                          <div class="mask rgba-white-slight"></div>
                                                                        </a>
                                                                      </div>
                                                                
                                                                      <!--Card content-->
                                                                     <div class="card-body">
                                                                            <!--Title-->
                                                                            <h4 class="card-title text-center">Tomb Raider</h4>
                                                                            <hr>
                                                                            <!--Text-->
                                                                            <p class="card-text"><b>Genre:</b> Action</p>
                                                                            <hr>
                                                                            <p class="card-text"><b>Length:</b> 1:47:45</p>
                                                                            <hr>
                                                                            <a href="#!" class="btn btn-primary" style="border-radius:20px;padding:8px; text-transform:capitalize; font-size:14px;">Check Out</a><br>
                                                                            
                                                                          </div>
                                                                          <button class="btn btn-sm btn-outline-warning BTN" id="hide_img"><span data-feather="zoom-out"></span> Hide Image</button>
                                                                          <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img"><span data-feather="zoom-in"></span> Show Image</button>
                                                                          <div class="progress pro_hide" id="prog_container">
                                                                          <div class="progress-bar" id="prog"></div>
                                                                          </div>
                                                                    </div>
                                                                    <!--/.Card-->
                                                                
                                                                  </div>
                                                                  <!--Grid column-->
                                                                
                                                                </div>


</div>

</main>                                
<footer class="page-footer text-center text-md-left font-medium indigo pt-4 mt-4">
  
      <!--Footer Links-->
      <div class="container text-center text-md-left">
          <div class="row">
  
              <!--First column-->
              <div class="col-md-6 pb-3">
                  <h5 class="text-uppercase">Quick Links</h5>
                  
                  <b style="font-size:18px;"><a href="index.html">Home | 
                                
                              </a></b>
                              <b style="font-size:18px;"><a  href="our_gallery.html">Gallery | 
                                
                                  </a></b>
                                  <b style="font-size:18px;"><a href="send_message.html">Contact 
                                
                                      </a></b>

                                                                            
                                      <form class="form-inline">
                                        <div class="md-form mt-0">
                                        <!--<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">-->
                                        <select class="custom-select" id="scroll_up_select" name="sellist1" style="background-color:black;color:white;">
                                          <option selected value="150">Select Scroll Speed</option>
                                          <option value="1000">Extra Slow</option>
                                          <option value="500">Slow</option>
                                          <option value="150">Normal</option>
                                          <option value="50">Fast</option>
                                          <option value="10">Very Fast</option>
                                          <option value="1">Extra Fast</option>
                                        </select>
                                        </div>
                                        <div class="md-form mt-0">
                                          <button class="btn sc_style sc" id="scroll_up"><span data-feather="arrow-up"></span> Automatic ScrollUp</button>
                                    </div>
                                      </form>

<!--<button class="btn sc_style sc" id="scroll_up"><span data-feather="arrow-up"></span> Automatic ScrollUp</button>-->
                  
              </div>
              <!--/.First column-->
  
              <!--Second column-->
              <div class="col-md-6 pb-3">
                      <!--
                      <a href="#!"><img src="images/teleg1.png"></a>
                      <a href="#!"><img src="images/teleg.png"></a>
                      <a href="#!"><img src="images/teleg.png"></a>
                      <a href="#!"><img src="images/teleg1.png"></a>
                      -->
              </div>
      
          </div>
      </div>
      
      <div class="footer-copyright py-3 text-center indigo">
          © 2018 Copyright:
          <a href="#"> 247Rentals.com </a>
      </div>
      
  
  </footer>
  <script src="bootstrap/js/jquery.js"></script>
  <script src="js/popper.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="bootstrap/js/mdb.min.js"></script>
  <script src="includes/js/feather.min.js"></script>    
  <script src="feath.js"></script>
  <script src="js/gallery.js"></script>
</body>
</html>