<?php 
session_start();
include_once('db_configuration.php');
class DB_Operations extends db_config{
private $conn;    
public function __construct(){
   try{
    $this->conn = new PDO("mysql:host=$this->host;dbname=$this->db","$this->username",$this->password);
    //echo "Connected successfully";
}catch(PDOException $ex){
    die("Could not connect ".$ex->getMessage());
}
}


public function insertBlob($username,$title,$genre,$description,$download_link) {
    //$blob = fopen($filePath, 'rb');
    //try{
    $sql = "INSERT INTO movies(username,title,genre,description,download_link) VALUES(:username,:title,:genre,:description,:download_link)";
    $stmt = $this->conn->prepare($sql);
    $stmt->bindParam(':username', $username);
    $stmt->bindParam(':title', $title);
    $stmt->bindParam(':genre', $genre);
    $stmt->bindParam(':description', $description);
    $stmt->bindParam(':download_link', $download_link);
    //$stmt->bindParam(':data', $blob, PDO::PARAM_LOB);

    return $stmt->execute();
    //}catch(Exception $e){
      //  return $e->getMessage();
    //}
}

/**
 * update the files table with the new blob from the file specified
 * by the filepath
 * @param int $id
 * @param string $filePath
 * @param string $mime
 * @return bool
 */
function updateBlob($id, $filePath, $mime) {

    $blob = fopen($filePath, 'rb');

    $sql = "UPDATE files
            SET mime = :mime,
                data = :data
            WHERE id = :id;";

    $stmt = $this->pdo->prepare($sql);

    $stmt->bindParam(':mime', $mime);
    $stmt->bindParam(':data', $blob, PDO::PARAM_LOB);
    $stmt->bindParam(':id', $id);

    return $stmt->execute();
}

/**
 * select data from the the files
 * @param int $id
 * @return array contains mime type and BLOB data
 */
public function selectBlob($id) {

    $sql = "SELECT mime,
                    data
               FROM files
              WHERE id = :id;";

    $stmt = $this->pdo->prepare($sql);
    $stmt->execute(array(":id" => $id));
    $stmt->bindColumn(1, $mime);
    $stmt->bindColumn(2, $data, PDO::PARAM_LOB);

    $stmt->fetch(PDO::FETCH_BOUND);

    return array("mime" => $mime,
        "data" => $data);
}


public function get_movie_ratings(){
    /**
     *  Documentation Search Data
 * $config['table_name']='registration';
 Table columns to search for
   * $config['search_parameters'] = array('username','email','pt_pass','first_name') || string || comma separated str
   Where id = exec_value[0]
   * $config['where_parameters'] = array('id','id','id'); || empty string
   *$config['exec_values'][]=1;
   *$config['exec_values'][]=2;
   *$config['exec_values'][]=3;
   *$this->search_database($config);
     * 
     */
$config['table_name']= "movies";
$config['search_parameters']= array('title','genre','description','total_rates','stars','id');
$query = $this->conn->query($this->build_search_string($config));
if(false === $query){
    return false;
} 
$query->setFetchMode(PDO::FETCH_ASSOC);
$array=array();
while($r = $query->fetch()):
 $array[]=$r;
endwhile;
return $array;    
}

private function build_insert_string(&$config){
    
    $table_string = $this->create_table_string($config['array_insert']);
    $table_values = $this->create_insert_values($config['array_insert']);
    $string = "insert into {$config['table_name']} $table_string VALUES $table_values";
    return $string;
}
private function build_insert_string_prepared(&$config){
    
    $table_string = $this->create_table_string_prepared($config['array_table_column']);
    $table_values = $this->create_insert_values_prepared($config['array_table_column']);
    $string = "insert into {$config['table_name']} $table_string VALUES $table_values";
    return $string;
}
private function create_table_string_prepared($array){
    if(empty($array)){
        return false;
    }
    //$keys = array_keys($array);
    $string = "( {$array[0]}";
    for($c=1;$c<count($array);$c++){
        $string.=", {$array[$c]}";
    }
    $string.=")";
    return $string;
}

private function create_table_string($array){
    if(empty($array)){
        return false;
    }
    $keys = array_keys($array);
    $string = "( {$keys[0]}";
    for($c=1;$c<count($keys);$c++){
        $string.=", {$keys[$c]}";
    }
    $string.=")";
    return $string;
}
private function create_insert_values($array){
    if(empty($array)){
        return false;
    }
    $keys = array_values($array);
    $string = "( '{$keys[0]}'";
    for($c=1;$c<count($keys);$c++){
        $string.=", '{$keys[$c]}'";
    }
    $string.=")";
    return $string;
}

private function create_insert_values_prepared($array){
    if(empty($array)){
        return false;
    }
    //$keys = array_values($array);
    $string = "( ?";
    for($c=1;$c<count($array);$c++){
        $string.=", ?";
    }
    $string.=")";
    return $string;
}

public function build_search_string_prepared(&$config){
    if(!isset($config['where_parameters'])){
        $config['where_parameters']='';
    }
    return $this->search_param($config['search_parameters'])."from {$config['table_name']}".$this->generate_where_prepared($config['where_parameters']);
}

private function build_update_string_prepared(&$config){
    if(!isset($config['update_where'])){
        $config['update_where']='';
    }
 $upd_value = $this->generate_update_prepared($config['update_values']);
 $upd_where = $this->generate_where_prepared($config['update_where']);   
 return  "update {$config['table_name']} $upd_value $upd_where"; 
 
}

public function update_database(&$config){
    $sql=$this->conn->prepare($this->build_update_string_prepared($config));
    for($count = 0; $count<count($config['where_value']);$count++){
        array_push($config['updated_values'],$config['where_value'][$count]);
    }
    //$config['where_value'];
    return $sql->execute($config['updated_values']);
}
 public function test_u_dat(){
     $config['update_values'] = array('username','email');
     $config['update_where'] = array('id','id');
     $config['table_name'] = 'registration';
     $config['updated_values'] = array('freedom','freetone4ever@yahoo.com');
     $config['where_value'] = array(172,173); 
     $upd = $this->update_database($config);
     if($upd)
     echo "Updated successfully";
     else
     echo "Not updated";
 }

private function generate_where_prepared($where){
    if(is_null($where) || empty($where)){
        return "";
    }
    //$keys = array_keys($where);
    //$values = array_values($where);
    $init = " where {$where[0]} = ?";
    $arr[] = $where[0];
    for($count = 1; $count<count($where);$count++){
        $and_or = in_array($where[$count],$arr)?'or':'and';
        $init.= " $and_or {$where[$count]} = ?";
        $arr[] = $where[$count];
    }
    return $init;
}



public function test_upd_string(){
    $config['update_values'] = array('username','id','email');
    $config['update_where'] = array('username','id');
    $config['table_name']='registration';
    echo $this->update_database($config);
}



private function generate_update_prepared($where){
    if(is_null($where) || empty($where)){
        return false;
    }
    $init = " set {$where[0]} = ?";
    for($count = 1; $count<count($where);$count++){
        //$and_or = in_array($where[$count],$arr)?'or':'and';
        $init.= ",  {$where[$count]} = ?";
        
    }
    return $init;
}

public function build_search_string(&$config){
    if(!isset($config['where_parameters'])){
        $config['where_parameters']='';
    }
    return $this->search_param($config['search_parameters'])."from {$config['table_name']}".$this->generate_where($config['where_parameters']);
}
private function search_param($search){
 if(is_string($search)){
     return "select $search";
 }
 if(!is_array($search)){
    return false;
 }
 $init = "select {$search[0]}";
 for($count = 1; $count<count($search);$count++){
     $init.=", {$search[$count]}";
 }
 return $init.=" ";
}
private function generate_where($where){
    if(is_null($where) || empty($where)){
        return "";
    }
    $keys = array_keys($where);
    $values = array_values($where);
    $init = " where {$keys[0]} = '{$values[0]}'";
    for($count = 1; $count<count($keys);$count++){
        $init.= " and {$keys[$count]} = '{$values[$count]}'";
    }
    return $init;
}
public function populate_database(){
    $config['table_name']='registration';
    $array_firstname=array("John","Doe","Leo","Segun","Philip","Rodney","Max","Kellerman");
    $array_lastname=array("John","Doe","Leo","Segun","Philip","Rodney","Max","Kellerman");
    $array_username=array("john123","doe890","900leo","segun67","phil903","roddyp","maximuse","keller69");
    $array_email=array("John@gmail.com","Doe@gmail.com","Leo@gmail.com","Segun@gmail.com","Philip@gmail.com","Rodney@gmail.com","Max@gmail.com","Kellerman@gmail.com");
    $count = 1;
    $insert = true;
    while($count<21){
        $array['first_name']=$array_firstname[array_rand($array_firstname)];
        $array['last_name']=$array_lastname[array_rand($array_lastname)];
        $array['email']=$array_email[array_rand($array_email)];
        $array['username']=$array_username[array_rand($array_username)];
        $array['pt_pass']=$array_username[array_rand($array_username)];
        $array['password']=password_hash($array['pt_pass'],PASSWORD_BCRYPT);
        $config['array_insert']=$array;
        $config['table_name']='registration';
        $insert = $insert && $this->conn->exec($this->build_insert_string($config));
        $count++;
        
    }
    return $insert;
}

public function populate_database_prepared(){
    $config['table_name']='registration';
    $array_firstname=array("John","Doe","Leo","Segun","Philip","Rodney","Max","Kellerman");
    $array_lastname=array("John","Doe","Leo","Segun","Philip","Rodney","Max","Kellerman");
    $array_username=array("john123","doe890","900leo","segun67","phil903","roddyp","maximuse","keller69");
    $array_email=array("John@gmail.com","Doe@gmail.com","Leo@gmail.com","Segun@gmail.com","Philip@gmail.com","Rodney@gmail.com","Max@gmail.com","Kellerman@gmail.com");
    $countt = 1;
    $insert = true;
    while($countt<21){
        $array[]=$array_firstname[array_rand($array_firstname)];
        $array[]=$array_lastname[array_rand($array_lastname)];
        $array[]=$array_email[array_rand($array_email)];
        $array[]=$array_username[array_rand($array_username)];
        $hash=$array_username[array_rand($array_username)];
        //$hash = $array['pt_pass'];
        $array[]=password_hash($hash,PASSWORD_BCRYPT);
        $array[]=$hash;
        $config['array_table_value']=$array;
        $config['table_name']='registration';
        $config['array_table_column']=array('first_name','last_name','email','username','password','pt_pass');
        
        $ins = $this->insert_into_database($config);
        $insert = $insert && boolval($ins);
        $array=null;
        $countt++;
        
    }
    return $insert;
}

public function insert_into_database(&$config){
    $sql = $this->conn->prepare($this->build_insert_string_prepared($config));
    for($count=0;$count<count($config['array_table_value']);$count++){
        $sql->bindParam($count+1,$config['array_table_value'][$count]);
    }
    return $sql->execute();
}



public function test_search(){
    $config['table_name']='registration';
    $config['search_parameters'] = array('username','email','pt_pass','first_name');

   // $config['where_parameters'] = array('username'=>'leoneo','password'=>'victory');
   $query = $this->conn->query($this->build_search_string($config));
   if(false === $query){
       return false;
   } 
   $query->setFetchMode(PDO::FETCH_ASSOC);
   $array=array();
   while($r = $query->fetch()):
    $array[]=$r;
   endwhile;
   return $array;    
}

public function test_download($id){
    $config['table_name']='movies';
    $config['search_parameters'] = array('mime','data');

    $config['where_parameters'] = array('id'=>$id);
   $query = $this->conn->query($this->build_search_string($config));
   if(false === $query){
       return false;
   } 
   $query->setFetchMode(PDO::FETCH_ASSOC);
   $array=array();
   while($r = $query->fetch()):
    $array[]=$r;
   endwhile;
   return $array;    
}

public function search_database(&$config){
    $query = $this->conn->prepare($this->build_search_string_prepared($config));
    $query->execute($config['exec_values']);
   if(false === $query){
       return false;
   } 
   $query->setFetchMode(PDO::FETCH_ASSOC);
   $array=array();
   while($r = $query->fetch()):
    $array[]=$r;
   endwhile;
   return $array;  
}


public function test_search_prepared(){
    $config['table_name']='registration';
    $config['search_parameters'] = array('username','email','pt_pass','first_name');
    $config['where_parameters'] = array('id','id','id');
   $config['exec_values'][]=1;
   $config['exec_values'][]=2;
   $config['exec_values'][]=3;
   return $this->search_database($config);

}


public function write_ln($data){
    echo "$data <br>";
}
    
}
$test = new DB_Operations();
if(isset($_GET['video_id'])&&isset($_GET['rate'])){

    $rate_value = $_GET['rate']%5;
    if($rate_value === 0)
    $rate_value=5;

    /**
     *  Documentation Search Data
 * $config['table_name']='registration';
 Table columns to search for
   * $config['search_parameters'] = array('username','email','pt_pass','first_name') || string || comma separated str
   Where id = exec_value[0]
   * $config['where_parameters'] = array('id','id','id'); || empty string
   *$config['exec_values'][]=1;
   *$config['exec_values'][]=2;
   *$config['exec_values'][]=3;
   *return $this->search_database($config);
     */
    $config['table_name']='movie_ratings';
    $config['search_parameters']=array('username');
    $config['where_parameters']=array('username','movie_id');
    $config['exec_values'][]=$_SESSION['login_user'];
    $config['exec_values'][]=$_GET['video_id'];
    $num=0;
    if($test->search_database($config)){

        $config_u['update_values'] = array('rate_star');
        $config_u['update_where'] = array('movie_id','username');  $config_u['where_value'] = array($_GET['video_id'],$_SESSION['login_user']);
        $config_u['table_name'] = 'movie_ratings';
        $config_u['updated_values'] = array($rate_value);

        $upd = $test->update_database($config_u);
        if($upd){
            //star = total stars/number of rating
            $config_s['table_name']='movie_ratings';
            $config_s['search_parameters']=array('rate_star');
            $config_s['where_parameters']=array('movie_id');
            //$config['exec_values'][]=$_SESSION['login_user'];
            $config_s['exec_values'][]=$_GET['video_id'];
            $search = $test->search_database($config_s);
            $stars = 0;
            foreach($search as $res):
                $stars+=$res['rate_star'];
                $num +=1;
            endforeach;
            //$stars+=$rate_value;
            $val = $stars/$num;
            $final_star = intval($val);


            $config_u1['update_values'] = array('stars');
            $config_u1['update_where'] = array('id');  $config_u1['where_value'] = array($_GET['video_id']);
            $config_u1['table_name'] = 'movies';
            $config_u1['updated_values'] = array($final_star);
    
            $upd = $test->update_database($config_u1);
            if($upd)
            echo "good";
            else
            echo "bad";
        }
        //get all the values and do the maths

    }else{



        


            $config_s['table_name']='movie_ratings';
            $config_s['search_parameters']=array('rate_star');
            $config_s['where_parameters']=array('movie_id');
            //$config['exec_values'][]=$_SESSION['login_user'];
            $config_s['exec_values'][]=$_GET['video_id'];
            $search = $test->search_database($config_s);

            $stars = 0;
            $num=0;
            if($search){
                
            foreach($search as $res):
                $stars+=$res['rate_star'];
                $num +=1;
            endforeach;
            $stars+=$rate_value;
            $num+=1;
            $val = $stars/$num;
            $final_star = intval($val);
            //$num+=1;

            $config_u['update_values'] = array('total_rates','stars');
        $config_u['update_where'] = array('id');  $config_u['where_value'] = array($_GET['video_id']);
        $config_u['table_name'] = 'movies';
        $config_u['updated_values'] = array($num,$final_star);
        $upd = $test->update_database($config_u);

        
$array[]=$_GET['video_id'];$array[]=$_SESSION['login_user'];$array[]=$rate_value;
$configg['table_name']='movie_ratings';
$configg['array_table_value']=$array;
$configg['array_table_column']=array('movie_id','username','rate_star');
$ins = $test->insert_into_database($configg);
        if($ins && $upd){
            echo "good";
        }else{
            echo "bad";
        }

            }else{
                $num = 1;

                $config_u['update_values'] = array('total_rates','stars');
        $config_u['update_where'] = array('id');  $config_u['where_value'] = array($_GET['video_id']);
        $config_u['table_name'] = 'movies';
        $config_u['updated_values'] = array($num,$rate_value);
        $upd = $test->update_database($config_u);

/**
 * * Documentation Inserting data
 Values to insert 
 * $config['array_table_value']=$array; form-> $array[] = value
    *$config['table_name']='registration';
    Table columns 
    *$config['array_table_column']=array('first_name','last_name','email','username','password','pt_pass');
        *$ins = $this->insert_into_database($config);
 * 
 */
$array[]=$_GET['video_id'];$array[]=$_SESSION['login_user'];$array[]=$rate_value;
$configg['table_name']='movie_ratings';
$configg['array_table_value']=$array;
$configg['array_table_column']=array('movie_id','username','rate_star');
$ins = $test->insert_into_database($configg);
        if($ins && $upd){
            echo "good";
        }else{
            echo "bad";
        }


            }
            


        /**
         * Documentation Updating data
 update table set username to new_value and email to new value 
 * $config['update_values'] = array('username','email');
                                    where id = 172 and id = 173
     *$config['update_where'] = array('id','id');  *$config['where_value'] = array(172,173); 
    * $config['table_name'] = 'registration';
 the new values to update the database with  -> username = freedom email = freetone4life@yahoo.com
     *$config['updated_values'] = array('freedom','freetone4ever@yahoo.com');

        update the table     
     *$upd = $this->update_database($config);
         * 
         */
        
    }
}



/**
         * check the rate table to check if the user has rated before
         * if yes=> update rate
         * if no=>add to rate
         * update rate => update rate table with new user input
         * call the rates data(total rates and each stars per rate)
         * update movie table with new value
         * 
*/


/**
 * Documentation Updating data
 update table set username to new_value and email to new value 
 * $config['update_values'] = array('username','email');
                                    where id = 172 and id = 173
     *$config['update_where'] = array('id','id');  *$config['where_value'] = array(172,173); 
    * $config['table_name'] = 'registration';
 the new values to update the database with  -> username = freedom email = freetone4life@yahoo.com
     *$config['updated_values'] = array('freedom','freetone4ever@yahoo.com');

        update the table     
     *$upd = $this->update_database($config);
 * 
 * 
 * 
 * Documentation Inserting data
 Values to insert 
 * $config['array_table_value']=$array; form-> $array[] = value
    *$config['table_name']='registration';
    Table columns 
    *$config['array_table_column']=array('first_name','last_name','email','username','password','pt_pass');
        *$ins = $this->insert_into_database($config);
 * 
 *
 * Documentation Search Data
 *  Documentation Search Data
 * $config['table_name']='registration';
 Table columns to search for
   * $config['search_parameters'] = array('username','email','pt_pass','first_name') || string || comma separated str
   Where id = exec_value[0]
   * $config['where_parameters'] = array('id','id','id'); || empty string
   *$config['exec_values'][]=1;
   *$config['exec_values'][]=2;
   *$config['exec_values'][]=3;
   *return $this->search_database($config);
     * 
 *  
 * 
 */


?>