<?php 
session_start();
include_once('db_configuration.php');
class DB_Operations extends db_config{
private $conn;    
public function __construct(){
   try{
    $this->conn = new PDO("mysql:host=$this->host;dbname=$this->db","$this->username",$this->password);
    //echo "Connected successfully";
}catch(PDOException $ex){
    die("Could not connect ".$ex->getMessage());
}
}


public function insertBlob($username,$title,$genre,$description,$filePath, $mime) {
    $blob = fopen($filePath, 'rb');

    $sql = "INSERT INTO movies(username,title,genre,description,mime,data) VALUES(:username,:title,:genre,:description,:mime,:data)";
    $stmt = $this->conn->prepare($sql);
    $stmt->bindParam(':username', $username);
    $stmt->bindParam(':title', $title);
    $stmt->bindParam(':genre', $genre);
    $stmt->bindParam(':description', $description);
    $stmt->bindParam(':mime', $mime);
    $stmt->bindParam(':data', $blob, PDO::PARAM_LOB);

    return $stmt->execute();
}

/**
 * update the files table with the new blob from the file specified
 * by the filepath
 * @param int $id
 * @param string $filePath
 * @param string $mime
 * @return bool
 */
function updateBlob($id, $filePath, $mime) {

    $blob = fopen($filePath, 'rb');

    $sql = "UPDATE files
            SET mime = :mime,
                data = :data
            WHERE id = :id;";

    $stmt = $this->pdo->prepare($sql);

    $stmt->bindParam(':mime', $mime);
    $stmt->bindParam(':data', $blob, PDO::PARAM_LOB);
    $stmt->bindParam(':id', $id);

    return $stmt->execute();
}

/**
 * select data from the the files
 * @param int $id
 * @return array contains mime type and BLOB data
 */
public function selectBlob($id) {

    $sql = "SELECT mime,
                    data
               FROM files
              WHERE id = :id;";

    $stmt = $this->pdo->prepare($sql);
    $stmt->execute(array(":id" => $id));
    $stmt->bindColumn(1, $mime);
    $stmt->bindColumn(2, $data, PDO::PARAM_LOB);

    $stmt->fetch(PDO::FETCH_BOUND);

    return array("mime" => $mime,
        "data" => $data);
}




private function build_insert_string(&$config){
    
    $table_string = $this->create_table_string($config['array_insert']);
    $table_values = $this->create_insert_values($config['array_insert']);
    $string = "insert into {$config['table_name']} $table_string VALUES $table_values";
    return $string;
}
private function build_insert_string_prepared(&$config){
    
    $table_string = $this->create_table_string_prepared($config['array_table_column']);
    $table_values = $this->create_insert_values_prepared($config['array_table_column']);
    $string = "insert into {$config['table_name']} $table_string VALUES $table_values";
    return $string;
}
private function create_table_string_prepared($array){
    if(empty($array)){
        return false;
    }
    //$keys = array_keys($array);
    $string = "( {$array[0]}";
    for($c=1;$c<count($array);$c++){
        $string.=", {$array[$c]}";
    }
    $string.=")";
    return $string;
}

private function create_table_string($array){
    if(empty($array)){
        return false;
    }
    $keys = array_keys($array);
    $string = "( {$keys[0]}";
    for($c=1;$c<count($keys);$c++){
        $string.=", {$keys[$c]}";
    }
    $string.=")";
    return $string;
}
private function create_insert_values($array){
    if(empty($array)){
        return false;
    }
    $keys = array_values($array);
    $string = "( '{$keys[0]}'";
    for($c=1;$c<count($keys);$c++){
        $string.=", '{$keys[$c]}'";
    }
    $string.=")";
    return $string;
}

private function create_insert_values_prepared($array){
    if(empty($array)){
        return false;
    }
    //$keys = array_values($array);
    $string = "( ?";
    for($c=1;$c<count($array);$c++){
        $string.=", ?";
    }
    $string.=")";
    return $string;
}

public function build_search_string_prepared(&$config){
    if(!isset($config['where_parameters'])){
        $config['where_parameters']='';
    }
    return $this->search_param($config['search_parameters'])."from {$config['table_name']}".$this->generate_where_prepared($config['where_parameters']);
}

private function build_update_string_prepared(&$config){
    if(!isset($config['update_where'])){
        $config['update_where']='';
    }
 $upd_value = $this->generate_update_prepared($config['update_values']);
 $upd_where = $this->generate_where_prepared($config['update_where']);   
 return  "update {$config['table_name']} $upd_value $upd_where"; 
 
}

public function update_database(&$config){
    $sql=$this->conn->prepare($this->build_update_string_prepared($config));
    for($count = 0; $count<count($config['where_value']);$count++){
        array_push($config['updated_values'],$config['where_value'][$count]);
    }
    //$config['where_value'];
    return $sql->execute($config['updated_values']);
}
 public function test_u_dat(){
     $config['update_values'] = array('username','email');
     $config['update_where'] = array('id','id');
     $config['table_name'] = 'registration';
     $config['updated_values'] = array('freedom','freetone4ever@yahoo.com');
     $config['where_value'] = array(172,173); 
     $upd = $this->update_database($config);
     if($upd)
     echo "Updated successfully";
     else
     echo "Not updated";
 }

private function generate_where_prepared($where){
    if(is_null($where) || empty($where)){
        return "";
    }
    //$keys = array_keys($where);
    //$values = array_values($where);
    $init = " where {$where[0]} = ?";
    $arr[] = $where[0];
    for($count = 1; $count<count($where);$count++){
        $and_or = in_array($where[$count],$arr)?'or':'and';
        $init.= " $and_or {$where[$count]} = ?";
        $arr[] = $where[$count];
    }
    return $init;
}



public function test_upd_string(){
    $config['update_values'] = array('username','id','email');
    $config['update_where'] = array('username','id');
    $config['table_name']='registration';
    echo $this->update_database($config);
}



private function generate_update_prepared($where){
    if(is_null($where) || empty($where)){
        return false;
    }
    $init = " set {$where[0]} = ?";
    for($count = 1; $count<count($where);$count++){
        //$and_or = in_array($where[$count],$arr)?'or':'and';
        $init.= ",  {$where[$count]} = ?";
        
    }
    return $init;
}

public function build_search_string(&$config){
    if(!isset($config['where_parameters'])){
        $config['where_parameters']='';
    }
    return $this->search_param($config['search_parameters'])."from {$config['table_name']}".$this->generate_where($config['where_parameters']);
}
private function search_param($search){
 if(is_string($search)){
     return "select $search";
 }
 if(!is_array($search)){
    return false;
 }
 $init = "select {$search[0]}";
 for($count = 1; $count<count($search);$count++){
     $init.=", {$search[$count]}";
 }
 return $init.=" ";
}
private function generate_where($where){
    if(is_null($where) || empty($where)){
        return "";
    }
    $keys = array_keys($where);
    $values = array_values($where);
    $init = " where {$keys[0]} = '{$values[0]}'";
    for($count = 1; $count<count($keys);$count++){
        $init.= " and {$keys[$count]} = '{$values[$count]}'";
    }
    return $init;
}
public function populate_database(){
    $config['table_name']='registration';
    $array_firstname=array("John","Doe","Leo","Segun","Philip","Rodney","Max","Kellerman");
    $array_lastname=array("John","Doe","Leo","Segun","Philip","Rodney","Max","Kellerman");
    $array_username=array("john123","doe890","900leo","segun67","phil903","roddyp","maximuse","keller69");
    $array_email=array("John@gmail.com","Doe@gmail.com","Leo@gmail.com","Segun@gmail.com","Philip@gmail.com","Rodney@gmail.com","Max@gmail.com","Kellerman@gmail.com");
    $count = 1;
    $insert = true;
    while($count<21){
        $array['first_name']=$array_firstname[array_rand($array_firstname)];
        $array['last_name']=$array_lastname[array_rand($array_lastname)];
        $array['email']=$array_email[array_rand($array_email)];
        $array['username']=$array_username[array_rand($array_username)];
        $array['pt_pass']=$array_username[array_rand($array_username)];
        $array['password']=password_hash($array['pt_pass'],PASSWORD_BCRYPT);
        $config['array_insert']=$array;
        $config['table_name']='registration';
        $insert = $insert && $this->conn->exec($this->build_insert_string($config));
        $count++;
        
    }
    return $insert;
}

public function populate_database_prepared(){
    $config['table_name']='registration';
    $array_firstname=array("John","Doe","Leo","Segun","Philip","Rodney","Max","Kellerman");
    $array_lastname=array("John","Doe","Leo","Segun","Philip","Rodney","Max","Kellerman");
    $array_username=array("john123","doe890","900leo","segun67","phil903","roddyp","maximuse","keller69");
    $array_email=array("John@gmail.com","Doe@gmail.com","Leo@gmail.com","Segun@gmail.com","Philip@gmail.com","Rodney@gmail.com","Max@gmail.com","Kellerman@gmail.com");
    $countt = 1;
    $insert = true;
    while($countt<21){
        $array[]=$array_firstname[array_rand($array_firstname)];
        $array[]=$array_lastname[array_rand($array_lastname)];
        $array[]=$array_email[array_rand($array_email)];
        $array[]=$array_username[array_rand($array_username)];
        $hash=$array_username[array_rand($array_username)];
        //$hash = $array['pt_pass'];
        $array[]=password_hash($hash,PASSWORD_BCRYPT);
        $array[]=$hash;
        $config['array_table_value']=$array;
        $config['table_name']='registration';
        $config['array_table_column']=array('first_name','last_name','email','username','password','pt_pass');
        
        $ins = $this->insert_into_database($config);
        $insert = $insert && boolval($ins);
        $array=null;
        $countt++;
        
    }
    return $insert;
}

public function insert_into_database(&$config){
    $sql = $this->conn->prepare($this->build_insert_string_prepared($config));
    for($count=0;$count<count($config['array_table_value']);$count++){
        $sql->bindParam($count+1,$config['array_table_value'][$count]);
    }
    return $sql->execute();
}



public function test_search(){
    $config['table_name']='registration';
    $config['search_parameters'] = array('username','email','pt_pass','first_name');

   // $config['where_parameters'] = array('username'=>'leoneo','password'=>'victory');
   $query = $this->conn->query($this->build_search_string($config));
   if(false === $query){
       return false;
   } 
   $query->setFetchMode(PDO::FETCH_ASSOC);
   $array=array();
   while($r = $query->fetch()):
    $array[]=$r;
   endwhile;
   return $array;    
}

public function test_download($id){
    $config['table_name']='movies';
    $config['search_parameters'] = array('mime','data');

    $config['where_parameters'] = array('id'=>$id);
   $query = $this->conn->query($this->build_search_string($config));
   if(false === $query){
       return false;
   } 
   $query->setFetchMode(PDO::FETCH_ASSOC);
   $array=array();
   while($r = $query->fetch()):
    $array[]=$r;
   endwhile;
   return $array;    
}

public function search_database(&$config){
    $query = $this->conn->prepare($this->build_search_string_prepared($config));
    $query->execute($config['exec_values']);
   if(false === $query){
       return false;
   } 
   $query->setFetchMode(PDO::FETCH_ASSOC);
   $array=array();
   while($r = $query->fetch()):
    $array[]=$r;
   endwhile;
   return $array;  
}


public function test_search_prepared(){
    $config['table_name']='registration';
    $config['search_parameters'] = array('username','email','pt_pass','first_name');
    $config['where_parameters'] = array('id','id','id');
   $config['exec_values'][]=1;
   $config['exec_values'][]=2;
   $config['exec_values'][]=3;
   return $this->search_database($config);
   /*$query = $this->conn->prepare($this->build_search_string_prepared($config));
   $query->execute($config['exec_values']);
   if(false === $query){
       return false;
   } 
   $query->setFetchMode(PDO::FETCH_ASSOC);
   $array=array();
   while($r = $query->fetch()):
    $array[]=$r;
   endwhile;
   return $array;*/   
}


public function write_ln($data){
    echo "$data <br>";
}
    
}
$test = new DB_Operations();
if(empty($_POST['id_v'][0])){
//header('videos.php');    
}
$id = $_GET['id_v'];
//echo $id;
$array = $test->test_download($id);
foreach($array as $arr){
$mime = $arr['mime'];
$file = $arr['data'];    
header("Content-type: $mime");
//ob_clean();
//flush();
echo $file;
//header("Content-Disposition: attachment; filename=$file");
//ob_clean();
//flush();
//echo $content;
}
/*
$file = $_FILES['video']['name'];
$extension = check_extension($file);
if(check_acceptable_extensions($extension,array('mp4','avi','3gp'))){
// $size = $_FILES['video']['size'];
 if($_FILES['video']['size']>10000000){
  echo "large file";   
 }else{
     $error = $_FILES['video']['error'];
     if($error > 0){
         switch($error){
            case 1:
            echo 'ini error';
            break;
            case 3:
            echo 'network error';
            break;
            case 4:
            echo 'no file error';
            break;
            default:
            echo 'php error';
            //break;
         }
     }else{/*
         $name = return_hash($_SESSION['login_user']);
$result=move_uploaded_file($_FILES['video']['tmp_name'],$_SERVER['DOCUMENT_ROOT']."/"."video_uploads/".$name.'.'.$extension);
        $link = $_SERVER['DOCUMENT_ROOT']."/"."video_uploads/".$name.'.'.$extension;
        if($result == 1){
        $array =array($_SESSION['login_user'],$_POST['movie_title'],$_POST['movie_genre'],$_POST['movie_description'],$link);
        $config['array_table_value']=$array;
        $config['table_name']='movies';
        $config['array_table_column']=array('username','title','genre','description','download_link');
        if($test->insert_into_database($config)){
            $_SESSION['update_reply']='<div class="succ_div ">
            <div class="succ_details text-center featurette-heading font-weight-normal"> Movie information uploaded and saved successfully </div>
        </div>';
            echo "successful";
        }
        
        else
        echo 'failed insert';
        }else{
            echo 'error '.$_FILES['video']['error'];
        }
    */
    //insertBlob($username,$title,$genre,$description,$filePath, $mime)
    //$_SESSION['login_user'],$_POST['movie_title'],$_POST['movie_genre'],$_POST['movie_description']
  /*      if($test->insertBlob($_SESSION['login_user'],$_POST['movie_title'],$_POST['movie_genre'],$_POST['movie_description'],$_FILES['video']['tmp_name'],$_FILES['video']['type'])){
            $_SESSION['update_reply']='<div class="succ_div ">
            <div class="succ_details text-center featurette-heading font-weight-normal"> Movie information uploaded and saved successfully </div>
        </div>';
        echo 'successful';
        }else{
        echo 'failed insert';
        }
}
 }   
}else{
 echo "invalid extension $extension";   
}
/*$array =array($_POST['name'],$_POST['mat_no'],$_POST['link']);
$config['array_table_value']=$array;
$config['table_name']='test_table';
$config['array_table_column']=array('name','mat_no','link');
$count = 0;
$name1 = $_FILES['video']['name'];
$name2 = $_FILES['image']['name'];
$ext = check_extension($name1);
$ext1 = check_extension($name2);*/
//$ext1 = end(explode('.',$name2));





/*if($test->insert_into_database($config))
echo "$ext and $ext1 extension file(s) uploaded successfully";
else
echo 'Failed';

//$test->test_u_dat();
//if($test->populate_database_prepared())
//echo "Data inserted successfully";
/*if(false !== $test->test_search_prepared()){
    //while($res = $test->test_search()):
        foreach($test->test_search_prepared() as $tes){
            $test->write_ln($tes['username']."    ".$tes['email']."    ".$tes['pt_pass']."   ".$tes['first_name']);
        }
      
    //endwhile;
    //print_r($test->test_search());
}*/
//$config['table_name']='registration';
//$config['search_parameters'] = array('username','email','password','first_name');
//$config['where_parameters'] = array('username'=>'leoneo','password'=>'victory');
//echo $test->build_search_string($config);
//if($test->populate_database())
//echo "20 random rows of data inserted successfully";
//else
//echo "Oooops there was an uncaught error somewhere";
function check_extension($file_name){
    //if(is_file($file_name))
    return pathinfo($file_name,PATHINFO_EXTENSION);
    //else
   // return "Invalid";
}
function check_acceptable_extensions($extension,$array){
    return in_array(strtolower($extension),$array); 
}
function check_error($key){
    $array = array("Size of uploaded file is greater than what the server permits");
}
function return_hash($value){
    $value = str_replace(' ','',password_hash($value,PASSWORD_BCRYPT).microtime());
    $value = str_replace('/','',$value);
    $value = str_replace('\\','',$value); 
    $value = str_replace('.','',$value);
    $value = str_replace(',','',$value); 
    $value = str_replace('$2y$10$','',$value);
    return strtolower($value);
}
/**
 * verify file extension and size
 * check for upload errors and send appropriate messages to client
 * check if movie title exists with username
 * move the file to from temp to final and rename accordingly
 * set session data for reply
 * 
 * 
 * Env variable cloudinary CLOUDINARY_URL=cloudinary://675734373938478:GrxFHel9KdRDHrZaD5Zu71V5gV4@freetone
 * API secret GrxFHel9KdRDHrZaD5Zu71V5gV4
 * API key 675734373938478
 * Cloud name fretone
 * 
 */

?>