//remove_add_attr_many(attr_name,attr_value,element,isAdd)
remove_add_attr_many("disabled","disabled",".show_hide",true);
//remove_add_attr_one("disabled","disabled","#show_img",true);
remove_add_css_many("display","none",".pro_hide");
opq_val=1;
prog_val=100;
/*$("#prog").attr({
    "style":function(){
        return "width:70%";
    }
});
$("#prog").html("70%");*/ 
c_prog=0;
c_prog_value=0;
//$("#prog_container").css("display","none");



function shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id){
    var hiddenInt=0;
    var opq_val_hidden = 1;
    c_prog_hidden=0;
    c_prog_value_hidden=0;
    remove_add_attr_one("disabled","disabled","#"+current_element,true);    
    $("#"+progress_container_outer).css("display","block");    
    c_prog_hidden=0;    
    $("#"+progress_container_inner).attr({
        "style":function(){
            return "width:"+c_prog+"%";
        }
    });
    $("#"+progress_container_inner).html(c_prog_hidden+"%");    
    hiddenInt=setInterval(function(){
    $("#"+image_id).css("opacity",function(ind,curr_val){
    if(opq_val_hidden<=0){
    c_prog_hidden=100;
    c_prog_value_hidden=1;
    remove_add_attr_one("disabled","","#"+other_element,false);    
    clearInterval(hiddenInt);    
    }
    c_prog_hidden=Math.ceil((c_prog_value_hidden+=0.1)*100);    
    if(c_prog_hidden<=100){    
    $("#"+progress_container_inner).attr({
        "style":function(){
            return "width:"+c_prog_hidden+"%";
        }
    });
    $("#"+progress_container_inner).html(c_prog_hidden+"%");        
    }
    return opq_val_hidden-=0.1;    
    });    
    },100); 
}

var shade = function(){
remove_add_attr_one("disabled","disabled","#hide_img",true);    
$("#prog_container").css("display","block");    
c_prog=0;    
$("#prog").attr({
    "style":function(){
        return "width:"+c_prog+"%";
    }
});
$("#prog").html(c_prog+"%");    
hideInt=setInterval(function(){
$("#img_opq").css("opacity",function(ind,curr_val){
if(opq_val<=0){
c_prog=100;
c_prog_value=1;
remove_add_attr_one("disabled","","#show_img",false);    
clearInterval(hideInt);    
}
c_prog=Math.ceil((c_prog_value+=0.1)*100);    
if(c_prog<=100){    
$("#prog").attr({
    "style":function(){
        return "width:"+c_prog+"%";
    }
});
$("#prog").html(c_prog+"%");        
}
return opq_val-=0.1;    
});    
},100);    
}


function unshaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id){
    c_prog_value_shown=1;
    opq_val_shown=0;
    unhiddenInt = 0;    
    remove_add_attr_one("disabled","disabled","#"+current_element,true);     
    unhiddenInt = setInterval(function(){
    $("#"+image_id).css("opacity",function(ind,curr_val){
    //console.log(curr_val);
    if(opq_val_shown>1){
    //opq_val_shown=1;    
    //c_prog_shown=10;
    //c_prog_value_=0.1;    
    $("#"+progress_container_outer).css("display","none");    
    remove_add_attr_one("disabled","","#"+other_element,false);    
    clearInterval(unhiddenInt);    
    }
    c_prog_shown=Math.floor((c_prog_value_shown-=0.1)*100);
    //c_prog=Math.ceil((opq_val-0.1)*100);    
    if(c_prog_shown>=0){    
    $("#"+progress_container_inner).attr({
        "style":function(){
            return "width:"+c_prog_shown+"%";
        }
    });
    $("#"+progress_container_inner).html(c_prog_shown+"%");        
    }    
    return opq_val_shown+=0.1;    
    })    
    },200)
}



var unshade = function(){
remove_add_attr_one("disabled","disabled","#show_img",true);     
c_prog_value=1;    
unhideInt = setInterval(function(){
//console.log("c_prog value is "+c_prog);
//console.log("c_prog_value value is "+c_prog_value);        
$("#img_opq").css("opacity",function(ind,curr_val){
//console.log(curr_val);
if(opq_val>1){
opq_val=1;    
c_prog=10;
c_prog_value=0.1;    
$("#prog_container").css("display","none");    
remove_add_attr_one("disabled","","#hide_img",false);    
clearInterval(unhideInt);    
}
c_prog=Math.floor((c_prog_value-=0.1)*100);
//c_prog=Math.ceil((opq_val-0.1)*100);    
if(c_prog>=0){    
$("#prog").attr({
    "style":function(){
        return "width:"+c_prog+"%";
    }
});
$("#prog").html(c_prog+"%");        
}    
return opq_val+=0.1;    
})    
},200)    
    
}
var hideInt = 0;
var unhideInt = 0;




$("#hide_img").click(function(){
//shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
shaded("hide_img","show_img","prog_container","prog","img_opq");
});
$("#show_img").click(function(){
 //unshade();   
 unshaded("show_img","hide_img","prog_container","prog","img_opq"); 
});

$("#hide_img1").click(function(){
    //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
    shaded("hide_img1","show_img1","prog_container1","prog1","img_opq1");
    });
    $("#show_img1").click(function(){
     //unshade();   
     unshaded("show_img1","hide_img1","prog_container1","prog1","img_opq1"); 
    });

    $("#hide_img2").click(function(){
        //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
        shaded("hide_img2","show_img2","prog_container2","prog2","img_opq2");
        });
        $("#show_img2").click(function(){
         //unshade();   
         unshaded("show_img2","hide_img2","prog_container2","prog2","img_opq2"); 
        });

        $("#hide_img3").click(function(){
            //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
            shaded("hide_img3","show_img3","prog_container3","prog3","img_opq3");
            });
            $("#show_img3").click(function(){
             //unshade();   
             unshaded("show_img3","hide_img3","prog_container3","prog3","img_opq3"); 
            });        
$("#hide_img4").click(function(){
                //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
shaded("hide_img4","show_img4","prog_container4","prog4","img_opq4");
                });
$("#show_img4").click(function(){
                 //unshade();   
unshaded("show_img4","hide_img4","prog_container4","prog4","img_opq4"); 
                });                    
                $("#hide_img5").click(function(){
                    //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
    shaded("hide_img5","show_img5","prog_container5","prog5","img_opq5");
                    });
    $("#show_img5").click(function(){
                     //unshade();   
    unshaded("show_img5","hide_img5","prog_container5","prog5","img_opq5"); 
                    });                                  

                    $("#hide_img6").click(function(){
                        //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
        shaded("hide_img6","show_img6","prog_container6","prog6","img_opq6");
                        });
        $("#show_img6").click(function(){
                         //unshade();   
        unshaded("show_img6","hide_img6","prog_container6","prog6","img_opq6"); 
                        });      
                        $("#hide_img7").click(function(){
                            //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
            shaded("hide_img7","show_img7","prog_container7","prog7","img_opq7");
                            });
            $("#show_img7").click(function(){
                             //unshade();   
            unshaded("show_img7","hide_img7","prog_container7","prog7","img_opq7"); 
                            }); 
                            $("#hide_img8").click(function(){
                                //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
                shaded("hide_img8","show_img8","prog_container8","prog8","img_opq8");
                                });
                $("#show_img8").click(function(){
                                 //unshade();   
                unshaded("show_img8","hide_img8","prog_container8","prog8","img_opq8"); 
                                });      

                                
                                $("#hide_img9").click(function(){
                                    //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
                    shaded("hide_img9","show_img9","prog_container9","prog9","img_opq9");
                                    });
                    $("#show_img9").click(function(){
                                     //unshade();   
                    unshaded("show_img9","hide_img9","prog_container9","prog9","img_opq9"); 
                                    });      

                                    $("#hide_img10").click(function(){
                                        //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
                        shaded("hide_img10","show_img10","prog_container10","prog10","img_opq10");
                                        });
                        $("#show_img10").click(function(){
                                         //unshade();   
                        unshaded("show_img10","hide_img10","prog_container10","prog10","img_opq10"); 
                                        });                                     

                                        $("#hide_img11").click(function(){
                                            //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
                            shaded("hide_img11","show_img11","prog_container11","prog11","img_opq11");
                                            });
                            $("#show_img11").click(function(){
                                             //unshade();   
                            unshaded("show_img11","hide_img11","prog_container11","prog11","img_opq11"); 
                                            });       

                                            $("#hide_img12").click(function(){
                                                //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
                                shaded("hide_img12","show_img12","prog_container12","prog12","img_opq12");
                                                });
                                $("#show_img12").click(function(){
                                                 //unshade();   
                                unshaded("show_img12","hide_img12","prog_container12","prog12","img_opq12"); 
                                                });                                                                                 
    
                                                $("#hide_img13").click(function(){
                                                    //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
                                    shaded("hide_img13","show_img13","prog_container13","prog13","img_opq13");
                                                    });
                                    $("#show_img13").click(function(){
                                                     //unshade();   
                                    unshaded("show_img13","hide_img13","prog_container13","prog13","img_opq13"); 
                                                    });       
                                                    
                                                    
$("#hide_img14").click(function(){
                //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
shaded("hide_img14","show_img14","prog_container14","prog14","img_opq14");
                });
$("#show_img14").click(function(){
                    //unshade();   
unshaded("show_img14","hide_img14","prog_container14","prog14","img_opq14"); 
                });
$("#hide_img15").click(function(){
                        //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
        shaded("hide_img15","show_img15","prog_container15","prog15","img_opq15");
                        });
        $("#show_img15").click(function(){
                            //unshade();   
        unshaded("show_img15","hide_img15","prog_container15","prog15","img_opq15"); 
                        });

                        $("#hide_img16").click(function(){
                            //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
            shaded("hide_img16","show_img16","prog_container16","prog16","img_opq16");
                            });
            $("#show_img16").click(function(){
                                //unshade();   
            unshaded("show_img16","hide_img16","prog_container16","prog16","img_opq16"); 
                            });       
                            
                            $("#hide_img17").click(function(){
                                //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
                shaded("hide_img17","show_img17","prog_container17","prog17","img_opq17");
                                });
                $("#show_img17").click(function(){
                                    //unshade();   
                unshaded("show_img17","hide_img17","prog_container17","prog17","img_opq17"); 
                                });       
                                
                                $("#hide_img18").click(function(){
                                    //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
                    shaded("hide_img18","show_img18","prog_container18","prog18","img_opq18");
                                    });
                    $("#show_img18").click(function(){
                                        //unshade();   
                    unshaded("show_img18","hide_img18","prog_container18","prog18","img_opq18"); 
                                    });       
                                    
                                    $("#hide_img19").click(function(){
                                        //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
                        shaded("hide_img19","show_img19","prog_container19","prog19","img_opq19");
                                        });
                        $("#show_img19").click(function(){
                                            //unshade();   
                        unshaded("show_img19","hide_img19","prog_container19","prog19","img_opq19"); 
                                        });       
                                        
                                        $("#hide_img20").click(function(){
                                            //shade();   shaded(current_element,other_element,progress_container_outer,progress_container_inner,image_id)
                            shaded("hide_img20","show_img20","prog_container20","prog20","img_opq20");
                                            });
                            $("#show_img20").click(function(){
                                                //unshade();   
                            unshaded("show_img20","hide_img20","prog_container20","prog20","img_opq20"); 
                                            });                                        

    /*var obj=jQuery("script",document);
console.log("There are ",obj.length," Javascript files in this document");
console.log("Selector used was ",obj.selector);
console.log("The search context is ",obj.context);
console.log("The jquery version number is, Version ",obj.jquery);
var arr=obj.toArray();
for(var i=0;i<arr.length;i++)
    console.log(arr[i]);

arr.forEach(element => {
    console.log(element.src);
});   
jQuery("script",document).each(function(index,element){
console.log("Number "+(index+1)+" script's source is "+element.src);
}) 
$("a").each(function(idx){
    //$(this).prepend(idx+": ");
    var cl = $(this).attr("class");
    //console.log($(this).attr("href"));
    
    if(this.id === "last")
    return false;
})
$(".pag").each(function(index,element){
    loc=location.href;
    $(this).attr({"href":loc+"#"+(index+1)});
  //  $(this).css({"display":"none"});
    console.log($(this).attr("href"));
})
//pageYOffset
*/
pageSize=window.pageYOffset;
pageSize1=window.pageYOffset;
//window.on
var w = $(window);
//alert("Window height is "+w.height()+" and scroll position is "+window.pageYOffset);
function scroll_up(n){
var w = $(window); // Wrap the window in a jQuery object
var pagesize = w.height(); // Get the size of a page
var current = w.scrollTop(); // Get the current scrollbar position
w.scrollTop(n);
}
//window.ons
window.onscroll=function(){
//console.log(pageYOffset);    
//pageDown=pageYOffset;
    
}

/*var setI=setInterval(function(){
pageSize-=200;
scroll_up(pageSize);
console.log(pageSize);    
if(pageSize<=0){
var w = $(window);    
var pSize = w.height(); // Get the size of a page
w.scrollTop(pSize1*10)    
pageSize=window.pageYOffset;
}
    //clearInterval(setI);
},1000);*/
$("img.card-img-top").each(function(){
    //$(this).css("display","none");
})
pageUp=0;
pageDown=0;
count1=1;
sUP=0;sDown=0
var scrollUP = function(){
var pos_var= s1.offset(); 
var speed = $("#scroll_up_select").val();    
s1.attr("disabled","disabled");    
s.attr("disabled","disabled");     
sUP=setInterval(function(){
scroll_up(pageYOffset-20);
if(pageYOffset<=0){
count1*=20;    
pos_var.top+=count1;    
s1.offset(pos_var);      
clearInterval(sUP);
count1=0;    
s.removeAttr("disabled");    
s1.removeAttr("disabled");        
clearInterval(intCl);     
s1.css({"border-radius":function(id,elem){
                return "20px";
            },"background-color":"black","color":"white","opacity":1})     
}
pos_var.top-=20;
count1+=1;    
s1.offset(pos_var);    
//console.log("interval is "+sUP);    
},speed);    
}

scrollCheck = 0;
scrollVar = 0;

var s=$("#scroll_down");
var s1=$("#scroll_up");
//s.attr("readonly","readonly");
var pos_fixed = s.offset();
var fixed_top=pos_fixed.top;
//console.log("Final fixed pos is "+fixed_top);
//console.log("window width is "+$(window).width());
count = 1;
//console.log("Fixed pos is "+fixed_top);    
//var pos_var= s.offset();
var scrollDOWN = function(){
var pos_var= s.offset();    
s.attr("disabled","disabled");    
s1.attr("disabled","disabled"); 
var speed = $("#scroll_down_select").val();       
sDown=setInterval(function(){
scroll_up(pageYOffset+20);
scrollVar=pageYOffset;    
if(scrollVar==scrollCheck){
count*=20;    
pos_var.top-=count;    
s.offset(pos_var);        
//console.log("Final fixed pos is "+pos_fixed.top);        
clearInterval(sDown);
count=0;    
pos_var= s.offset();    
s.removeAttr("disabled");    
s1.removeAttr("disabled");        
clearInterval(intCl);    
s.css({"border-radius":function(id,elem){
                return "20px";
            },"background-color":"black","color":"white","opacity":1})    
}
scrollCheck=scrollVar;   
pos_var.top+=20;
count+=1;    
//pos_fixed.top-=10;    
s.offset(pos_var);        
//console.log("Variable pos is "+pos_var.top);    
},speed);    
}



intCl=0;
brad=0;
/*setInterval(function(){
    var off=$("#lastDiv");
    var pos = off.offset();
    pos.top +=100;
    off.offset(pos);
},3000)*/
$(".sc").each(function(){
    var get = $(this);
    //$(this).text("Scroll");
    //console.log("name is "+$(this).text());
    $(this).mouseover(function(){
        //if(get.hasClass("sc_style")){
          //  get.removeClass("sc_style");
        //}
        //get.addClass("sc_mouse");
        //get.toggleClass("sc_style")
        //get.toggleClass("sc_mouse")
        brad=0;
        color=["darkred","darkbrown","darkgreen","green","darkmagneta","darkgoldenrod","darkviolet","darkorange","deeppink","black","deepskyblue"];
        intCl=setInterval(function(){
            colValue=Math.floor(Math.random()*color.length);
            get.css({"border-radius":function(id,elem){
               // console.log(elem);
                if(elem==="20px" || elem===20 || elem === "20")
                return 0;
                else
                return 20;
            },"background-color":color[colValue],"opacity":0.9})
        },350);
    });
    $(this).mouseout(function(){
        /*if(get.hasClass("sc_mouse")){
            get.removeClass("sc_mouse");
        }
        get.addClass("sc_style");*/
        //get.toggleClass("sc_mouse")
        //get.toggleClass("sc_style")
        clearInterval(intCl);
        get.css({"border-radius":function(id,elem){
                return "20px";
            },"background-color":"black","color":"white","opacity":1})
    });
    //$(this).addClass("sc_mouse");
})

jQuery("#scroll_up").click(function(){
scrollUP();    
    
//    scroll_to_id(2);
    //alert(window.pageYOffset);
    //console.log(window.pageYOffset);
    //setI;
});
jQuery("#scroll_down").click(function(){
scrollDOWN();    
});

function scroll_to_id(id){
    var loc = location.href+"#"+id;
    window.location.href=loc;
}
function remove_add_attr_one(attr_name,attr_value,element,isAdd){
if(isAdd){
$(element).attr(attr_name,attr_value);     
}else{
$(element).removeAttr(attr_name);    
}
}

function remove_add_attr_many(attr_name,attr_value,element,isAdd){
    if(isAdd){
    $(element).each(function(){
     $(this).attr(attr_name,attr_value);   
    })
    }else{
    $(element).each(function(){
     $(this).removeAttr(attr_name);   
    })
    }
}


function remove_add_css_many(attr_name,attr_value,element){
    
    $(element).each(function(){
     $(this).css(attr_name,attr_value);   
    })
    
}
