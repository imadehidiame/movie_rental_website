"use strict";
exports.__esModule = true;
var leap_year_1 = require("./leap_year");
var leap = new leap_year_1.LeapYear();
$("#search").click(function () {
    if ($("#start").val() === '' || $("#end").val() === '') {
        alert("Please make sure the 'Start Year' and 'End Year' are selected before proceeding.");
        return false;
    }
    $("#loader").toggleClass("n_d");
    $("#get").toggleClass("tint_display");
    setTimeout(function () {
        var years = {
            year1: $("#start").val(),
            year2: $("#end").val()
        };
        leap.printYearsWithObject(years, $("#order").val());
        $("#loader").toggleClass("n_d");
        $("#get").toggleClass("tint_display");
    }, 5000);
    return false;
});
