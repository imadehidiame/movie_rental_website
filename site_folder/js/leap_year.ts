declare var $: any;
class LeapYear{
    private year1:any;private year2:any;
    t:any;
    private years:any;
    constructor(){
        this.years = {
            year1:2018,
            year2:1980
        };
        this.t=$("#get");
        this.printYearsWithObject(this.years,"DESC");
        console.log("New additionsss");
        
    }
    checkLeapYear(year:number):boolean{
        return (year%4===0 && year % 100 !== 0) || year % 400 ===0;
    }
    printYears(startYear:any,endYear:any,order?:string):void{
        
    let leapYearCount: number = 0;
    let stringValue: string="<table class='table table-dark table-hover'><thead><tr><th>Year</th><th>Year Type</th></tr></thead><tbody>";
      if(startYear===endYear){
        var string_check: string = this.checkLeapYear(endYear)?" a Leap Year":" not a Leap Year";
        stringValue+=endYear+" is"+string_check;
        //console.log(endYear+" is"+string_check);
        this.t.html(stringValue);   
        return;
      }  
     this.checkYearEquality(startYear,endYear); 
     startYear=this.returnYear1();
     endYear=this.returnYear2();
     
    //console.log(`in use method startYear is ${startYear} and endYear is ${endYear}`);
    switch(order){
        case "ASC":
        for(var i = startYear;i<=endYear;i++){

            leapYearCount += this.checkLeapYear(i)?1:0;
            let classType: any =  this.checkLeapYear(i)?"bg-danger" : "bg-success";

            let appendString : string = this.checkLeapYear(i)?"Leap Year" : "Normal Year";
            //stringValue+=i+appendString+"<br>";
            stringValue+="<tr><td>"+i+"</td><td class='"+classType+"'>"+appendString+"</td></tr>";
            //console.log(i+appendString);

        }
        break;
        case "DESC":
        for(var i = endYear;i>=startYear;i--){
            leapYearCount += this.checkLeapYear(i)?1:0;
            let classType: any =  this.checkLeapYear(i)?"bg-danger" : "bg-success";

            let appendString : string = this.checkLeapYear(i)?"Leap Year" : "Normal Year";
            //stringValue+=i+appendString+"<br>";
            stringValue+="<tr><td>"+i+"</td><td class='"+classType+"'>"+appendString+"</td></tr>";
        }
        break;
        default:
        for(var i = startYear;i<=endYear;i++){
            leapYearCount += this.checkLeapYear(i)?1:0;
            let classType: any =  this.checkLeapYear(i)?"bg-danger" : "bg-success";

            let appendString : string = this.checkLeapYear(i)?"Leap Year" : "Normal Year";
            //stringValue+=i+appendString+"<br>";
            stringValue+="<tr><td>"+i+"</td><td class='"+classType+"'>"+appendString+"</td></tr>";
        }
    }
    stringValue+="<tr><td>Leap Year Count</td><td>"+leapYearCount+"</td></tr></tbody></table>";
    //console.log(stringValue);
    this.t.html(stringValue);
    }

    printYearsWithObject(years:any,order?:string):void{
        
    let leapYearCount: number = 0;
    let stringValue: string="<table class='table table-dark table-hover'><thead><tr><th>Year</th><th>Year Type</th></tr></thead><tbody>";
      if(years.year1===years.year2){
        var string_check: string = this.checkLeapYear(years.year1)?" a Leap Year":" not a Leap Year";
        stringValue+=years.year1+" is"+string_check;
        //console.log(endYear+" is"+string_check);
        this.t.html(stringValue);   
        return;
      }  
     this.checkYearEqualityWithObject(years); 
     
    switch(order){
        case "ASC":
        for(var i = years.year1;i<=years.year2;i++){

            leapYearCount += this.checkLeapYear(i)?1:0;
            let classType: any =  this.checkLeapYear(i)?"bg-danger" : "bg-success";

            let appendString : string = this.checkLeapYear(i)?"Leap Year" : "Normal Year";
            //stringValue+=i+appendString+"<br>";
            stringValue+="<tr><td>"+i+"</td><td class='"+classType+"'>"+appendString+"</td></tr>";
            //console.log(i+appendString);

        }
        break;
        case "DESC":
        for(var i = years.year2;i>=years.year1;i--){
            leapYearCount += this.checkLeapYear(i)?1:0;
            let classType: any =  this.checkLeapYear(i)?"bg-danger" : "bg-success";

            let appendString : string = this.checkLeapYear(i)?"Leap Year" : "Normal Year";
            //stringValue+=i+appendString+"<br>";
            stringValue+="<tr><td>"+i+"</td><td class='"+classType+"'>"+appendString+"</td></tr>";
        }
        break;
        default:
        for(var i = years.year1;i<=years.year2;i++){
            leapYearCount += this.checkLeapYear(i)?1:0;
            let classType: any =  this.checkLeapYear(i)?"bg-danger" : "bg-success";

            let appendString : string = this.checkLeapYear(i)?"Leap Year" : "Normal Year";
            //stringValue+=i+appendString+"<br>";
            stringValue+="<tr><td>"+i+"</td><td class='"+classType+"'>"+appendString+"</td></tr>";
        }
    }
    stringValue+="<tr><td>Leap Year Count</td><td>"+leapYearCount+"</td></tr></tbody></table>";
    //console.log(stringValue);
    this.t.html(stringValue);
    }


    private checkYearEquality(year1:number,year2:number): void {
        if(year1>year2){
            var temp = year2;
            year2=year1;
            year1=temp;
        }
        this.year1=year1;
        this.year2=year2;
    }
    private checkYearEqualityWithObject(years:any): void {
        if(years.year1>years.year2){
            var temp = years.year2;
            years.year2=years.year1;
            years.year1=temp;
        }
     
    }
    private returnYear1():number{
        return this.year1;
    }
    private returnYear2():number{
        return this.year2;
    }


}
//let leap = new LeapYear();
//leap.printYears(1980,2018,"DESC");

//leap.checkLeapYear(2005)?console.log(`2005 is a leap year`): console.log("2005 is not a leap year");

export { LeapYear };