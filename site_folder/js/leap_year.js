"use strict";
exports.__esModule = true;
var LeapYear = /** @class */ (function () {
    function LeapYear() {
        this.years = {
            year1: 2018,
            year2: 1980
        };
        this.t = $("#get");
        this.printYearsWithObject(this.years, "DESC");
        console.log("New additionsss");
    }
    LeapYear.prototype.checkLeapYear = function (year) {
        return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
    };
    LeapYear.prototype.printYears = function (startYear, endYear, order) {
        var leapYearCount = 0;
        var stringValue = "<table class='table table-dark table-hover'><thead><tr><th>Year</th><th>Year Type</th></tr></thead><tbody>";
        if (startYear === endYear) {
            var string_check = this.checkLeapYear(endYear) ? " a Leap Year" : " not a Leap Year";
            stringValue += endYear + " is" + string_check;
            //console.log(endYear+" is"+string_check);
            this.t.html(stringValue);
            return;
        }
        this.checkYearEquality(startYear, endYear);
        startYear = this.returnYear1();
        endYear = this.returnYear2();
        //console.log(`in use method startYear is ${startYear} and endYear is ${endYear}`);
        switch (order) {
            case "ASC":
                for (var i = startYear; i <= endYear; i++) {
                    leapYearCount += this.checkLeapYear(i) ? 1 : 0;
                    var classType = this.checkLeapYear(i) ? "bg-danger" : "bg-success";
                    var appendString = this.checkLeapYear(i) ? "Leap Year" : "Normal Year";
                    //stringValue+=i+appendString+"<br>";
                    stringValue += "<tr><td>" + i + "</td><td class='" + classType + "'>" + appendString + "</td></tr>";
                    //console.log(i+appendString);
                }
                break;
            case "DESC":
                for (var i = endYear; i >= startYear; i--) {
                    leapYearCount += this.checkLeapYear(i) ? 1 : 0;
                    var classType = this.checkLeapYear(i) ? "bg-danger" : "bg-success";
                    var appendString = this.checkLeapYear(i) ? "Leap Year" : "Normal Year";
                    //stringValue+=i+appendString+"<br>";
                    stringValue += "<tr><td>" + i + "</td><td class='" + classType + "'>" + appendString + "</td></tr>";
                }
                break;
            default:
                for (var i = startYear; i <= endYear; i++) {
                    leapYearCount += this.checkLeapYear(i) ? 1 : 0;
                    var classType = this.checkLeapYear(i) ? "bg-danger" : "bg-success";
                    var appendString = this.checkLeapYear(i) ? "Leap Year" : "Normal Year";
                    //stringValue+=i+appendString+"<br>";
                    stringValue += "<tr><td>" + i + "</td><td class='" + classType + "'>" + appendString + "</td></tr>";
                }
        }
        stringValue += "<tr><td>Leap Year Count</td><td>" + leapYearCount + "</td></tr></tbody></table>";
        //console.log(stringValue);
        this.t.html(stringValue);
    };
    LeapYear.prototype.printYearsWithObject = function (years, order) {
        var leapYearCount = 0;
        var stringValue = "<table class='table table-dark table-hover'><thead><tr><th>Year</th><th>Year Type</th></tr></thead><tbody>";
        if (years.year1 === years.year2) {
            var string_check = this.checkLeapYear(years.year1) ? " a Leap Year" : " not a Leap Year";
            stringValue += years.year1 + " is" + string_check;
            //console.log(endYear+" is"+string_check);
            this.t.html(stringValue);
            return;
        }
        this.checkYearEqualityWithObject(years);
        switch (order) {
            case "ASC":
                for (var i = years.year1; i <= years.year2; i++) {
                    leapYearCount += this.checkLeapYear(i) ? 1 : 0;
                    var classType = this.checkLeapYear(i) ? "bg-danger" : "bg-success";
                    var appendString = this.checkLeapYear(i) ? "Leap Year" : "Normal Year";
                    //stringValue+=i+appendString+"<br>";
                    stringValue += "<tr><td>" + i + "</td><td class='" + classType + "'>" + appendString + "</td></tr>";
                    //console.log(i+appendString);
                }
                break;
            case "DESC":
                for (var i = years.year2; i >= years.year1; i--) {
                    leapYearCount += this.checkLeapYear(i) ? 1 : 0;
                    var classType = this.checkLeapYear(i) ? "bg-danger" : "bg-success";
                    var appendString = this.checkLeapYear(i) ? "Leap Year" : "Normal Year";
                    //stringValue+=i+appendString+"<br>";
                    stringValue += "<tr><td>" + i + "</td><td class='" + classType + "'>" + appendString + "</td></tr>";
                }
                break;
            default:
                for (var i = years.year1; i <= years.year2; i++) {
                    leapYearCount += this.checkLeapYear(i) ? 1 : 0;
                    var classType = this.checkLeapYear(i) ? "bg-danger" : "bg-success";
                    var appendString = this.checkLeapYear(i) ? "Leap Year" : "Normal Year";
                    //stringValue+=i+appendString+"<br>";
                    stringValue += "<tr><td>" + i + "</td><td class='" + classType + "'>" + appendString + "</td></tr>";
                }
        }
        stringValue += "<tr><td>Leap Year Count</td><td>" + leapYearCount + "</td></tr></tbody></table>";
        //console.log(stringValue);
        this.t.html(stringValue);
    };
    LeapYear.prototype.checkYearEquality = function (year1, year2) {
        if (year1 > year2) {
            var temp = year2;
            year2 = year1;
            year1 = temp;
        }
        this.year1 = year1;
        this.year2 = year2;
    };
    LeapYear.prototype.checkYearEqualityWithObject = function (years) {
        if (years.year1 > years.year2) {
            var temp = years.year2;
            years.year2 = years.year1;
            years.year1 = temp;
        }
    };
    LeapYear.prototype.returnYear1 = function () {
        return this.year1;
    };
    LeapYear.prototype.returnYear2 = function () {
        return this.year2;
    };
    return LeapYear;
}());
exports.LeapYear = LeapYear;
