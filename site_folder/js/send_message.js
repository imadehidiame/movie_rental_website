//length_assigned, length_error_message-optional,empty_error_message-optional,length_valid, name=$().value()  empty,length_valid [email]
$('.inp').each(function(){
$(this).css({
    "box-shadow":"none"
});
});
var check_boolean = true;  
var check_boolean1 = true;    
var check_boolean2 = true;        
var o = {
    fieldname:{
    id:"name",
    housing_element:"span",    
    validation_rules:{
        valid_length: 4,
        valid_length_message: "optional",
        valid_email: true,
        valid_email_message: "optional",
        valid_required: true,
        valid_required_message: "optional"   
    }
    }
};

var name_dirty = false;
var email_dirty = false;
var message_dirty = false;

var name_dirty1 = false;
var email_dirty1 = false;
var message_dirty1 = false;

$("#name").keyup(function(){
    if(name_dirty&&name_dirty1){
     var criteria = [
 {
    fieldname:{
    id:"name",
    housing_element:"name_error",    
    validation_rules:{
        valid_length: 5,
        valid_length_message: "Five characters at least is required",
        valid_required: true
    }
    }
}
     ];
     validateAllFields(criteria);
     
     
    }
});


$("#name").keypress(function(){
    var x = event.which || event.keyCode;
    var str = String.fromCharCode(x);
    if(/[0-9A-Za-z]/.test(str)&& !(x>=0 && x<=31)){
     //event.preventDefault();
     name_dirty=true;
    }
});

$("#name").keydown(function(){
    var x = event.which || event.keyCode;
    var str = String.fromCharCode(x);
    if(/[0-9A-Za-z]/.test(str)&& !(x>=0 && x<=31)){
     //event.preventDefault();
     name_dirty=true;
    }
});

$("#name").focusout(function(){
    if(name_dirty === true){
        name_dirty1=true;
    }
    //name_dirty1=true;
    if(name_dirty1){
     var criteria = [
    {
    fieldname:{
    id:"name",
    housing_element:"name_error",    
    validation_rules:{
        valid_length: 5,
        valid_length_message: "Five characters at least is required",
        valid_required: true
    }
    }
    }
     ];
     validateAllFields(criteria);
    }
    
});



$("#message").keypress(function(){
    var x = event.which || event.keyCode;
    var str = String.fromCharCode(x);
    if(/[0-9A-Za-z]/.test(str)&& !(x>=0 && x<=31)){
     //event.preventDefault();
     message_dirty=true;
    }
});

$("#message").keydown(function(){
    var x = event.which || event.keyCode;
    var str = String.fromCharCode(x);
    if(/[0-9A-Za-z]/.test(str)&& !(x>=0 && x<=31)){
     //event.preventDefault();
     message_dirty=true;
    }
});

$("#message").focusout(function(){
    if(message_dirty===true){
    message_dirty1=true;
    }
    //    message_dirty1=true;
    if(message_dirty1){
     var criteria = [
    {
    fieldname:{
    id:"message",
    housing_element:"message_error",    
    validation_rules:{
        valid_length: 10,
        valid_length_message: "Message is way too short. Message should contain at least 10 characters",
        valid_required: true,
        valid_required_message: "Message is a required field that should not be ignored"
        }
        }
    }    

     ];
     validateAllFields(criteria);
    }
    
});


$("#message").keyup(function(){
    if(message_dirty&&message_dirty1){
     var criteria = [
    {
    fieldname:{
    id:"message",
    housing_element:"message_error",    
    validation_rules:{
        valid_length: 10,
        valid_length_message: "Message is way too short. Message should contain at least 10 characters",
        valid_required: true,
        valid_required_message: "Message is a required field that should not be ignored"
        }
        }
    }    

     ];
     validateAllFields(criteria);
    }
});


$("#email").keypress(function(){
    var x = event.which || event.keyCode;
    var str = String.fromCharCode(x);
    if(/[0-9A-Za-z]/.test(str)&& !(x>=0 && x<=31)){
     //event.preventDefault();
     email_dirty=true;
    }
});

$("#email").keydown(function(){
    var x = event.which || event.keyCode;
    var str = String.fromCharCode(x);
    if(/[0-9A-Za-z]/.test(str)&& !(x>=0 && x<=31)){
     //event.preventDefault();
     email_dirty=true;
    }
});

$("#email").focusout(function(){
    if(email_dirty===true){
    email_dirty1=true;
    }
    if(email_dirty1){
     var criteria = [
    {
    fieldname:{
    id:"email",
    housing_element:"email_error",    
    validation_rules:{
        valid_email: true,
        valid_email_message: "Enter a valid email address",
        valid_required: true
    }
    }
    }    
];
     validateAllFields(criteria);
    }
    
});


$("#email").keyup(function(){
    if(email_dirty&&email_dirty1){
     var criteria = [
    {
    fieldname:{
    id:"email",
    housing_element:"email_error",    
    validation_rules:{
        valid_email: true,
        valid_email_message: "Enter a valid email address",
        valid_required: true
    }
    }
    }    
];
     validateAllFields(criteria);
    }
});



$("#submit_button").click(function(){
/*name_dirty=true;
name_dirty1=true;
email_dirty=true;
email_dirty1=true;
message_dirty=true;
message_dirty1=true;    
var criteria = [
 
 
{
    fieldname:{
    id:"email",
    housing_element:"email_error",    
    validation_rules:{
        valid_email: true,
        valid_email_message: "Enter a valid email address",
        valid_required: true
    }
    }
}

    

];    
if(validateAllFields(criteria)){
$(".inp").each(function(){
$(this).val(function(){
    return this.defaultValue;
});    
});*/    
return true;
});

function createJqueryObject(id){
    return "#"+id;
}

function validateAllFields(fieldObject,errorDivID){
var basicCheck = true;    
errorDivID=createJqueryObject(errorDivID);   
//console.log("err div is "+errorDivID);    
for(var i=0;i<fieldObject.length;i++){
var basicCheckInLoop = true;        
var errorText ="<ul class='my'>";    
var jqueryObject = createJqueryObject(fieldObject[i].fieldname.id);
var housingElement = createJqueryObject(fieldObject[i].fieldname.housing_element);
//console.log(housingElement);    
    
    
if(fieldObject[i].fieldname.validation_rules.valid_required && fieldObject[i].fieldname.validation_rules.valid_required === true){
basicCheckInLoop=basicCheckInLoop&&validateStringLength($(jqueryObject).val(),1);
if(!validateStringLength($(jqueryObject).val(),1)){
if(fieldObject[i].fieldname.validation_rules.valid_required_message && validateStringLength(fieldObject[i].fieldname.validation_rules.valid_required_message,1)){
errorText+="<li>"+fieldObject[i].fieldname.validation_rules.valid_required_message+"</li>";    
}else{
errorText+="<li>This field is required and cannot be empty</li>";        
}    
}    
}    
    
if(fieldObject[i].fieldname.validation_rules.valid_length && fieldObject[i].fieldname.validation_rules.valid_length>=1){
basicCheckInLoop=basicCheckInLoop&&validateStringLength($(jqueryObject).val(),fieldObject[i].fieldname.validation_rules.valid_length);
if(!validateStringLength($(jqueryObject).val(),fieldObject[i].fieldname.validation_rules.valid_length)){
if(fieldObject[i].fieldname.validation_rules.valid_length_message && validateStringLength(fieldObject[i].fieldname.validation_rules.valid_length_message,1)){
errorText+="<li>"+fieldObject[i].fieldname.validation_rules.valid_length_message+"</li>";    
}else{
errorText+="<li>This field requires at least "+fieldObject[i].fieldname.validation_rules.valid_length+" character(s)</li>";        
}    
}    
}

if(fieldObject[i].fieldname.validation_rules.valid_email && fieldObject[i].fieldname.validation_rules.valid_email === true){
basicCheckInLoop=basicCheckInLoop&&validateEmail($(jqueryObject).val());
if(!validateEmail($(jqueryObject).val())){
if(fieldObject[i].fieldname.validation_rules.valid_email_message && validateStringLength(fieldObject[i].fieldname.validation_rules.valid_email_message,1)){
errorText+="<li>"+fieldObject[i].fieldname.validation_rules.valid_email_message+"</li>";    
}else{
errorText+="<li>The email provided is not valid</li>";        
}    
}    
}
    

errorText+="</ul>";    
if(!basicCheckInLoop){
$(jqueryObject).css("border-bottom","1px solid red");    
$(housingElement).html(errorText);    
}else{
$(jqueryObject).css("border-bottom","1px solid green");    
$(housingElement).html("");        
}    
basicCheck=basicCheck&&basicCheckInLoop;    
}    
if(!basicCheck){
if(errorDivID){    
$(errorDivID).css("color","red");
$(errorDivID).html("Please ensure all errors are corrected before form submission");    
}
}else{
if(errorDivID){    
$(errorDivID).css("color","green");
$(errorDivID).html("Form validated successfully");    
}
}    
return basicCheck;    
}
function validateStringLength(stringValue,stringLength){
    return stringValue.trim().length>=stringLength;
}

function validateEmail(stringValue){
var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
return re.test(stringValue.toLowerCase());    
}    


function validate_field(criteria,field_element,field_error_element,log_error_element,element_type,id_value){
var error_message = "<ul>";
//var check_boolean = true;  
///^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    
//var check_boolean1 = true;    
//var check_boolean2 = true;        
var jqo = create_jquery_object(element_type,field_element,id_value);
if(criteria.length_valid){
if(!validate_length(criteria,criteria.length_assigned)){
check_boolean=false;    
if(criteria.length_error_message){
error_message+="<li>"+criteria.length_error_message+"</li>";    
}else{
error_message+="<li>This field must have at least "+criteria.length_assigned+" character(s)</li>";    
}    
}else{
check_boolean=true;    
}    
}

if(criteria.empty){
if(!val_empty(criteria)){
check_boolean1=false;    
if(criteria.empty_error_message){
error_message+="<li>"+criteria.empty_error_message+"</li>";    
}else{
error_message+="<li>This field must not be empty</li>";    
}    
}else{
check_boolean1=true;    
}    
}
    
if(criteria.email){
if(!validate_email(criteria)){
check_boolean2=false;    
if(criteria.empty_email_message){
error_message+="<li>"+criteria.empty_email_message+"</li>";    
}else{
error_message+="<li>Please enter a valid email address</li>";    
}    
}else{
check_boolean2=true;    
}    
}    
    
error_message+="</ul>";    
$("#"+field_error_element).html(error_message);    
console.log("check is "+check_boolean);
console.log("check1 is "+check_boolean1);
console.log("check2 is "+check_boolean2);    
if(!check_boolean || check_boolean===false||!check_boolean1 || check_boolean1===false||!check_boolean2 || check_boolean2===false){
$(jqo).css("border-bottom","1px solid red");    
$("#"+log_error_element).html("Please ensure all errors are corrected before submitting the form");      
}else{
$(jqo).css("border-bottom","1px solid lightgreen");    
$("#"+log_error_element).html("Form has been validated and your message has been sent");
$("#"+field_error_element).html("");     
}
    
}









var validate_email_field = function(){
var jqo = create_jquery_object("text","email","email");    
var criteria = {
    empty: true,
    name: $(jqo).val(),
    email:true
    
};
validate_field(criteria,"email","email_error","log_messages","text");

};


var validate_name_field = function(){
var jqo = create_jquery_object("text","name","name");    
var criteria = {
    length_assigned : 2,
    length_error_message: "Requires at least 2 characters",
    length_valid: true,
    empty: true,
    name: $(jqo).val()
    
};
validate_field(criteria,"name","name_error","log_messages","text");

};


var validate_message_field = function(){
var jqo = create_jquery_object("textarea","message","message");    
var criteria = {
    length_assigned : 5,
    length_error_message: "Message too short. Requires at least 5 characters",
    length_valid: true,
    empty: true,
    name: $(jqo).val()
    
};
validate_field(criteria,"message","message_error","log_messages","textarea");

};


function create_jquery_object(element_type,field_element,id_value){
if(element_type==="textarea"){
return ("#"+id_value);    
}    
return "input:"+element_type+"[name="+field_element+"]";    
}    
function validate_email(obj){
var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
return re.test(String(obj.name.trim()).toLowerCase());    
    
}    
function validate_length(obj,check_value){
if(obj.name.trim().length >= check_value){
    console.log("Length is "+obj.name.trim().length);
    return true;
}
    console.log("Length is "+obj.name.trim().length);
    return false;
}
function val_empty(obj){
return !(!obj.name || obj.name === "" || obj.name.trim().length===0);
}
var obj = {
    "name": "Leo",
    "age" : 25,
    "empty":" "
    
};
//alert($("input:textarea[name=message]").val() );  
var validate_form = function(){
tex = "";    
var final_object = {
   name: $("input:text[name=email]").val() 
};

if(!val_empty(final_object)){
tex+="\nEmail empty";    
}
final_object = {
   name: $("input:text[name=name]").val() 
};
if(!val_empty(final_object)){
tex+="\nName empty";    
}
final_object = {
   name: $("input:text[name=subject]").val() 
};    
if(!val_empty(final_object)){
tex+="\nSubject empty";    
}
if(tex===""){
return "Validation successful";    
}    
return tex;    
};




/*if(val_empty(obj)){
    alert("test successful");
}else{
    alert("test unsuccessful");
}*/
blink_int=1;
hide_int=0;
side_int=0;
//if(blink_int)
//alert("blink");
var constant_offset = $("#submit_button").offset();

//$("div.em").append("");

var side_animation = $("#side_animation");
var submit_button = $("#submit_button");
var blink_animation = $("#blink_animation");
var hide_animation = $("#hide_animation");
var clear_animation = $("#clear_animation");
var side_to_side = function(){
    constant_offset = $("#submit_button").offset();
    var anime_speed = $("#animation_speed").val();
    var count = 0;
    var started = false;
    var notStarted = true;
    side_int=setInterval(function(){
     if(notStarted){
        count+=1;
        var pos = submit_button.offset();
        pos.left+=20;
        submit_button.offset(pos);
      
     }
     if(started){
        count-=1;
        var pos = submit_button.offset();
        pos.left-=20;
        submit_button.offset(pos);   
      
     }
      if(count>=4){
      notStarted=false;    
      started=true;
      }
      if(count<=0){
        notStarted=true;    
        started=false;   
      }
      
        
    },anime_speed);
    
};
var blink = function(){
    var anime_speed = $("#animation_speed").val();
    var opacity = "1";
    blink_int=setInterval(function(){
    if(opacity=="1")
    opacity="0";
    else
    opacity="1";    
    submit_button.css("opacity",opacity);
    },anime_speed);
    
};

var clear_animations = function(){
    if(blink_int){
    clearInterval(blink_int);
    submit_button.css("opacity","1");
    
    }

    if(side_int){
    clearInterval(side_int);
    $("#submit_button").offset(constant_offset);
    }
};


var hide = function(){
    submit_button.toggleClass("n_d");
};


side_animation.click(function(){
    side_to_side();
});
blink_animation.click(function(){
    blink();
});
hide_animation.click(function(){
    hide();
});
clear_animation.click(function(){
    clear_animations();
});

