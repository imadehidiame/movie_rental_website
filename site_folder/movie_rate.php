<?php 
session_start();
if(!isset($_SESSION['login']) || $_SESSION['login'] !== true){
    $_SESSION['sign_error'] = "You must log in to access your dashboard";    
    header("Location: login.php");    
}
?>
<!DOCTYPE html>
<html lang="en">    
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">   
<link rel="shortcut icon" href="images/icon2.fw.png">
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/mdb.css">
<link rel="stylesheet" href="stylo.css">
<style>

.sc_style{
        border-radius:20px;
        padding:8px; 
        text-transform:capitalize; 
        font-size:14px; 
        background-color: black; 
        border: 1px solid black;
    } 
    .sc_mouse{
        border-radius:20px;
        padding:8px; 
        text-transform:capitalize; 
        font-size:14px; 
        background-color: white; 
        color:black;
    }

#div_span{
    border: 1px solid lightgrey; 
    background-color: lightgray;
    border-radius: 50px;
    width: auto;
}

.succ_div{
                    width:auto;
                    float:left;                    
                }
                
                .succ_details{
                    width:auto;
                    background:#5aeeb0;
                    color:#048d54;
                    padding:10px 40px;
                    margin:30px;
                    border-radius:5px;                  
                }
.vid_div{
        border: 1px solid lightgrey; 
        border-radius: 10px; 
        margin: 20px 10px 20px 10px;
        text-align: center;
}        
.square-btnn{
    display: block;    
}
.f_right{
        float: right;
}        
.h2_text{
        color:white;
}        
.p_text{
        color: white; 
        font-size: 18px;
}        
.back_div{
        background-color:black; 
        opacity:0.9; 
        width: inherit; 
        height: 50%; 
        padding: 8%;        
}        
.d_bg1{
        background-image: url("images/thor_bus1.png");
        background-repeat: no-repeat;
        width: 100%;
        height: 750px;
        border:1px solid white;
        border-radius: 5px;

    }
    .d_bg2{
        background-image: url("images/thor_bus2.png");
        background-repeat: no-repeat;
        height: 750px;
        width: 100%;
        border:1px solid white;
        border-radius: 5px;
    }
    .d_bg3{
        background-image: url("images/thor_bus3.png");
        background-repeat: no-repeat;
        height: 750px;
        width: 100%;
        border:1px solid white;
        border-radius: 5px;
    }

 .foot_img{
        /*width: 100%;*/
        margin-top: 20px;
        border-top: 1px solid lightgrey;
        height: 600px;
        background-image: url("images/busss.png");
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
        
    }   
    

    @media screen and (max-width: 900px) {
  .c1{
    margin-left:30px;
  }
  .back_div{
      padding: 2%;    
      height: 60%;
   
  }
  .h2_text{
        font-size: 18px;
  }
  .vid_div{
        border: 1px solid white;           
  }
  .p_text{
        font-size: 15px;
  }
  video{
      margin-left: 50px;
  }
  .foot_img{
        
          background-image: url("images/bus11.png");
          height: 500px;
  }
  .d_bg1{
        
        height: 390px;
        width:100%;
  }
  .d_bg2{
        
        width:100%;
        height: 390px;

  }
  .d_bg3{
        
        height: 390px;
        width:100%;

  }
  .square-btnn{
    /*display: none;    */
}

}


@media screen and (max-width: 500px) {
  .c1{
    margin-left:30px;
  }
  .vid_div{
        border: 1px solid white;           
  }
  #div_span{
      border-radius: 80px;
  }
  .back_div{
      padding: 2%;    
      height: 100%;
  }
  .h2_text{
        font-size: 15px;
  }
  .p_text{
        font-size: 12px;
  }
  video{
      margin-left: 80px;
  }
  .foot_img{
        
          background-image: url("images/bus1.png");
          height: 500px;
  }
  .d_bg1{
        background-image: url("images/thor11.png");
        height: 200px;

  }
  .d_bg2{
        background-image: url("images/thor22.png");
        height: 200px;

  }
  .d_bg3{
        background-image: url("images/thor4444.png");
        height: 200px;

  }
  .square-btnn{
    /*display: none;    */
}

}
li:hover{
        text-decoration: underline;
}


</style>
<title>247Rentals</title>   
</head>
<body>
                <header>
                                <nav class="navbar navbar-expand-lg navbar-dark indigo">
                                    
                                      
                                      <a class="navbar-brand" href="index.php"><img src="images/icon.fw.png" style="margin-right:3px"><span style="font-size:18px;" class="badge badge-pill badge-light">247Rentals</span></a>
                                    
                                      
                                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
                                          aria-expanded="false" aria-label="Toggle navigation">
                                          <span class="navbar-toggler-icon"></span>
                                      </button>
                                    
                                      <!-- Collapsible content -->
                                      <div class="collapse navbar-collapse" id="basicExampleNav">
                                    
                                          <!-- Links -->
                                          <ul class="navbar-nav mr-auto">
                                              <li class="nav-item">
                                                  <a class="nav-link" href="loginsuccess.php">Dashboard
                                                      <span class="sr-only">(current)</span>
                                                  </a>
                                              </li>
                                              <li class="nav-item">
                                                  <a class="nav-link" href="profile.php">Profile</a>
                                              </li>
                                              
                        
                                              <li class="nav-item">
                                                <a class="nav-link" href="videos.php">Upload Videos</a>
                                            </li>

                                            <li class="nav-item active">
                                                <a class="nav-link" href="movie_rate.php">Rate Movies</a>
                                            </li>

                                            <li class="nav-item">
                                                  <a class="nav-link" href="logout.php">Log out</a>
                                              </li>
                                              <!-- Dropdown -->
                                                                                        </ul>
                                          <!-- Links -->
                                    
                                                                                    
                                          
                                      </div>
                                      <!-- Collapsible content -->
                                    
                                    </nav>

                                    

                                </header>
<main class="mt-40">
 <div class="jumbotron joumbotron-fluid indigo">
        <div class="row justify-content-center"><div class="p-2 mb-4 text-center text-white" id="div_span"><h4 class="display-4 font-weight-bold black-text">Rate The Movies</h4></div></div>
        
        

</div>   
<div class="container">


    
                                                    <hr>
                                                        <!-- Heading -->
                                                        <h2 class="mb-3 font-weight-bold text-center"> Rate The Movies On 247Rentals</h3>
                                                        <!--Grid row-->
                                                        <div class="row" id="movie_pane">
                                                    
                    

                
</div>

</main>                                
<footer class="page-footer text-center text-md-left font-medium indigo pt-4 mt-4">
  
      <!--Footer Links-->
      <div class="container text-center text-md-left">
          <div class="row">
  
              <!--First column-->
              <div class="col-md-6 pb-3">
                  <h5 class="text-uppercase">Quick Links</h5>
                  
                  <b style="font-size:18px;"><a href="loginsuccess.php">Dashboard | 
                                
                              </a></b>
                              <b style="font-size:18px;"><a  href="profile.php">Profile | 
                                
                                  </a></b>
                                  <b style="font-size:18px;"><a href="videos.php">Upload Videos 
                                
                                      </a></b>

                                                                            
                                      

<!--<button class="btn sc_style sc" id="scroll_up"><span data-feather="arrow-up"></span> Automatic ScrollUp</button>-->
                  
              </div>
              <!--/.First column-->
  
              <!--Second column-->
              <div class="col-md-6 pb-3">
                      <!--
                      <a href="#!"><img src="images/teleg1.png"></a>
                      <a href="#!"><img src="images/teleg.png"></a>
                      <a href="#!"><img src="images/teleg.png"></a>
                      <a href="#!"><img src="images/teleg1.png"></a>
                      -->
              </div>
      
          </div>
      </div>
      
      <div class="footer-copyright py-3 text-center indigo">
          © 2018 Copyright:
          <a href="#"> 247Rentals.com </a>
          <?php  ?>
      </div>
      
  
  </footer>
  <script src="bootstrap/js/jquery.js"></script>
  <script src="js/popper.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="bootstrap/js/mdb.min.js"></script>
  <script src="includes/js/feather.min.js"></script>    
  <script src="feath.js"></script>
  <script src="js/gallery.js"></script>

<script>
if(typeof console._commandLineAPI !== 'undefined'){
    console.API = console._commandLineAPI;
}else if(typeof console._inspectorCommandLineAPI !== 'undefined'){
    console.API = console._inspectorCommandLineAPI;
}else if(typeof console.clear !== 'undefined'){
    console.API = console;
}
console.API.clear();
    update();
    setInterval(function(){
    //update();    
    },6000);

    function rate_value(me){
        document.getElementById(me);
        $("#movie_pane").css('opacity',0.3);
        //var h = document.getElementById("me");
        //h.cl
        var url = $("#"+me.className).val();
        var worker = new Worker("util.js");
        worker.postMessage(url);
        worker.onmessage = function(e){
            if(e.data=="good"){
                update();
             /*if(upd){
                $("#movie_pane").css('opacity',1); 
                alert("You have successfully rated a movie");
             }else{
                $("#movie_pane").css('opacity',1);
                alert("You have successfully rated a movie");
             }*/   
             $("#movie_pane").css('opacity',1); 
            alert("You have successfully rated a movie");
            //update();
             
        }else{
            console.log(e.data);
        }
            //else
            
        }
        worker.onerror=function(e){
            $("#movie_pane").css('opacity',1);
            console.log(e.lineno+" "+e.message);
        }
    }


        var count = 1;
        var worker1 = new Worker("util1.js");
        //worker.postMessage(url);
        worker1.onmessage = function(e){
            if(typeof console._commandLineAPI !== 'undefined'){
    console.API = console._commandLineAPI;
}else if(typeof console._inspectorCommandLineAPI !== 'undefined'){
    console.API = console._inspectorCommandLineAPI;
}else if(typeof console.clear !== 'undefined'){
    console.API = console;
}
console.API.clear();
            //count+=1;
            $("#movie_pane").html("");
            //console.log(e.data);
            //console.log(e.data);
            $("#movie_pane").html(e.data);
            
        }
        worker1.onerror=function(e){
            
           // $("#movie_pane").css('opacity',1);
            //console.log(e.lineno+" "+e.message);
        }


        
    function update(){
        
        try{
        var request = new XMLHttpRequest();
    //https://week-6.herokuapp.com/t.php
    //var url = e.data;
        request.open('GET','get_movies.php');

        //request.setRequestHeader('Content-type','text/html');
        //request.timeout = 500000;
        request.onreadystatechange = function() { // A simple event handler.
        if (request.status === 200 ){
          
        var server_data = JSON.parse(request.responseText);
        console.log(server_data.length);
        //console.log(request.responseText);
        var html_string = "";
        //console.log(server_data.length);
        var rate = 1;
        for(var i = 0; i< server_data.length; i++){
        var img_string = '';
        var star = server_data[i].average_rating;
        var star_value = star>1?"stars":"star";
        var unstar = 5 - star;
        
        for(var u = 0; u< star; u++){
            img_string+='<img src="images/rate2.png" onclick="rate_value(this)" class="'+rate+'" style="background-color: black;"><input type="hidden" id="'+rate+'" value="rate.php?video_id='+server_data[i].movie_id+'&rate='+rate+'">';
            rate+=1;
        }
        for(var u = 0; u< unstar; u++){
            img_string+='<img src="images/rate1.png" onclick="rate_value(this)" class="'+rate+'" style="background-color: black;"><input type="hidden" id="'+rate+'" value="rate.php?video_id='+server_data[i].movie_id+'&rate='+rate+'">';
            rate+=1;
        } 
        var tot_users = server_data[i].total_rating==1?"user has":"users have";
    html_string+='<div class="col-lg-4 col-md-12"><div class="card"><div class="view overlay"><div style="background-color:black; width:350px; height:150px; color: white" id="img_opq19" class="card-img-top"><div class="succ_div "><div class="succ_details text-center featurette-heading font-weight-bold">'+server_data[i].title+'</div></div></div></div><div class="card-body"><h4 class="card-title text-center">Movie Attributes</h4><hr><p class="card-text"><b>Genre:</b> '+server_data[i].genre+'</p><hr><p class="card-text"><b>Description:</b> '+server_data[i].description+'</p><hr><div class="row text-center">'+img_string+'</div></div><div class="md-form form-sm">Total Rating <input type="text" style="font-weight: bold" value="'+server_data[i].total_rating+' '+tot_users+' rated this movie" class="form-control form-control-sm inp" placeholder="Movie Title" readonly></div><div class="md-form form-sm">Number of stars on average <input type="text" value="'+server_data[i].average_rating+' '+star_value+'" style="font-weight: bold" class="form-control form-control-sm inp" readonly></div></div></div>';
        }


        $("#movie_pane").html("");
        //console.log(e.data);
        $("#movie_pane").html(html_string);      
     
    };
}
    request.send();
}catch(err){
    console.log("error is "+err.name);
}
    }
    

function update1(){

var request = new XMLHttpRequest();
//https://week-6.herokuapp.com/t.php
//var url = e.data;
request.open('GET','get_movies.php');

//request.setRequestHeader('Content-type','text/html');
//request.timeout = 500000;
request.onreadystatechange = function() { // A simple event handler.
if (request.status === 200 ){
  
var server_data = JSON.parse(request.responseText);
//console.log(request.responseText);
var html_string = "";
//console.log(server_data.length);
var rate = 1;
for(var i = 0; i< server_data.length; i++){
var img_string = '';
var star = server_data[i].average_rating;
var star_value = star>1?"stars":"star";
var unstar = 5 - star;

for(var u = 0; u< star; u++){
    img_string+='<img src="images/rate2.png" onclick="rate_value(this)" class="'+rate+'" style="background-color: black;"><input type="hidden" id="'+rate+'" value="rate.php?video_id='+server_data[i].movie_id+'&rate='+rate+'">';
    rate+=1;
}
for(var u = 0; u< unstar; u++){
    img_string+='<img src="images/rate1.png" onclick="rate_value(this)" class="'+rate+'" style="background-color: black;"><input type="hidden" id="'+rate+'" value="rate.php?video_id='+server_data[i].movie_id+'&rate='+rate+'">';
    rate+=1;
} 

html_string+='<div class="col-lg-4 col-md-12"><div class="card"><div class="view overlay"><div style="background-color:black; width:350px; height:150px; color: white" id="img_opq19" class="card-img-top"><div class="succ_div "><div class="succ_details text-center featurette-heading font-weight-bold">'+server_data[i].title+'</div></div></div></div><div class="card-body"><h4 class="card-title text-center">Movie Attributes</h4><hr><p class="card-text"><b>Genre:</b> '+server_data[i].genre+'</p><hr><p class="card-text"><b>Description:</b> '+server_data[i].description+'</p><hr><div class="row text-center">'+img_string+'</div></div><div class="md-form form-sm">Total Rating <input type="text" style="font-weight: bold" value="'+server_data[i].total_rating+' users have rated this movie" class="form-control form-control-sm inp" placeholder="Movie Title" readonly></div><div class="md-form form-sm">Number of stars on average <input type="text" value="'+server_data[i].average_rating+' '+star_value+'" style="font-weight: bold" class="form-control form-control-sm inp" readonly></div></div></div>';    
}


$("#movie_pane").html("");
//console.log(e.data);
$("#movie_pane").html(html_string);
if(html_string || html_string !==''){
    return true;
}      
return false;
};
}
request.send();
request.onerror = function(){
    return "";
}
}


    /*var sse = new EventSource('get_movies1.php');
    sse.addEventListener("ping",function(e){

        //<div class="col-lg-4 col-md-12"><div class="card"><div class="view overlay"><div style="background-color:black; width:350px; height:150px; color: white" id="img_opq19" class="card-img-top"><div class="succ_div "><div class="succ_details text-center featurette-heading font-weight-bold">Blood work</div></div></div></div><div class="card-body"><h4 class="card-title text-center">Movie Attributes</h4><hr><p class="card-text"><b>Genre:</b> Action</p><hr><p class="card-text"><b>Description:</b> Find out</p><hr><div class="row text-center"><img src="images/rate2.png" style="background-color: black;"><img src="images/rate2.png" style="background-color: black;"><img src="images/rate2.png" style="background-color: black;"><img src="images/rate1.png" style="background-color: black;"><img src="images/rate1.png" style="background-color: black;"></div></div><div class="md-form form-sm">Total Rating <input type="text" style="font-weight: bold" value="11 users have rated this movie" class="form-control form-control-sm inp" placeholder="Movie Title" readonly></div><div class="md-form form-sm">Number of stars on average <input type="text" value="5 stars" style="font-weight: bold" class="form-control form-control-sm inp" readonly></div></div></div>

        //title,genre,description,total_rating, average_rating

        var server_data = JSON.parse(e.data);
        var html_string = "";
        //console.log(server_data.length);
        var rate = 1;
        for(var i = 0; i< server_data.length; i++){
        var img_string = '';
        var star = server_data[i].average_rating;
        var star_value = star>1?"stars":"star";
        var unstar = 5 - star;
        
        for(var u = 0; u< star; u++){
            img_string+='<img src="images/rate2.png" onclick="rate_value(this)" class="'+rate+'" style="background-color: black;"><input type="hidden" id="'+rate+'" value="rate.php?video_id='+server_data[i].movie_id+'&rate='+rate+'">';
            rate+=1;
        }
        for(var u = 0; u< unstar; u++){
            img_string+='<img src="images/rate1.png" onclick="rate_value(this)" class="'+rate+'" style="background-color: black;"><input type="hidden" id="'+rate+'" value="rate.php?video_id='+server_data[i].movie_id+'&rate='+rate+'">';
            rate+=1;
        } 

        html_string+='<div class="col-lg-4 col-md-12"><div class="card"><div class="view overlay"><div style="background-color:black; width:350px; height:150px; color: white" id="img_opq19" class="card-img-top"><div class="succ_div "><div class="succ_details text-center featurette-heading font-weight-bold">'+server_data[i].title+'</div></div></div></div><div class="card-body"><h4 class="card-title text-center">Movie Attributes</h4><hr><p class="card-text"><b>Genre:</b> '+server_data[i].genre+'</p><hr><p class="card-text"><b>Description:</b> '+server_data[i].description+'</p><hr><div class="row text-center">'+img_string+'</div></div><div class="md-form form-sm">Total Rating <input type="text" style="font-weight: bold" value="'+server_data[i].total_rating+' users have rated this movie" class="form-control form-control-sm inp" placeholder="Movie Title" readonly></div><div class="md-form form-sm">Number of stars on average <input type="text" value="'+server_data[i].average_rating+' '+star_value+'" style="font-weight: bold" class="form-control form-control-sm inp" readonly></div></div></div>';    
        }


        $("#movie_pane").html("");
        //console.log(e.data);
        $("#movie_pane").html(html_string);
    },false);  
    */
            
        
    </script> 
    


</body>
</html>
