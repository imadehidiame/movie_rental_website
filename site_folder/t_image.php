<?php 
session_start();
include_once('db_configuration.php');
class DB_Operations extends db_config{
private $conn;    
public function __construct(){
if(!isset($_SESSION['login_user'])){
echo "offline";
header("Location: login.php");    
}
   try{
    $this->conn = new PDO("mysql:host=$this->host;dbname=$this->db","$this->username",$this->password);
    //echo "Connected successfully";
}catch(PDOException $ex){
    die("Could not connect ".$ex->getMessage());
}
}



private function build_insert_string(&$config){
    
    $table_string = $this->create_table_string($config['array_insert']);
    $table_values = $this->create_insert_values($config['array_insert']);
    $string = "insert into {$config['table_name']} $table_string VALUES $table_values";
    return $string;
}
private function build_insert_string_prepared(&$config){
    
    $table_string = $this->create_table_string_prepared($config['array_table_column']);
    $table_values = $this->create_insert_values_prepared($config['array_table_column']);
    $string = "insert into {$config['table_name']} $table_string VALUES $table_values";
    return $string;
}
private function create_table_string_prepared($array){
    if(empty($array)){
        return false;
    }
    //$keys = array_keys($array);
    $string = "( {$array[0]}";
    for($c=1;$c<count($array);$c++){
        $string.=", {$array[$c]}";
    }
    $string.=")";
    return $string;
}

private function create_table_string($array){
    if(empty($array)){
        return false;
    }
    $keys = array_keys($array);
    $string = "( {$keys[0]}";
    for($c=1;$c<count($keys);$c++){
        $string.=", {$keys[$c]}";
    }
    $string.=")";
    return $string;
}
private function create_insert_values($array){
    if(empty($array)){
        return false;
    }
    $keys = array_values($array);
    $string = "( '{$keys[0]}'";
    for($c=1;$c<count($keys);$c++){
        $string.=", '{$keys[$c]}'";
    }
    $string.=")";
    return $string;
}

private function create_insert_values_prepared($array){
    if(empty($array)){
        return false;
    }
    //$keys = array_values($array);
    $string = "( ?";
    for($c=1;$c<count($array);$c++){
        $string.=", ?";
    }
    $string.=")";
    return $string;
}

public function build_search_string_prepared(&$config){
    if(!isset($config['where_parameters'])){
        $config['where_parameters']='';
    }
    return $this->search_param($config['search_parameters'])."from {$config['table_name']}".$this->generate_where_prepared($config['where_parameters']);
}

private function build_update_string_prepared(&$config){
    if(!isset($config['update_where'])){
        $config['update_where']='';
    }
 $upd_value = $this->generate_update_prepared($config['update_values']);
 $upd_where = $this->generate_where_prepared($config['update_where']);   
 return  "update {$config['table_name']} $upd_value $upd_where"; 
 
}

public function update_database(&$config){
    $sql=$this->conn->prepare($this->build_update_string_prepared($config));
    for($count = 0; $count<count($config['where_value']);$count++){
        array_push($config['updated_values'],$config['where_value'][$count]);
    }
    //$config['where_value'];
    return $sql->execute($config['updated_values']);
}

private function generate_where_prepared($where){
    if(is_null($where) || empty($where)){
        return "";
    }
    //$keys = array_keys($where);
    //$values = array_values($where);
    $init = " where {$where[0]} = ?";
    $arr[] = $where[0];
    for($count = 1; $count<count($where);$count++){
        $and_or = in_array($where[$count],$arr)?'or':'and';
        $init.= " $and_or {$where[$count]} = ?";
        $arr[] = $where[$count];
    }
    return $init;
}

private function generate_update_prepared($where){
    if(is_null($where) || empty($where)){
        return false;
    }
    $init = " set {$where[0]} = ?";
    for($count = 1; $count<count($where);$count++){
        //$and_or = in_array($where[$count],$arr)?'or':'and';
        $init.= ",  {$where[$count]} = ?";
        
    }
    return $init;
}

public function build_search_string(&$config){
    if(!isset($config['where_parameters'])){
        $config['where_parameters']='';
    }
    return $this->search_param($config['search_parameters'])."from {$config['table_name']}".$this->generate_where($config['where_parameters']);
}
private function search_param($search){
 if(is_string($search)){
     return "select $search";
 }
 if(!is_array($search)){
    return false;
 }
 $init = "select {$search[0]}";
 for($count = 1; $count<count($search);$count++){
     $init.=", {$search[$count]}";
 }
 return $init.=" ";
}
private function generate_where($where){
    if(is_null($where) || empty($where)){
        return "";
    }
    $keys = array_keys($where);
    $values = array_values($where);
    $init = " where {$keys[0]} = '{$values[0]}'";
    for($count = 1; $count<count($keys);$count++){
        $init.= " and {$keys[$count]} = '{$values[$count]}'";
    }
    return $init;
}

public function populate_database_prepared(){
    $config['table_name']='registration';
    $array_firstname=array("John","Doe","Leo","Segun","Philip","Rodney","Max","Kellerman");
    $array_lastname=array("John","Doe","Leo","Segun","Philip","Rodney","Max","Kellerman");
    $array_username=array("john123","doe890","900leo","segun67","phil903","roddyp","maximuse","keller69");
    $array_email=array("John@gmail.com","Doe@gmail.com","Leo@gmail.com","Segun@gmail.com","Philip@gmail.com","Rodney@gmail.com","Max@gmail.com","Kellerman@gmail.com");
    $countt = 1;
    $insert = true;
    while($countt<21){
        $array[]=$array_firstname[array_rand($array_firstname)];
        $array[]=$array_lastname[array_rand($array_lastname)];
        $array[]=$array_email[array_rand($array_email)];
        $array[]=$array_username[array_rand($array_username)];
        $hash=$array_username[array_rand($array_username)];
        //$hash = $array['pt_pass'];
        $array[]=password_hash($hash,PASSWORD_BCRYPT);
        $array[]=$hash;
        $config['array_table_value']=$array;
        $config['table_name']='registration';
        $config['array_table_column']=array('first_name','last_name','email','username','password','pt_pass');
        
        $ins = $this->insert_into_database($config);
        $insert = $insert && boolval($ins);
        $array=null;
        $countt++;
        
    }
    return $insert;
}

public function insert_into_database(&$config){
    $sql = $this->conn->prepare($this->build_insert_string_prepared($config));
    for($count=0;$count<count($config['array_table_value']);$count++){
        $sql->bindParam($count+1,$config['array_table_value'][$count]);
    }
    return $sql->execute();
}



public function test_search(){
    $config['table_name']='registration';
    $config['search_parameters'] = array('username','email','pt_pass','first_name');

   // $config['where_parameters'] = array('username'=>'leoneo','password'=>'victory');
   $query = $this->conn->query($this->build_search_string($config));
   if(false === $query){
       return false;
   } 
   $query->setFetchMode(PDO::FETCH_ASSOC);
   $array=array();
   while($r = $query->fetch()):
    $array[]=$r;
   endwhile;
   return $array;    
}

public function test_search_image(){
    $config['table_name']='image_file';
    $config['search_parameters'] = array('image_file');

    $config['where_parameters'] = array('username'=>$_SESSION['login_user']);
   $query = $this->conn->query($this->build_search_string($config));
   if(false === $query){
       return false;
   } 
   $query->setFetchMode(PDO::FETCH_ASSOC);
   $array=array();
   while($r = $query->fetch()):
    $array[]=$r;
   endwhile;
   return $array;    
}

public function test_download($id){
    $config['table_name']='movies';
    $config['search_parameters'] = array('mime','data');

    $config['where_parameters'] = array('id'=>$id);
   $query = $this->conn->query($this->build_search_string($config));
   if(false === $query){
       return false;
   } 
   $query->setFetchMode(PDO::FETCH_ASSOC);
   $array=array();
   while($r = $query->fetch()):
    $array[]=$r;
   endwhile;
   return $array;    
}

public function search_database(&$config){
    $query = $this->conn->prepare($this->build_search_string_prepared($config));
    $query->execute($config['exec_values']);
   if(false === $query){
       return false;
   } 
   $query->setFetchMode(PDO::FETCH_ASSOC);
   $array=array();
   while($r = $query->fetch()):
    $array[]=$r;
   endwhile;
   return $array;  
}


public function test_search_prepared(){
    $config['table_name']='registration';
    $config['search_parameters'] = array('username','email','pt_pass','first_name');
    $config['where_parameters'] = array('id','id','id');
   $config['exec_values'][]=1;
   $config['exec_values'][]=2;
   $config['exec_values'][]=3;
   return $this->search_database($config);
      
}

public function image_search(){
    $config['table_name']='image_file';
    $config['search_parameters'] = array('image_filee');
    $config['where_parameters'] = array('username');
    $config['exec_values'][]=$_SESSION['login_user'];
   //$config['exec_values'][]=2;
   //$config['exec_values'][]=3;
    return $this->search_database($config);
      
}


public function write_ln($data){
    echo "$data <br>";
}
    
}
$test = new DB_Operations();
//ini_set('upload_tmp_dir','/temp/file/dir');
$file = $_FILES['image_file']['name'];
$extension = check_extension($file);
if(check_acceptable_extensions($extension,array('jpg','png','jpeg','gif'))){
// $size = $_FILES['video']['size'];
 if($_FILES['image_file']['size']>10000000){
  echo "large file";   
 }else{
     $error = $_FILES['image_file']['error'];
     if($error > 0){
         switch($error){
            case 1:
            echo 'ini error';
            break;
            case 3:
            echo 'network error';
            break;
            case 4:
            echo 'no file error';
            break;
            default:
            echo 'php error';
            //break;
         }
     }else{
        if(is_uploaded_file($_FILES['image_file']['tmp_name'])){

            $dir = "uploadimage";
            if(!file_exists("$dir")){
                mkdir($dir);
               
            }
            chmod($dir,0777);
            $download_link = "$dir/".return_hash($_SESSION['login_user']).".".$extension;
            $fh = fopen($download_link,"a+");
            if(copy($_FILES['image_file']['tmp_name'],$download_link)){
                fclose($fh);
            }

            if($test->image_search() === false || !$test->image_search()){

            $array =array($download_link,$_SESSION['login_user']);
            $config['array_table_value']=$array;
            $config['table_name']='image_file';
            $config['array_table_column']=array('image_filee','username');
            if($test->insert_into_database($config)){
                echo "successful";
                }else{
                echo "unsuccessful";
                }
            }else{
                foreach($test->image_search() as $arr):
                if(file_exists($arr['image_filee'])){
                    unlink($arr['image_filee']);
                }    
                $config['update_values'] = array('image_filee');
                $config['update_where'] = array('username');
                $config['table_name'] = 'image_file';
                $config['updated_values'] = array($download_link);
                $config['where_value'] = array($_SESSION['login_user']); 
                $upd = $test->update_database($config);
                if($upd)
                echo "successful";
                else
                echo "unsuccessful";

                endforeach;
            }

           
        } 
        
        
}

 }   
}else{
 echo "invalid extension $extension";   
}

function check_extension($file_name){
    //if(is_file($file_name))
    return pathinfo($file_name,PATHINFO_EXTENSION);
    //else
   // return "Invalid";
}
function check_acceptable_extensions($extension,$array){
    return in_array(strtolower($extension),$array); 
}
function check_error($key){
    $array = array("Size of uploaded file is greater than what the server permits");
}
function return_hash($value){
    $value = str_replace(' ','',password_hash($value,PASSWORD_BCRYPT).microtime());
    $value = str_replace('/','',$value);
    $value = str_replace('\\','',$value); 
    $value = str_replace('.','',$value);
    $value = str_replace(',','',$value); 
    $value = str_replace('$2y$10$','',$value);
    return strtolower($value);
}

?>