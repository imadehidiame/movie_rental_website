<?php 
//session_start();
include_once('dashboard.php');
if(!isset($_SESSION['login']) || $_SESSION['login'] !== true){
$_SESSION['sign_error'] = "You must log in to access your dashboard";    
header("Location: login.php");    
}
$test = new Dashboard();
$check = $test->check_login_database($_SESSION['login_user'],"*",'username');
$login=array();
foreach($check as $res){
    $test->set_session_value('login_details',$res);    
} 
?>
<!DOCTYPE html>
<html lang="en">    
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">   
<link rel="shortcut icon" href="images/icon2.fw.png">
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/mdb.css">
<link rel="stylesheet" href="stylo.css">
<style>


.vid_div{
        border: 1px solid lightgrey; 
        border-radius: 10px; 
        margin: 20px 10px 20px 10px;
        padding-left:10px;
         padding-right:10px;
}        
.square-btnn{
    display: block;    
}
.f_right{
        float: right;
}        
.h2_text{
        color:white;
}        
.p_text{
        color: white; 
        font-size: 18px;
}        
.back_div{
        background-color:black; 
        opacity:0.8; 
        width: inherit; 
        height: 50%; 
        padding: 8%;        
}        
.d_bg1{
        background-image: url("images/thor_bus1.png");
        background-repeat: no-repeat;
        width: 100%;
        height: 750px;
        border:1px solid white;
        border-radius: 5px;

    }
    .d_bg2{
        background-image: url("images/thor_bus2.png");
        background-repeat: no-repeat;
        height: 750px;
        width: 100%;
        border:1px solid white;
        border-radius: 5px;
    }
    .d_bg3{
        background-image: url("images/thor_bus3.png");
        background-repeat: no-repeat;
        height: 750px;
        width: 100%;
        border:1px solid white;
        border-radius: 5px;
    }

 .foot_img{
        /*width: 100%;*/
        margin-top: 20px;
        border-top: 1px solid lightgrey;
        height: 600px;
        background-image: url("images/busss.png");
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
        
    }   
    

    @media screen and (max-width: 900px) {
  .c1{
    margin-left:30px;
  }
  .back_div{
      padding: 2%;    
      height: 60%;
   
  }
  .h2_text{
        font-size: 18px;
  }
  .p_text{
        font-size: 15px;
  }
  video{
      margin-left: 50px;
  }
  .foot_img{
        
          background-image: url("images/bus11.png");
          height: 500px;
  }
  .d_bg1{
        
        height: 390px;
        width:100%;
  }
  .d_bg2{
        
        width:100%;
        height: 390px;

  }
  .d_bg3{
        
        height: 390px;
        width:100%;

  }
  .square-btnn{
    /*display: none;    */
}

}


@media screen and (max-width: 500px) {
  .c1{
    margin-left:30px;
  }
  .vid_div{
        border: 1px solid white;           
  }
  .back_div{
      padding: 2%;    
      height: 100%;
  }
  .h2_text{
        font-size: 15px;
  }
  .p_text{
        font-size: 12px;
  }
  video{
      margin-left: 80px;
  }
  .foot_img{
        
          background-image: url("images/bus1.png");
          height: 500px;
  }
  .d_bg1{
        background-image: url("images/thor11.png");
        height: 200px;

  }
  .d_bg2{
        background-image: url("images/thor22.png");
        height: 200px;

  }
  .d_bg3{
        background-image: url("images/thor4444.png");
        height: 200px;

  }
  .square-btnn{
    /*display: none;    */
}

}
li:hover{
        text-decoration: underline;
}


</style>
<title>247Rentals</title>   
</head>
<body>
                <header>
                        <nav class="navbar navbar-expand-lg navbar-dark indigo">
                                
                                  
                                  <a class="navbar-brand" href="index.php"><img src="images/icon.fw.png" style="margin-right:3px"><span style="font-size:18px;" class="badge badge-pill badge-light">247Rentals</span></a>
                                
                                  
                                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
                                      aria-expanded="false" aria-label="Toggle navigation">
                                      <span class="navbar-toggler-icon"></span>
                                  </button>
                                
                                  <!-- Collapsible content -->
                                  <div class="collapse navbar-collapse" id="basicExampleNav">
                                
                                      <!-- Links -->
                                      <ul class="navbar-nav mr-auto">
                                          <li class="nav-item active">
                                              <a class="nav-link" href="loginsuccess.php">Dashboard
                                                  <span class="sr-only">(current)</span>
                                              </a>
                                          </li>
                                          <li class="nav-item">
                                              <a class="nav-link" href="profile.php">Profile</a>
                                          </li>
                                          
                    
                                          <li class="nav-item">
                                            <a class="nav-link" href="videos.php">Upload Movies</a>
                                        </li>

                                        <li class="nav-item">
                                                <a class="nav-link" href="movie_rate.php">Rate Movies</a>
                                            </li>

                                        <li class="nav-item">
                                                  <a class="nav-link" href="logout.php">Log out</a>
                                              </li>
                                          <!-- Dropdown -->
                                          
                                      </ul>
                                      <!-- Links -->
                               
                                  </div>
                                  <!-- Collapsible content -->
                                
                                </nav>

                               
                                </header>
                                <main class="mt-5">
                                        <div class="container">
                                            
                                            
                                                
                                            <!--Section: Best Features-->
                                    
                                            <hr class="my-5">
                                    
                                         
                                    
                                            
                                    
                                            <!--Section: Contact-->
                                            <section id="contact">
                                                    
                                                        <!-- Heading -->
                                                        <h2 class="mb-3 font-weight-bold text-center">Dear <?php echo $_SESSION['login_details']['first_name']; ?></h2>
                                                <h3 class="mb-0 font-weight-normal text-center text-danger"> Welcome to Your DASHBOARD</h3>
                                                        <!--Grid row-->
                                                        <div class="row">
                                                    

                                                                <div class="col-lg-2">
                                                                        <!-- Form contact -->
                                                                        <!-- Form contact -->
                                                                      </div>

                                                        <!--Grid column-->
                                                        <div class="col-lg-8">
                                                                <!-- Form contact -->
                                                                    <form class="p-5" action="logout.php" method="POST">
                                                            
                                                                    <div class="md-form form-sm"> Username
                                                                        <input type="text" name="username" id="form32" value="<?php echo $_SESSION['login_details']['username']; ?>" class="form-control form-control-sm inp" placeholder="Username" readonly>
                                                                        
                                                                    </div>
                                                                        
                                                                        <div class="md-form form-sm">Email
                                                                        <input type="text" value="<?php echo $_SESSION['login_details']['email']; ?>" name="password" id="form32" class="form-control form-control-sm inp" placeholder="Password" readonly>
                                                                        
                                                                    </div>
                                                                    
                                                                    <div class="mt-4 text-center">
                                                                    <button type="submit" class="btn btn-sm btn-success BTN" id="submit_button"><span data-feather="send"></span> Log Out</button>
                                                                        
                                                                    </div>
                                                                        <div class="mt-4 text-center" id="log_messages">
                                                                        
                                                                        </div>
                                                                    </form>
                                                                    
                                                                    

                                                                    
                                                              </div>


                                                              <div class="col-lg-2">
                                                                    <!-- Form contact -->
                                                                    
                                                                    <!-- Form contact -->
                                                                  </div>
                                                        <!--Grid column-->
                                                    
                                                        <!--Grid column-->
                                                        
                                                        <!--Grid column-->
                                                    
                                                        </div>
                                                        <!--Grid row-->
                                                    
                                                    </section>
                                            <!--Section: Contact-->
                                    
                                        </div>
                                    </main>
                                    <footer class="page-footer text-center text-md-left font-medium indigo pt-4 mt-4">
                                            
                                                <!--Footer Links-->
                                                <div class="container text-center text-md-left">
                                                    <div class="row">
                                            
                                                        <!--First column-->
                                                        <div class="col-md-6 pb-3">
                                                            <h5 class="text-uppercase">Quick Links</h5>
                                                            
                                                            <b style="font-size:18px;"><a href="loginsuccess.php">Dashboard | 
                                                                          
                                                                        </a></b>
                                                                        <b style="font-size:18px;"><a  href="profile.php">Profile | 
                                                                          
                                                                            </a></b>
                                                                            <b style="font-size:18px;"><a href="videos.php">Upload Movie 
                                                                          
                                                                                </a></b>
                                
                                                            
                                                        </div>
                                                        <!--/.First column-->
                                            
                                                        <!--Second column-->
                                                        <div class="col-md-6 pb-3">
                                                                <!--
                                                                <a href="#!"><img src="images/teleg1.png"></a>
                                                                <a href="#!"><img src="images/teleg.png"></a>
                                                                <a href="#!"><img src="images/teleg.png"></a>
                                                                <a href="#!"><img src="images/teleg1.png"></a>
                                                                -->
                                                        </div>
                                                
                                                    </div>
                                                </div>
                                                
                                                <div class="footer-copyright py-3 text-center indigo">
                                                    © 2018 Copyright:
                                                    <a href="#"> 247Rentals.com </a>
                                                </div>
                                                
                                            
                                            </footer>
                
                    <script src="bootstrap/js/jquery.js"></script>
                    <script src="js/popper.js"></script>
                    <script src="bootstrap/js/bootstrap.min.js"></script>
                    <script src="bootstrap/js/mdb.min.js"></script>
                    <script src="includes/js/feather.min.js"></script>    
                    <script src="feath.js"></script>
                    <script src="js/send_message.js"></script>
                    <script>
                            $('.carousel').carousel({
                                interval: 3000,
                            })
                        </script>                
</body>
</html>