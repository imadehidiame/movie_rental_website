<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">    
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">   
<link rel="shortcut icon" href="images/icon2.fw.png">
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/mdb.css">
<link rel="stylesheet" href="stylo.css">
<style>
html,
body,
header,
#intro{
    height: 100%;
}

#intro {
    background: url("images/smartp.jpg") no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}

.vid_div{
        border: 1px solid lightgrey; 
        border-radius: 10px; 
        margin: 20px 10px 20px 10px;
        padding-left:10px;
         padding-right:10px;
}        
.square-btnn{
    display: block;    
}
.f_right{
        float: right;
}        
.h2_text{
        color:white;
}        
.p_text{
        color: white; 
        font-size: 18px;
}        
.back_div{
        background-color:black; 
        opacity:0.8; 
        width: inherit; 
        height: 50%; 
        padding: 8%;        
}        
.d_bg1{
        background-image: url("images/thor_bus1.png");
        background-repeat: no-repeat;
        width: 100%;
        height: 750px;
        border:1px solid white;
        border-radius: 5px;

    }
    .d_bg2{
        background-image: url("images/thor_bus2.png");
        background-repeat: no-repeat;
        height: 750px;
        width: 100%;
        border:1px solid white;
        border-radius: 5px;
    }
    .d_bg3{
        background-image: url("images/thor_bus3.png");
        background-repeat: no-repeat;
        height: 750px;
        width: 100%;
        border:1px solid white;
        border-radius: 5px;
    }

 .foot_img{
        /*width: 100%;*/
        margin-top: 20px;
        border-top: 1px solid lightgrey;
        height: 600px;
        background-image: url("images/busss.png");
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
        
    }   
    

    @media screen and (max-width: 900px) {
  .c1{
    margin-left:30px;
  }
  .back_div{
      padding: 2%;    
      height: 60%;
   
  }
  .h2_text{
        font-size: 18px;
  }
  .p_text{
        font-size: 15px;
  }
  video{
      margin-left: 50px;
  }
  .foot_img{
        
          background-image: url("images/bus11.png");
          height: 500px;
  }
  .d_bg1{
        
        height: 390px;
        width:100%;
  }
  .d_bg2{
        
        width:100%;
        height: 390px;

  }
  .d_bg3{
        
        height: 390px;
        width:100%;

  }
  .square-btnn{
    /*display: none;    */
}

}


@media screen and (max-width: 500px) {
  .c1{
    margin-left:30px;
  }
  .vid_div{
        border: 1px solid white;           
  }
  .back_div{
      padding: 2%;    
      height: 100%;
  }
  .h2_text{
        font-size: 15px;
  }
  .p_text{
        font-size: 12px;
  }
  video{
      margin-left: 80px;
  }
  .foot_img{
        
          background-image: url("images/bus1.png");
          height: 500px;
  }
  .d_bg1{
        background-image: url("images/thor11.png");
        height: 200px;

  }
  .d_bg2{
        background-image: url("images/thor22.png");
        height: 200px;

  }
  .d_bg3{
        background-image: url("images/thor4444.png");
        height: 200px;

  }
  .square-btnn{
    /*display: none;    */
}

}
li:hover{
        text-decoration: underline;
}


</style>
<title>247Rentals</title>   
</head>
<body>
                <header>
                        <nav class="navbar navbar-expand-lg navbar-dark indigo">
                                
                                  
                                  <a class="navbar-brand" href="index.php"><img src="images/icon.fw.png" style="margin-right:3px"><span style="font-size:18px;" class="badge badge-pill badge-light">247Rentals</span></a>
                                
                                  
                                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
                                      aria-expanded="false" aria-label="Toggle navigation">
                                      <span class="navbar-toggler-icon"></span>
                                  </button>
                                
                                  <!-- Collapsible content -->
                                  <div class="collapse navbar-collapse" id="basicExampleNav">
                                
                                      <!-- Links -->
                                      <ul class="navbar-nav mr-auto">
                                          <li class="nav-item">
                                              <a class="nav-link" href="index.php">Home
                                                  <span class="sr-only">(current)</span>
                                              </a>
                                          </li>
                                          <li class="nav-item">
                                              <a class="nav-link" href="our_gallery.php">Gallery</a>
                                          </li>
                                          
                    
                                          <li class="nav-item">
                                            <a class="nav-link" href="about_us.php">About</a>
                                        </li>
                                          <!-- Dropdown -->
                                          <li class="nav-item dropdown active">
                                                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span style="font-size:13px;" class="badge badge-pill badge-light">247Rentals </span> Account</a>
                                                    <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                                            <a class="dropdown-item" href="login.php">Log in</a>
                                                        <a class="dropdown-item" href="sign_up.php">Sign up</a>
                                                       
                                                        
                                                    </div>
                                                </li>
                                                <li class="nav-item">
                                                        <a class="nav-link" href="leap_year.php">Leap Year Check</a>
                                                    </li>
                                      </ul>
                                      <!-- Links -->
                                
                                      <form class="form-inline">
                                        <div class="md-form mt-0">
                                        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                                        </div>
                                        <div class="md-form mt-0">
                                                <button class="btn btn-sm btn-success BTN" id="hide_img"><span data-feather="search"></span> Search</button>
                                    </div>
                                      </form>
                                  </div>
                                  <!-- Collapsible content -->
                                
                                </nav>

                                    <div id="intro" class="view">
                                            
                     <div class="mask rgba-black-strong">
                                            
                    <div class="container-fluid d-flex align-items-center justify-content-center h-100">
                                            
                     <div class="row d-flex justify-content-center text-center">
                                            
                             <div class="col-md-10">
                                            
                                                                <!-- Heading -->
             <h2 class="font-weight-bold white-text pt-5 mb-2" style="font-size:5vw;">Give Us A Call or Send Us A Message</h2>
                                            
             <h2 class="display-4 font-weight-bold white-text pt-5 mb-2"><span class="badge badge-pill badge-light">247Rentals</span></h2>                         <!-- Divider -->
                                                                <!--<hr class="hr-light">
                                            
                                                                 Description 
                                                                <h4 class="white-text my-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti consequuntur.</h4>
                                                                <button type="button" class="btn btn-outline-white">Read more<i class="fa fa-book ml-2"></i></button>
                                            -->
                                                            </div>
                                            
                                                        </div>
                                            
                                                    </div>
                                            
                                                </div>
                                            
                                            </div>

                                </header>
                                <main class="mt-5">
                                        <div class="container">
                                            
                                            
                                                
                                            <!--Section: Best Features-->
                                    
                                            <hr class="my-5">
                                    
                                         
                                    
                                            
                                    
                                            <!--Section: Contact-->
                                            <section id="contact">
                                                    
                                                        <!-- Heading -->
                                                        <h2 class="mb-3 font-weight-bold text-center">Login</h2>
                                                <h3 class="mb-0 font-weight-normal text-center text-danger"><?php 
                                                    if(isset($_SESSION["sign_error"])){
                                                        echo $_SESSION["sign_error"];
                                                        unset($_SESSION["sign_error"]);
                                                    }
                                                    ?></h3>
                                                        <!--Grid row-->
                                                        <div class="row">
                                                    

                                                                <div class="col-lg-2">
                                                                        <!-- Form contact -->
                                                                        <!-- Form contact -->
                                                                      </div>

                                                        <!--Grid column-->
                                                        <div class="col-lg-8">
                                                                <!-- Form contact -->
                                                                    <form class="p-5" action="sign_up_in_database.php" method="POST">
                                                            
                                                                    <div class="md-form form-sm"> <span class="orange-text" data-feather="tag"></span>
                                                                        <input type="text" name="username" id="form32" class="form-control form-control-sm inp" placeholder="Username" value="<?php if(isset($_SESSION['l_user']))
                                                                        echo $_SESSION['l_user'];
                                                                        unset($_SESSION['l_user']);
                                                                        ?>" required>
                                                                        
                                                                    </div>
                                                                         
                                                                        <div class="md-form form-sm"> <span class="orange-text" data-feather="tag"></span>
                                                                        <input type="password" name="password" id="form32" class="form-control form-control-sm inp" placeholder="Password" required>
                                                                        
                                                                    </div>
                                                                    <input type="hidden" name="log" value="login">
                                                                    <div class="mt-4 text-center">
                                                                    <button type="submit" class="btn btn-sm btn-success BTN" id="submit_button"><span data-feather="send"></span> Submit</button>
                                                                        
                                                                    </div>
                                                                        <div class="mt-4 text-center" id="log_messages">
                                                                         
                                                                        </div>
                                                                    </form>
                                                                    
                                                                    
                                                                <h5 class="mb-3 font-weight-bold text-center">Don't Have An Account Yet? <a href="sign_up.php">SIGN UP</a></h5>
                                                                    
                                                              </div>


                                                              <div class="col-lg-2">
                                                                    <!-- Form contact -->
                                                                    
                                                                    <!-- Form contact -->
                                                                  </div>
                                                        <!--Grid column-->
                                                    
                                                        <!--Grid column-->
                                                        
                                                        <!--Grid column-->
                                                    
                                                        </div>
                                                        <!--Grid row-->
                                                    
                                                    </section>
                                            <!--Section: Contact-->
                                    
                                        </div>
                                    </main>
                                    <footer class="page-footer text-center text-md-left font-medium indigo pt-4 mt-4">
                                            
                                                <!--Footer Links-->
                                                <div class="container text-center text-md-left">
                                                    <div class="row">
                                            
                                                        <!--First column-->
                                                        <div class="col-md-6 pb-3">
                                                            <h5 class="text-uppercase">Quick Links</h5>
                                                            
                                                            <b style="font-size:18px;"><a href="index.html">Home | 
                                                                          
                                                                        </a></b>
                                                                        <b style="font-size:18px;"><a  href="our_gallery.html">Gallery | 
                                                                          
                                                                            </a></b>
                                                                            <b style="font-size:18px;"><a href="send_message.html">Contact 
                                                                          
                                                                                </a></b>
                                
                                                            
                                                        </div>
                                                        <!--/.First column-->
                                            
                                                        <!--Second column-->
                                                        <div class="col-md-6 pb-3">
                                                                <!--
                                                                <a href="#!"><img src="images/teleg1.png"></a>
                                                                <a href="#!"><img src="images/teleg.png"></a>
                                                                <a href="#!"><img src="images/teleg.png"></a>
                                                                <a href="#!"><img src="images/teleg1.png"></a>
                                                                -->
                                                        </div>
                                                
                                                    </div>
                                                </div>
                                                
                                                <div class="footer-copyright py-3 text-center indigo">
                                                    © 2018 Copyright:
                                                    <a href="#"> 247Rentals.com </a>
                                                </div>
                                                
                                            
                                            </footer>
                
                    <script src="bootstrap/js/jquery.js"></script>
                    <script src="js/popper.js"></script>
                    <script src="bootstrap/js/bootstrap.min.js"></script>
                    <script src="bootstrap/js/mdb.min.js"></script>
                    <script src="includes/js/feather.min.js"></script>    
                    <script src="feath.js"></script>
                    <script src="js/send_message.js"></script>
                    <script>
                            $('.carousel').carousel({
                                interval: 3000,
                            })
                        </script>                
</body>
</html>