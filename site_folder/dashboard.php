<?php 
session_start();
include_once('db_operations.php');
class Dashboard{
    private $db;
    public function __construct(){
        if(!isset($_SESSION['login']) || $_SESSION['login'] !== true){
            $_SESSION['sign_error'] = "You must log in to access your dashboard";    
//           header("Location: login.php");
        }
        $this->db = new Db_Operations();
    }
    private function check_data_in_database($value,$search){
        $array_val[] = $value; 
        $config['table_name']='registration';
        $config['search_parameters'] = array($search);
        $config['where_parameters'] = array($search);
        $config['exec_values']=$array_val;
        return $this->db->search_database($config);
    }
    public function check_login_database($value,$search,$search_param){
        $array_val[] = $value;
        $config['table_name']='registration';
        $config['search_parameters'] = array($search);
        $config['where_parameters'] = array($search_param);
        $config['exec_values']=$array_val;
        return $this->db->search_database($config);
    }

    public function check_image_database($value,$search,$search_param){
        $array_val[] = $value;
        $config['table_name']='image_file';
        $config['search_parameters'] = array($search);
        $config['where_parameters'] = array($search_param);
        $config['exec_values']=$array_val;
        return $this->db->search_database($config);
    }

    public function check_video_database($value,$search,$search_param){
        $array_val[] = $value;
        $config['table_name']='movies';
        $config['search_parameters'] = array($search);
        $config['where_parameters'] = array($search_param);
        $config['exec_values']=$array_val;
        return $this->db->search_database($config);
    }

    

    public function check_em($value){
        if($this->check_data_in_database($value,'username'))
        echo 'Username already registered';
        else
        echo 'Username not registered';
    }
    public function validate_all($first,$last,$email,$username,$password){
        $check = true;
        //$array[]=$first;$array[]=$last;$array[]=$email;$array[]=$username;$array[]=$password;
        $this->clean_value($first);
        $this->clean_value($last);
         $this->clean_value($email);
         $this->clean_value($username);
        $this->clean_value($password);
        //$this->clean_values($array);
        $var = filter_var($first,FILTER_CALLBACK,array('options'=>function($value){
            $valid_length = strlen($value)>=2;
            //$check = $check && $valid_length;
            if(!$valid_length){
                $this->set_session_value('firstname_validation_error','First name must contain at least two characters');
            }else{
                return $value;
            }
        }));
         
        $check = $check && boolval($var);

        $var = filter_var($last,FILTER_CALLBACK,array('options'=>function($value){
            $valid_length = strlen($value)>=2;
            //$check = $check && $valid_length;
            if(!$valid_length){
                $this->set_session_value('lastname_validation_error','Last name must contain at least two characters');
            }else{
                return $value;
            }
        }));

        $check = $check && boolval($var);

        $var = filter_var($email,FILTER_VALIDATE_EMAIL);
        $check = $check && boolval($var);
        if(!$var){
            $this->set_session_value('email_validation_error','Invalid email address entered');    
        }

        //$check_email = boolval($this->check_data_in_database($email,'email'));
        if($var){
        if($this->check_data_in_database($email,'email')){
            $check = false;
            $this->set_session_value('email_validation_error',"Unfortunately, $email has already been registered");
        }
        }
        $var = filter_var($username,FILTER_CALLBACK,array('options'=>function($value){
            $valid_length = strlen($value)>=2;
           // $check = $check && $valid_length;
            if(!$valid_length){
                $this->set_session_value('username_validation_error','Username must contain at least two characters');
            }else{
                return $value;
            }
        }));
        $check = $check && boolval($var);
        if($var){
        if($this->check_data_in_database($username,'username')){
            $check = false;
            $this->set_session_value('username_validation_error',"Unfortunately, $username has already been registered");
        }
        }
        $var = filter_var($password,FILTER_CALLBACK,array('options'=>function($value){
            $valid_length = strlen($value)>=2;
            //$check = $check && $valid_length;
            if(!$valid_length){
                $this->set_session_value('password_validation_error','Password must contain at least two characters');
            }else{
                return $value;
            }
        }));
        $check = $check && boolval($var);

        //echo $username;

        return $check;

    }


    public function validate_update($first,$last,$email,$password){
        $check = true;
        //$array[]=$first;$array[]=$last;$array[]=$email;$array[]=$username;$array[]=$password;
        $this->clean_value($first);
        $this->clean_value($last);
         $this->clean_value($email);
        $this->clean_value($password);
        //$this->clean_values($array);
        if(!empty($first)){
        $var = filter_var($first,FILTER_CALLBACK,array('options'=>function($value){
            $valid_length = strlen($value)>=2;
            //$check = $check && $valid_length;
            if(!$valid_length){
                $this->set_session_value('firstname_validation_error','First name must contain at least two characters');
            }else{
                return $value;
            }
        }));
        $check = $check && boolval($var);
    }
        
        if(!empty($last)){
        $var = filter_var($last,FILTER_CALLBACK,array('options'=>function($value){
            $valid_length = strlen($value)>=2;
            //$check = $check && $valid_length;
            if(!$valid_length){
                $this->set_session_value('lastname_validation_error','Last name must contain at least two characters');
            }else{
                return $value;
            }
        }));

        $check = $check && boolval($var);
    }

    if(!empty($email)){
        $var = filter_var($email,FILTER_VALIDATE_EMAIL);
        $check = $check && boolval($var);
        if(!$var){
            $this->set_session_value('email_validation_error','Invalid email address entered');    
        }

        //$check_email = boolval($this->check_data_in_database($email,'email'));
        if($var){
        if($this->check_data_in_database($email,'email')){
            $check = false;
            $this->set_session_value('email_validation_error',"$email has already been registered. Please ensure you fill out only fields you need to update. Do not fill out fields you don't want to update");
        }
        }
    }
    if(!empty($password)){
        $var = filter_var($password,FILTER_CALLBACK,array('options'=>function($value){
            $valid_length = strlen($value)>=2;
            //$check = $check && $valid_length;
            if(!$valid_length){
                $this->set_session_value('password_validation_error','Password must contain at least two characters');
            }else{
                return $value;
            }
        }));
        $check = $check && boolval($var);
    }
        //echo $username;

        return $check;

    }

    public function update_data($first,$last,$email,$password){
        if(!$this->validate_update($first,$last,$email,$password)){
            return "Invalid";
        }
        $config['update_values']=array();
        $config['update_where'] = array('username');
        $config['updated_values'] = array();
        $config['where_value'] = array($_SESSION['login_user']); 
        $config['table_name'] = 'registration';
        if(!empty($first)){
            $config['update_values'][]='first_name';
            $config['updated_values'][]=$first;
        }
        if(!empty($last)){
            $config['update_values'][]='last_name';
            $config['updated_values'][]=$last;
        }
        if(!empty($email)){
            $config['update_values'][]='email';
            $config['updated_values'][]=$email;
        }
        if(!empty($password)){
            $pt_pass = password_hash($password,PASSWORD_BCRYPT);
            $config['update_values'][]='pt_pass';
            $config['updated_values'][]=$password;
            $config['update_values'][]='password';
            $config['updated_values'][]=$pt_pass;
        }


        /*$config['update_values'] = array('username','email');
        $config['update_where'] = array('id','id');
        $config['table_name'] = 'registration';
        $config['updated_values'] = array('freedom','freetone4ever@yahoo.com');
        $config['where_value'] = array(172,173); */
        if(count($config['update_values'])<1 || empty($config['update_values'])){
            return "Empty";
        }
        $upd = $this->db->update_database($config);
        if($upd)
        return "Updated";
        //else
        //echo "Not updated";
    }


    public function update_video($title,$genre,$description,$id_value){
        
        $config['update_values']=array();
        $config['update_where'] = array('id');
        $config['updated_values'] = array();
        $config['where_value'] = array($id_value); 
        $config['table_name'] = 'movies';
        if(!empty($title)){
            $config['update_values'][]='title';
            $config['updated_values'][]=$title;
        }
        if(!empty($genre)){
            $config['update_values'][]='genre';
            $config['updated_values'][]=$genre;
        }
        if(!empty($description)){
            $config['update_values'][]='description';
            $config['updated_values'][]=$description;
        }
        
        /*$config['update_values'] = array('username','email');
        $config['update_where'] = array('id','id');
        $config['table_name'] = 'registration';
        $config['updated_values'] = array('freedom','freetone4ever@yahoo.com');
        $config['where_value'] = array(172,173); */
        if(count($config['update_values'])<1 || empty($config['update_values'])){
            return "Empty";
        }
        $upd = $this->db->update_database($config);
        if($upd)
        return "Updated";
        //else
        //echo "Not updated";
    }

    public function login($username,$password){
        $this->clean_value($username);
        $this->clean_value($password);
        $que = $this->check_login_database($username,"password",'username'); 
        if($que){
            foreach($que as $res){
                if(password_verify($password,$res['password'])){
                    return true;
                    break;
                }
                
            }
            
        }
        return false;
    }
    public function test_l(){
        $que = $this->check_login_database('leonardo',"*",'username'); 
        if($que){
            echo $que['password'];
        }
    }

    public function delete_video($id,$table_name,$id_value){
        return $this->db->delete_data($id,$table_name,$id_value);
    }

    public function insert_reg_details($first,$last,$email,$username,$password){
        if($this->validate_all($first,$last,$email,$username,$password)){
            $pt_pass = $password;
            $password = $this->return_hash($password);
            $config['array_table_column']=array('first_name','last_name','email','username','password','pt_pass');
            $config['array_table_value']=array($first,$last,$email,$username,$password,$pt_pass);
            $config['table_name']='registration';
            if($this->db->insert_into_database($config)){
                return true;
            }
        }
        return false;
    }
    private function return_hash($pass){
        return password_hash($pass,PASSWORD_BCRYPT);
    }
    private function check_empty($value){
        return empty($value);
    }
    private function clean_values(&$array){
        array_walk($array,function(&$value,$key){
           $value = $this->clean_value($value);
        });
    }
    public function clean_value(&$value){
        $value=strip_tags($value);
        $value=htmlspecialchars(trim($value));
        //return $value;
    }
    public function set_session_value($name,$value){
        $_SESSION[$name]=$value;
    }
    }
    $test=new Dashboard();
    if(isset($_POST['log'])){
    switch($_POST['log']){
        case 'update':
        $first = $_POST['firstname'];
        $last = $_POST['lastname'];
        $email = $_POST['email'];
        $pass = $_POST['password'];    
        $update = $test->update_data($first,$last,$email,$pass);
        //var_dump($update);
        if($update==="Updated"){
            $test->set_session_value('update_reply','Data updated successfully');
          header("Location: profile.php#contact");
    }else if($update==="Invalid"){
        //$test->set_session_value('s_u',$_POST['username']);
  //      $test->set_session_value('update_reply','Invalid data submitted');
        $test->set_session_value('s_first',$_POST['firstname']);
        $test->set_session_value('s_last',$_POST['lastname']);
        $test->set_session_value('s_email',$_POST['email']);
        header("Location: profile.php#contact");
    }else if($update==="Empty"){
        $test->set_session_value('update_reply','No data to update. Make sure you indicate only fields you want to update by filling these fields appropriately. Any field you have no intention of updating should be left blank');
        header("Location: profile.php#contact");
    }else{
        $test->set_session_value('update_reply','An error occured');
        header("Location: profile.php#contact");
    }
    break;
    case 'update_video':

        $title = $_POST['title'][0];
        $genre = $_POST['genre'][0];
        $description = $_POST['description'][0];
        $id_value = $_POST['id_value'][0]; 
        $test->clean_value($title);
        $test->clean_value($genre);
        $test->clean_value($description);
        $update = $test->update_video($title,$genre,$description,$id_value);
        //var_dump($update);
        if($update==="Updated"){
            $test->set_session_value('update_reply','<div class="succ_div ">
            <div class="succ_details text-center featurette-heading font-weight-normal">Movie data updated successfully</div></div>');
          header("Location: videos.php");
    }else if($update==="Empty"){
        $test->set_session_value('update_reply','<div class="succ_div ">
        <div class="succ_details text-center featurette-heading font-weight-normal">No data to update. Make sure you indicate only fields you want to update by filling these fields appropriately. Any field you have no intention of updating should be left blank</div></div>');
        header("Location: videos.php");
    }else{
        $test->set_session_value('update_reply','An error occured');
        header("Location: videos.php");
    }

    break; 
    }
    }      

    if(isset($_GET['del'])){
        if(file_exists($_GET['del'])){
            unlink($_GET['del']);
        }
    }
    
    if(isset($_POST['log_delete'])){
        switch($_POST['log_delete']){
            
        case 'delete':
            
            $d_id = $_POST['d_id'][0];
            $link = $_POST['link_id'][0];
            //echo "link is $link";
            $test->clean_value($d_id);
            
            $delete = $test->delete_video('id','movies',$d_id);
            //var_dump($update);
            if($delete){
                
                $string="Movie data deleted successfully";
                $test->set_session_value('update_reply','<div class="succ_div ">
                <div class="succ_details text-center featurette-heading font-weight-normal">'.$string.'</div></div>');
              header("Location: videos.php");
        }else{
            $test->set_session_value('update_reply','An error occured');
            header("Location: videos.php");
        }
    
        break; 
        }
        }      

?>