<!DOCTYPE html>
<html lang="en">    
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">   
<link rel="shortcut icon" href="images/icon2.fw.png">
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/mdb.css">
<link rel="stylesheet" href="bootstrap/gly.css">    
<style>
    .BTN{
        border-radius:15px;
        font-family: comic sans MS;
    }
    
    .feather{
        width: .875rem;
        height: .875rem;
        vertical-align: text-bottom;
        
    }
    
    .sc_style{
        border-radius:20px;
        padding:8px; 
        text-transform:capitalize; 
        font-size:14px; 
        background-color: black; 
        border: 1px solid black;
    }
    .sc_mouse{
        border-radius:20px;
        padding:8px; 
        text-transform:capitalize; 
        font-size:14px; 
        background-color: white; 
        color:black;
    }
#col_sp{
    margin-top: 100px;
}
#div_span{
    border: 1px solid lightgrey; 
    background-color: lightgray;
    border-radius: 50px;
    width: auto;
}
.vid_div{
        border: 1px solid lightgrey; 
        border-radius: 10px; 
        margin: 20px 10px 20px 10px;
        text-align: center;
}        
.square-btnn{
    display: block;    
}
.f_right{
        float: right;
}        
.h2_text{
        color:white;
}        
.p_text{
        color: white; 
        font-size: 18px;
}        
.back_div{
        background-color:black; 
        opacity:0.9; 
        width: inherit; 
        height: 50%; 
        padding: 8%;        
}        
.d_bg1{
        background-image: url("images/thor_bus1.png");
        background-repeat: no-repeat;
        width: 100%;
        height: 750px;
        border:1px solid white;
        border-radius: 5px;
        border-top: 50px;

    }
    .d_bg2{
        background-image: url("images/thor_bus2.png");
        background-repeat: no-repeat;
        height: 750px;
        width: 100%;
        border:1px solid white;
        border-radius: 5px;
        border-top: 50px;

    }
    .d_bg3{
        background-image: url("images/thor_bus3.png");
        background-repeat: no-repeat;
        height: 750px;
        width: 100%;
        border:1px solid white;
        border-radius: 5px;
        border-top: 50px;
    }

 .foot_img{
        /*width: 100%;*/
        margin-top: 20px;
        border-top: 1px solid lightgrey;
        height: 600px;
        background-image: url("images/busss.png");
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
        
    }   
    

    @media screen and (max-width: 900px) {
  .c1{
    margin-left:30px;
  }
  .back_div{
      padding: 2%;    
      height: 60%;
   
  }
  #col_sp{
    margin-top: 5px;
  }
  .h2_text{
        font-size: 18px;
  }
  .vid_div{
        border: 1px solid white;           
  }
  .p_text{
        font-size: 15px;
  }
  video{
      margin-left: 50px;
  }
  .foot_img{
        
          background-image: url("images/bus11.png");
          height: 500px;
  }
  .d_bg1{
        
        height: 390px;
        width:100%;
  }
  .d_bg2{
        
        width:100%;
        height: 390px;

  }
  .d_bg3{
        
        height: 390px;
        width:100%;

  }
  .square-btnn{
    /*display: none;    */
}

}


@media screen and (max-width: 500px) {
    #col_sp{
        margin-top:5px;
    }
  .c1{
    margin-left:30px;
  }
  .vid_div{
        border: 1px solid white;           
  }
  .back_div{
      padding: 2%;    
      height: 100%;
  }
  .h2_text{
        font-size: 15px;
  }
  .p_text{
        font-size: 12px;
  }
  video{
      margin-left: 80px;
  }
  .foot_img{
        
          background-image: url("images/bus1.png");
          height: 500px;
  }
  .d_bg1{
        background-image: url("images/thor11.png");
        height: 200px;

  }
  #div_span{
      border-radius: 80px;
  }
  .d_bg2{
        background-image: url("images/thor22.png");
        height: 200px;

  }
  .d_bg3{
        background-image: url("images/thor4444.png");
        height: 200px;

  }
  .square-btnn{
    /*display: none;    */
}

}
li:hover{
        text-decoration: underline;
}


</style>
<title>247Rentals</title>   
</head>
<body>
                <header>
                                <nav class="navbar navbar-expand-lg navbar-dark indigo">
                                    
                                      
                                      <a class="navbar-brand" href="index.html"><img src="images/icon.fw.png" style="margin-right:3px"><span style="font-size:18px;" class="badge badge-pill badge-light">247Rentals</span></a>
                                    
                                      
                                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
                                          aria-expanded="false" aria-label="Toggle navigation">
                                          <span class="navbar-toggler-icon"></span>
                                      </button>
                                    
                                      <!-- Collapsible content -->
                                      <div class="collapse navbar-collapse" id="basicExampleNav">
                                    
                                          <!-- Links -->
                                          <ul class="navbar-nav mr-auto">
                                              <li class="nav-item">
                                                  <a class="nav-link" href="index.php">Home
                                                      <span class="sr-only">(current)</span>
                                                  </a>
                                              </li>
                                              <li class="nav-item">
                                                  <a class="nav-link" href="our_gallery.php">Gallery</a>
                                              </li>
                                              
                        
                                              <li class="nav-item active">
                                                <a class="nav-link" href="about_us.php">About</a>
                                            </li>
                                              <!-- Dropdown -->
                                              <li class="nav-item dropdown">
                                                  <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Contact <span style="font-size:13px;" class="badge badge-pill badge-light">247Rentals</span></a>
                                                  <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                                                      <a class="dropdown-item" href="send_message.php">News Letter Subscription</a>
                                                        <a class="dropdown-item" href="place_order.php">Place An Order</a>
                                                      
                                                      
                                                  </div>
                                              </li>
                                              <li class="nav-item">
                                                    <a class="nav-link" href="leap_year.php">Leap Year</a>
                                                </li>
                                          </ul>
                                          <!-- Links -->
                                    
                                          <form class="form-inline">
                                            <div class="md-form mt-0">
                                            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                                            </div>
                                            <div class="md-form mt-0">
                                                <button class="btn btn-sm btn-success BTN"><span data-feather="search"></span> Search</button>
                                                
                                        </div>
                                          </form>
                                          <form class="form-inline">
                                                <div class="md-form mt-0">
                                                <!--<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">-->
                                                <select class="custom-select" id="scroll_down_select" name="sellist1" style="background-color:black;color:white;">
                                                  <option selected value="150">Select Scroll Speed</option>
                                                  <option value="1000">Extra Slow</option>
                                                  <option value="500">Slow</option>
                                                  <option value="150">Normal</option>  
                                                  <option value="50">Fast</option>
                                                  <option value="10">Very Fast</option>
                                                  <option value="1">Extra Fast</option>
                                                </select>
                                                </div>
                                                <div class="md-form mt-0">
                                                    <button class="btn sc_style sc" id="scroll_down"><span data-feather="arrow-down"></span>Automatic ScrollDown</button>
                                            </div>
                                              </form> 
                                      </div>
                                      <!-- Collapsible content -->
                                    
                                    </nav>

                                    

                                </header>
<main class="mt-40">
    
 <div class="jumbotron joumbotron-fluid indigo" id="good">
     <!--
     <div class="row">
     
     <div class="col-lg-12" id="div_span">
            <h4 class="display-4 font-weight-bold black-text pt-5 mb-2 text-center">About 247Rentals</h4>

     </div>
     
    </div>
--> 
    <div class="row justify-content-center" id="last"><div class="p-2 mb-4 text-center text-white" id="div_span"><h4 class="display-4 font-weight-bold black-text">About 247Rentals</h4></div></div>

        

</div>   
<div class="container" id="better">



        <div class="row" "best">
                <div class="col-md-7" id="col_sp">
                            <div class="view overlay z-depth-1-half">
                                <img src="images/display.jpg" id="img_opq3" class="card-img-top" alt="">
                                <div class="mask rgba-white-light"></div>
                            </div>
                            <button class="btn btn-sm btn-outline-warning BTN" id="hide_img3"><span data-feather="zoom-out"></span> Hide Image</button>
                            <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img3"><span data-feather="zoom-in"></span> Show Image</button>
                            <div class="progress pro_hide" id="prog_container3">
                            <div class="progress-bar" id="prog3"></div>
                            </div>
                        </div>
                
                        <div class="col-md-5" id="1">
                            
                            
    <h2 class="text-center">Our BIO</h2>
    <hr>
    <p>
                247Rentals is effectively meeting the needs of her Customers because we strive
                for the satisfaction of our customers and when we say 247, we mean it LITERALLY. 
               Working around the clock just to make sure you get what you desire is our delight
            </p>
            <p>
                    Entertainment has taken a different Shape. Meeting customers' needs at the comfort of their homes is guranteed
                    by our Home Service Team. Irrespective of the Genre (Horror, Action, Indigenous, Anime, name it), we have
                    everything you passionately desire to be entertained. As a matter of fact, you're one phone call away
                    from having that Movie or TV Series delivered to your door step.   
            </p>
            <p>
                    No matter the time, we can be reached 247. We don't take breaks, all we do is break records
                    in our service delivery Approach. Our Gallery has got what you need and more. Catch all the fun,
                    all movies can previewed in our <b style="color: blue; text-decoration: underline;"><a href="our_gallery.html">Gallery</a></b> by placing your mouse over the videos, and i bet you'll want to place
                    an Order afterwards.   
            </p>
            
    
                
                
                        </div>
                        
                
                    </div>


<hr>
        <div class="row">
                <div class="col-md-7" id="col_sp">
                            <div class="view overlay z-depth-1-half">
                                <img src="images/happybg.jpg" id="img_opq2" class="card-img-top" alt="">
                                <div class="mask rgba-white-light"></div>
                            </div>
                            <button class="btn btn-sm btn-outline-warning BTN" id="hide_img2"><span data-feather="zoom-out"></span> Hide Image</button>
                            <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img2"><span data-feather="zoom-in"></span> Show Image</button>
                            <div class="progress pro_hide" id="prog_container2">
                            <div class="progress-bar" id="prog2"></div>
                            </div>
                        </div>
                
                        <div class="col-md-5" id="2">
                            
                            
    <h2 class="text-center">Vision, Goals and Aspirations</h2>
    <hr>
    <p>
                247Rentals is effectively meeting the needs of her Customers because we strive
                for the satisfaction of our customers and when we say 247, we mean it LITERALLY. 
               Working around the clock just to make sure you get what you desire is our delight
            </p>
            <p>
                    Entertainment has taken a different Shape. Meeting customers' needs at the comfort of their homes is guranteed
                    by our Home Service Team. Irrespective of the Genre (Horror, Action, Indigenous, Anime, name it), we have
                    everything you passionately desire to be entertained. As a matter of fact, you're one phone call away
                    from having that Movie or TV Series delivered to your door step.   
            </p>
            <p>
                    No matter the time, we can be reached 247. We don't take breaks, all we do is break records
                    in our service delivery Approach. Our Gallery has got what you need and more. Catch all the fun,
                    all movies can previewed in our <b style="color: blue; text-decoration: underline;"><a href="our_gallery.html">Gallery</a></b> by placing your mouse over the videos, and i bet you'll want to place
                    an Order afterwards.   
            </p>
            
    
                
                
                        </div>
                        
                
                    </div>

<hr>



<div class="row">
        <div class="col-md-7" id="col_sp">
                    <div class="view overlay z-depth-1-half">
                        <img src="images/BUSINESS1.jpg" class="card-img-top" id="img_opq1" alt="">
                        <div class="mask rgba-white-light"></div>
                    </div>
                    <button class="btn btn-sm btn-outline-warning BTN" id="hide_img1"><span data-feather="zoom-out"></span> Hide Image</button>
                    <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img1"><span data-feather="zoom-in"></span> Show Image</button>
                    <div class="progress pro_hide" id="prog_container1">
                    <div class="progress-bar" id="prog1"></div>
                    </div>
                </div>
        
                <div class="col-md-5" id="3">
                    
                    
<h2 class="text-center">Customer Satisfaction Guaranteed</h2>
<hr>
<p>
        247Rentals is effectively meeting the needs of her Customers because we strive
        for the satisfaction of our customers and when we say 247, we mean it LITERALLY. 
       Working around the clock just to make sure you get what you desire is our delight
    </p>
    <p>
            Entertainment has taken a different Shape. Meeting customers' needs at the comfort of their homes is guranteed
            by our Home Service Team. Irrespective of the Genre (Horror, Action, Indigenous, Anime, name it), we have
            everything you passionately desire to be entertained. As a matter of fact, you're one phone call away
            from having that Movie or TV Series delivered to your door step.   
    </p>
    <p>
            No matter the time, we can be reached 247. We don't take breaks, all we do is break records
            in our service delivery Approach. Our Gallery has got what you need and more. Catch all the fun,
            all movies can previewed in our <b style="color: blue; text-decoration: underline;"><a href="our_gallery.html">Gallery</a></b> by placing your mouse over the videos, and i bet you'll want to place
            an Order afterwards.   
    </p>
    

        
        
                </div>
                
        
            </div>

            <hr>
            
            
            
            <div class="row" id="lastDiv">
                    <div class="col-md-7" id="col_sp">
                                <div class="view overlay z-depth-1-half">
                                    <img src="images/office.jpeg" class="card-img-top" alt="" id="img_opq">
                                    <div class="mask rgba-white-light"></div>
                                </div>
                        <button class="btn btn-sm btn-outline-warning BTN" id="hide_img"><span data-feather="zoom-out"></span> Hide Image</button>
                        <button class="btn btn-sm btn-outline-warning BTN show_hide" id="show_img"><span data-feather="zoom-in"></span> Show Image</button>
                        <div class="progress pro_hide" id="prog_container">
                        <div class="progress-bar" id="prog"></div>
                        </div>
                            </div>
                    
                            <div class="col-md-5">
                                
                                
            <h2 class="text-center">Connect With Us And Our Partners</h2>
            <hr>
            <p>
                    247Rentals is effectively meeting the needs of her Customers because we strive
                    for the satisfaction of our customers and when we say 247, we mean it LITERALLY. 
                   Working around the clock just to make sure you get what you desire is our delight
                </p>
                <p>
                        Entertainment has taken a different Shape. Meeting customers' needs at the comfort of their homes is guranteed
                        by our Home Service Team. Irrespective of the Genre (Horror, Action, Indigenous, Anime, name it), we have
                        everything you passionately desire to be entertained. As a matter of fact, you're one phone call away
                        from having that Movie or TV Series delivered to your door step.   
                </p>
                <p>
                        No matter the time, we can be reached 247. We don't take breaks, all we do is break records
                        in our service delivery Approach. Our Gallery has got what you need and more. Catch all the fun,
                        all movies can previewed in our <b style="color: blue; text-decoration: underline;"><a href="our_gallery.html">Gallery</a></b> by placing your mouse over the videos, and i bet you'll want to place
                        an Order afterwards.   
                </p>
                
            
                    
                    
                            </div>
                            
                    
                        </div>
            


</div>





</main>                                
<footer class="page-footer text-center text-md-left font-medium indigo pt-4 mt-4">
    
        <!--Footer Links-->
        <div class="container text-center text-md-left">
            <div class="row">
    
                <!--First column-->
                <div class="col-md-6 pb-3">
                    <h5 class="text-uppercase">Quick Links</h5>
                    
                    <b style="font-size:18px;"><a href="index.html">Home | 
                                  
                                </a></b>
                                <b style="font-size:18px;"><a  href="our_gallery.html">Gallery | 
                                  
                                    </a></b>
                                    <b style="font-size:18px;"><a href="send_message.html">Contact 
                                  
                        </a></b>
                        <form class="form-inline">
                                <div class="md-form mt-0">
                                <!--<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">-->
                                <select class="custom-select" id="scroll_up_select" name="sellist1" style="background-color:black;color:white;">
                                  <option selected value="150">Select Scroll Speed</option>
                                  <option value="1000">Extra Slow</option>
                                  <option value="500">Slow</option>
                                  <option value="150">Normal</option>
                                  <option value="50">Fast</option>
                                  <option value="10">Very Fast</option>
                                  <option value="1">Extra Fast</option>
                                </select>
                                </div>
                                <div class="md-form mt-0">
                                  <button class="btn sc_style sc" id="scroll_up"><span data-feather="arrow-up"></span> Automatic ScrollUp</button>
                            </div>
                              </form>
                    
                </div>
                <!--/.First column-->
    
                <!--Second column-->
                <div class="col-md-6 pb-3">
                        <!--
                        <a href="#!"><img src="images/teleg1.png"></a>
                        <a href="#!"><img src="images/teleg.png"></a>
                        <a href="#!"><img src="images/teleg.png"></a>
                        <a href="#!"><img src="images/teleg1.png"></a>
                        -->
                </div>
        
            </div>
        </div>
        
        <div class="footer-copyright py-3 text-center indigo">
            © 2018 Copyright:
            <a href="#"> 247Rentals.com </a>
        </div>
        
    
    </footer>
    
<script src="bootstrap/js/jquery.js"></script>
<script src="js/popper.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="bootstrap/js/mdb.min.js"></script>
<script src="includes/js/feather.min.js"></script>    
<script src="feath.js"></script>
<script src="js/contact.js"></script>
</body>
</html>