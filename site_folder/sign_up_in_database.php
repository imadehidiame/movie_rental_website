<?php 
session_start();
include_once('db_operations.php');
if(!isset($_POST['log'])||!isset($_POST['firstname']) || !isset($_POST['email']) || !isset($_POST['lastname']) || !isset($_POST['username']) || !isset($_POST['password']))
header("Location: sign_up.php");
class Sign_up{
    private $db;
    public function __construct(){
    
        $this->db = new Db_Operations();
    }
    private function check_data_in_database($value,$search){
        $array_val[] = $value;
        $config['table_name']='registration';
        $config['search_parameters'] = array($search);
        $config['where_parameters'] = array($search);
        $config['exec_values']=$array_val;
        return $this->db->search_database($config);
    }
    public function check_login_database($value,$search,$search_param){
        $array_val[] = $value;
        $config['table_name']='registration';
        $config['search_parameters'] = array($search);
        $config['where_parameters'] = array($search_param);
        $config['exec_values']=$array_val;
        return $this->db->search_database($config);
    }

    public function check_em($value){
        if($this->check_data_in_database($value,'username'))
        echo 'Username already registered';
        else
        echo 'Username not registered';
    }
    public function validate_all($first,$last,$email,$username,$password){
        $check = true;
        //$array[]=$first;$array[]=$last;$array[]=$email;$array[]=$username;$array[]=$password;
        $this->clean_value($first);
        $this->clean_value($last);
         $this->clean_value($email);
         $this->clean_value($username);
        $this->clean_value($password);
        //$this->clean_values($array);
        $var = filter_var($first,FILTER_CALLBACK,array('options'=>function($value){
            $valid_length = strlen($value)>=2;
            //$check = $check && $valid_length;
            if(!$valid_length){
                $this->set_session_value('firstname_validation_error','First name must contain at least two characters');
            }else{
                return $value;
            }
        }));
         
        $check = $check && boolval($var);

        $var = filter_var($last,FILTER_CALLBACK,array('options'=>function($value){
            $valid_length = strlen($value)>=2;
            //$check = $check && $valid_length;
            if(!$valid_length){
                $this->set_session_value('lastname_validation_error','Last name must contain at least two characters');
            }else{
                return $value;
            }
        }));

        $check = $check && boolval($var);

        $var = filter_var($email,FILTER_VALIDATE_EMAIL);
        $check = $check && boolval($var);
        if(!$var){
            $this->set_session_value('email_validation_error','Invalid email address entered');    
        }

        //$check_email = boolval($this->check_data_in_database($email,'email'));
        if($var){
        if($this->check_data_in_database($email,'email')){
            $check = false;
            $this->set_session_value('email_validation_error',"Unfortunately, $email has already been registered");
        }
        }
        $var = filter_var($username,FILTER_CALLBACK,array('options'=>function($value){
            $valid_length = strlen($value)>=2;
           // $check = $check && $valid_length;
            if(!$valid_length){
                $this->set_session_value('username_validation_error','Username must contain at least two characters');
            }else{
                return $value;
            }
        }));
        $check = $check && boolval($var);
        if($var){
        if($this->check_data_in_database($username,'username')){
            $check = false;
            $this->set_session_value('username_validation_error',"Unfortunately, $username has already been registered");
        }
        }
        $var = filter_var($password,FILTER_CALLBACK,array('options'=>function($value){
            $valid_length = strlen($value)>=2;
            //$check = $check && $valid_length;
            if(!$valid_length){
                $this->set_session_value('password_validation_error','Password must contain at least two characters');
            }else{
                return $value;
            }
        }));
        $check = $check && boolval($var);

        //echo $username;

        return $check;

    }
    public function login($username,$password){
        $this->clean_value($username);
        $this->clean_value($password);
        $que = $this->check_login_database($username,"password",'username'); 
        if($que){
            foreach($que as $res){
                if(password_verify($password,$res['password'])){
                    return true;
                    break;
                }
                
            }
            
        }
        return false;
    }
    public function test_l(){
        $que = $this->check_login_database('leonardo',"*",'username'); 
        if($que){
            echo $que['password'];
        }
    }
    public function insert_reg_details($first,$last,$email,$username,$password){
        if($this->validate_all($first,$last,$email,$username,$password)){
            $pt_pass = $password;
            $password = $this->return_hash($password);
            $config['array_table_column']=array('first_name','last_name','email','username','password','pt_pass');
            $config['array_table_value']=array($first,$last,$email,$username,$password,$pt_pass);
            $config['table_name']='registration';
            if($this->db->insert_into_database($config)){
                return true;
            }
        }
        return false;
    }
    private function return_hash($pass){
        return password_hash($pass,PASSWORD_BCRYPT);
    }
    private function check_empty($value){
        return empty($value);
    }
    private function clean_values(&$array){
        array_walk($array,function(&$value,$key){
           $value = $this->clean_value($value);
        });
    }
    private function clean_value(&$value){
        $value=strip_tags($value);
        $value=htmlspecialchars(trim($value));
        //return $value;
    }
    public function set_session_value($name,$value){
        $_SESSION[$name]=$value;
    }
    }

    
    //var_dump($test_value);
    /*function validate($val){
    $val = str_replace(" ","",$val);
    $length = strlen(trim($val))>3;
    if(!$length){
        return false;
    }else{
        return $val;
    }
}


$var = filter_var("g",FILTER_CALLBACK,array('options'=>function($val){
    $val = str_replace(" ","",$val);
    $length = strlen(trim($val))>3;
    if(!$length){
        $test_value = new Sign_up();
        $test_value->set_session_value('test_error','Value must contain at least two characters');
    }else{
        //return "val";
    }
}));*/

$test = new Sign_up();
switch($_POST['log']){
    case 'signup':
    if($test->insert_reg_details($_POST['firstname'],$_POST['lastname'],$_POST['email'],$_POST['username'],$_POST['password'])){
        $test->set_session_value('sign_error','Sign up successful, you can now log in to your Dashboard');
        header("Location: login.php");
}else{
    $test->set_session_value('s_user',$_POST['username']);
    $test->set_session_value('s_first',$_POST['firstname']);
    $test->set_session_value('s_last',$_POST['lastname']);
    $test->set_session_value('s_email',$_POST['email']);
    header("Location: sign_up.php");
}
break;
case 'login':
$user = $_POST['username'];
$pass = $_POST['password'];
if($test->login($user,$pass)){
    $test->set_session_value('login_user',$user);
    $test->set_session_value('login',true);
    header("Location: loginsuccess.php");
}else{
$test->set_session_value('sign_error','Invalid login details');
$test->set_session_value('l_user',$_POST['username']);
header("Location: login.php");
}
break;

}

//$test->check_em('keller69');




/*
if(!$var)
echo "<br> it is not valid oooooooo"."<br>";
else
echo "valid"."<br>";
 $var = filter_var("ima@gmail.c",FILTER_VALIDATE_EMAIL);
 if(!$var)
 echo "invalid email";
 else
 echo $var." is valid";
 */




?>