<?php 
class Leap{
    public function returnLeapYear($startYear,$endYear){
    $stringValue="";
    $leapCount=0;
    //"<tr><td>Mary</td><td>Moe</td></tr><tr><td>July</td><td>Dooley</td></tr>";
    for($count=$startYear;$count<=$endYear;$count++){
        $leapClass = $this->checkLeapYear($count)?"bg-danger":"bg-success";
        $leapStatus = $this->checkLeapYear($count)?"Leap Year":"Normal Year";
        $leapCount += $this->checkLeapYear($count)?1:0;
        $stringValue.="<tr><td>".$count."</td><td class='".$leapClass."'>".$leapStatus."</td></tr>";
    }
    $stringValue.="<tr><td>Leap Year Count</td><td>".$leapCount."</td></tr>";
    return $stringValue;
    }
    private function checkLeapYear($year){
       return ($year % 4===0 && $year % 100!==0)||$year%400===0;
    }
    public function returnYearRange($startYear,$endYear){
        $years="";
        for($count=$startYear;$count<=$endYear;$count++)
        $years.="<option value='$count'>$count</option>";
        return $years;
    }
    
}
$leap = new Leap();
$leapYears = $leap->returnLeapYear(1980,2018);
$yearRange = $leap->returnYearRange(1900,2018);
?>
<!DOCTYPE html>
<html lang="en">    
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">   
<link rel="shortcut icon" href="images/icon2.fw.png">
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/mdb.css">
<link rel="stylesheet" href="stylo.css">
<style>
html,body{
    height: 100%;
}
#intro {
    background: url("images/smartp.jpg") no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
.tint_display{
    opacity:0.2;
}
.vid_div{
        border: 1px solid lightgrey; 
        border-radius: 10px; 
        margin: 20px 10px 20px 10px;
        padding-left:10px;
         padding-right:10px;
}        
.square-btnn{
    display: block;    
}
.f_right{
        float: right;
}        
.h2_text{
        color:white;
}        
.p_text{
        color: white; 
        font-size: 18px;
}        
.back_div{
        background-color:black; 
        opacity:0.8; 
        width: inherit; 
        height: 50%; 
        padding: 8%;        
}        
.d_bg1{
        background-image: url("images/thor_bus1.png");
        background-repeat: no-repeat;
        width: 100%;
        height: 750px;
        border:1px solid white;
        border-radius: 5px;

    }
    .d_bg2{
        background-image: url("images/thor_bus2.png");
        background-repeat: no-repeat;
        height: 750px;
        width: 100%;
        border:1px solid white;
        border-radius: 5px;
    }
    .d_bg3{
        background-image: url("images/thor_bus3.png");
        background-repeat: no-repeat;
        height: 750px;
        width: 100%;
        border:1px solid white;
        border-radius: 5px;
    }

 .foot_img{
        /*width: 100%;*/
        margin-top: 20px;
        border-top: 1px solid lightgrey;
        height: 600px;
        background-image: url("images/busss.png");
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
        
    }   
    

    @media screen and (max-width: 900px) {
  .c1{
    margin-left:30px;
  }
  .back_div{
      padding: 2%;    
      height: 60%;
   
  }
  .h2_text{
        font-size: 18px;
  }
  .p_text{
        font-size: 15px;
  }
  video{
      margin-left: 50px;
  }
  .foot_img{
        
          background-image: url("images/bus11.png");
          height: 500px;
  }
  .d_bg1{
        
        height: 390px;
        width:100%;
  }
  .d_bg2{
        
        width:100%;
        height: 390px;

  }
  .d_bg3{
        
        height: 390px;
        width:100%;

  }
  .square-btnn{
    /*display: none;    */
}

}


@media screen and (max-width: 500px) {
  .c1{
    margin-left:30px;
  }
  .vid_div{
        border: 1px solid white;           
  }
  .back_div{
      padding: 2%;    
      height: 100%;
  }
  .h2_text{
        font-size: 15px;
  }
  .p_text{
        font-size: 12px;
  }
  video{
      margin-left: 80px;
  }
  .foot_img{
        
          background-image: url("images/bus1.png");
          height: 500px;
  }
  .d_bg1{
        background-image: url("images/thor11.png");
        height: 200px;

  }
  .d_bg2{
        background-image: url("images/thor22.png");
        height: 200px;

  }
  .d_bg3{
        background-image: url("images/thor4444.png");
        height: 200px;

  }
  .square-btnn{
    /*display: none;    */
}

}
li:hover{
        text-decoration: underline;
}


</style>
<title>247Rentals</title>   
</head>
<body>
                <header>
                        <nav class="navbar navbar-expand-lg navbar-dark indigo">
                                
                                  
                                  <a class="navbar-brand" href="index.php"><img src="images/icon.fw.png" style="margin-right:3px"><span style="font-size:18px;" class="badge badge-pill badge-light">247Rentals</span></a>
                                
                                  
                                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
                                      aria-expanded="false" aria-label="Toggle navigation">
                                      <span class="navbar-toggler-icon"></span>
                                  </button>
                                
                                  <!-- Collapsible content -->
                                  <div class="collapse navbar-collapse" id="basicExampleNav">
                                
                                      <!-- Links -->
                                      <ul class="navbar-nav mr-auto">
                                          <li class="nav-item">
                                              <a class="nav-link" href="index.php">Home
                                                  <span class="sr-only">(current)</span>
                                              </a>
                                          </li>
                                          <li class="nav-item">
                                              <a class="nav-link" href="our_gallery.php">Gallery</a>
                                          </li>
                                          
                    
                                          <li class="nav-item">
                                            <a class="nav-link" href="about_us.php">About</a>
                                        </li>
                                          <!-- Dropdown -->
                                          <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Contact <span style="font-size:13px;" class="badge badge-pill badge-light">247Rentals</span></a>
                                                    <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                                                        <a class="dropdown-item" href="send_message.php">News Letter Subscription</a>
                                                        <a class="dropdown-item" href="place_order.php">Place An Order</a>
                                                       
                                                        
                                                    </div>
                                                </li>
                                                <li class="nav-item active">
                                            <a class="nav-link" href="leap_year.php">Leap Year Check</a>
                                        </li>
                                      </ul>
                                      <!-- Links -->
                                
                                      <form class="form-inline">
                                        <div class="md-form mt-0">
                                        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                                        </div>
                                        <div class="md-form mt-0">
                                                <button class="btn btn-sm btn-success BTN" id="hide_img"><span data-feather="search"></span> Search</button>
                                    </div>
                                      </form>
                                  </div>
                                  <!-- Collapsible content -->
                                
                                </nav>

                                    

                                </header>
                                <main class="mt-5">
                                        <div class="container">
                                            <div class="row">
                                            <div class="col-md-6">
                                                    <table class="table table-dark table-hover">
                                                            <thead>
                                                              <tr>
                                                                <th>Year</th>
                                                                <th>Year Type</th>
                                                              </tr>
                                                            </thead>
                                                            <tbody>
                                                              <?php echo $leapYears; ?>
                                                            </tbody>
                                                          </table>
                                            </div>
                                            <div class="col-md-6"> 
                                             <div class="row">
                                                    <form class="form-inline">
                                                            <div class="md-form mt-0">
                                                            <!--<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">-->
                                                            <select class="custom-select" id="start" name="sellist1" style="background-color:black;color:white; margin-left:20px;">
                                                                    <option selected value="">Start Year</option>
                                                                    <?php echo $yearRange; ?>
                                                                  </select>
                                                            </div>
                                                            <div class="md-form mt-0">
                                                                    <!--<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">-->
                                                                    <select class="custom-select" id="end" name="sellist1" style="margin-left:20px;background-color:black;color:white;">
                                                                      <option selected value="">End Year</option>
                                                                      <?php echo $yearRange; ?>
                                                                    </select>
                                                                    </div>
                                                                    
                                                                    <div class="md-form mt-0">
                                                                            <!--<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">-->
                                                                            <select class="custom-select" id="order" name="sellist1" style="background-color:black;color:white; margin-left:20px;">
                                                                              <option selected value="ASC">Order</option>
                                                                              <option value="ASC">Ascending</option>
                                                                              <option value="DESC">Descending</option>
                                                                              
                                                                            </select>
                                                                            </div>

                                                            <div class="md-form mt-0">
                                                              <button class="btn sc_style sc" id="search" style="margin-left:20px; width: 90px;"><span data-feather="search"></span> Search</button>
                                                        </div>
                                                          </form>
                                            </div>
                                            <div class="row n_d" id="loader" style="margin-left:30px; margin-bottom:20px; color:red; font-weight:bold;">Loading Table Please Wait.....</div>   
                                            <div id="get" style="">
                                                    
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </main>
                                    <footer class="page-footer text-center text-md-left font-medium indigo pt-4 mt-4">
                                            
                                                <!--Footer Links-->
                                                <div class="container text-center text-md-left">
                                                    <div class="row">
                                            
                                                        <!--First column-->
                                                        <div class="col-md-6 pb-3">
                                                            <h5 class="text-uppercase">Quick Links</h5>
                                                            
                                                            <b style="font-size:18px;"><a href="index.html">Home | 
                                                                          
                                                                        </a></b>
                                                                        <b style="font-size:18px;"><a  href="our_gallery.html">Gallery | 
                                                                          
                                                                            </a></b>
                                                                            <b style="font-size:18px;"><a href="send_message.html">Contact 
                                                                          
                                                                                </a></b>
                                
                                                            
                                                        </div>
                                                        <!--/.First column-->
                                            
                                                        <!--Second column-->
                                                        <div class="col-md-6 pb-3">
                                                                <!--
                                                                <a href="#!"><img src="images/teleg1.png"></a>
                                                                <a href="#!"><img src="images/teleg.png"></a>
                                                                <a href="#!"><img src="images/teleg.png"></a>
                                                                <a href="#!"><img src="images/teleg1.png"></a>
                                                                -->
                                                        </div>
                                                
                                                    </div>
                                                </div>
                                                
                                                <div class="footer-copyright py-3 text-center indigo">
                                                    © 2018 Copyright:
                                                    <a href="#"> 247Rentals.com </a>
                                                </div>
                                                
                                            
                                            </footer>
                
                    <script src="bootstrap/js/jquery.js"></script>
                    <script src="js/popper.js"></script>
                    <script src="bootstrap/js/bootstrap.min.js"></script>
                    <script src="bootstrap/js/mdb.min.js"></script>
                    <script src="includes/js/feather.min.js"></script>    
                    <script src="feath.js"></script>
                    <script src="js/test_leap_enc.js"></script>
                    <script>
                            $('.carousel').carousel({
                                interval: 3000,
                            })
                        </script>                
</body>
</html>