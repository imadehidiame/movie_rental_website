<?php
//header("Cache-Control: no-cache");
header("Content-type: application/json");
include_once('rate.php');
//    public function index() {
    $test = new db_operations();    
  //  while(true){ 
    //ob_start();        
    //echo "event: ping\n";
        
        $movies = $test->get_movie_ratings();
    //title,genre,description,total_rating, average_rating
        $arr = array();
        foreach($movies as $movie):
            $title = trim($movie['title']);
            $genre = trim($movie['genre']);
            $description = trim($movie['description']);
            $total_rates = trim($movie['total_rates']);
            $stars = trim($movie['stars']);
            $id = trim($movie['id']);
            $arr[]=array(
                "title"=>$title,
                "genre"=>$genre,
                "description"=>$description,
                "total_rating"=>$total_rates,
                "average_rating"=>$stars,
                "movie_id"=>$id      
            );
        endforeach;
        
        echo json_encode($arr);   
      

?>